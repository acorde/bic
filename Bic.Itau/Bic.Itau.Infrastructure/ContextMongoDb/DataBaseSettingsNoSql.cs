﻿namespace Bic.Itau.Infrastructure.ContextMongoDb
{
    public class DataBaseSettingsNoSql
    {
        public string HostCorban { get; set; } = null!;        
        public string DatabaseName { get; set; } = null!;
        public string CollectionNameSXPItau { get; set; } = null!;
    }
}
