﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Bic.Itau.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    CodeHtml = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CodeInternal = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CnpjParameter = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CNPJConsulted = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    NumberRegistration = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    NameBusiness = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    RegistrationStage = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    SupplierName = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    Id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "START WITH 1 INCREMENT BY 1"),
                    UfParameter = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Logradouro = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Numero = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Complemento = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CEP = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Bairro = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Municipio = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    UF = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CodeIbge = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    CustomersId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Address_Customers_CustomersId",
                        column: x => x.CustomersId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Address_CustomersId",
                table: "Address",
                column: "CustomersId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}
