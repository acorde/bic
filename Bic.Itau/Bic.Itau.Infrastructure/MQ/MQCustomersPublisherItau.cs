﻿using Bic.Itau.Domain.Model;
using Bic.Itau.Domain.MQ;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Bic.Itau.Infrastructure.MQ
{
    public class MQCustomersPublisherItau : IMQCustomersPublisherItau
    {
        private readonly ILogger<MQCustomersPublisherItau> _logger;
        private readonly SettingsConfiguration _settings;

        public MQCustomersPublisherItau(ILogger<MQCustomersPublisherItau> logger, 
                                        SettingsConfiguration settings)
        {
            _logger = logger;
            _settings = settings;
        }

        public static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task PublisherCielo<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherCielo)} - Início da Publicação da mensagem na fila: thato-itau-cielo.");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare(_settings.RabbitQueueName.QueueDlq.QueueDlqThato.Cielo, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDlq.QueueDlqThato.Cielo, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName.QueueDlq.QueueDlqThato.Cielo, _settings.RabbitQueueName.QueueDlq.QueueDlqThato.Cielo, "");
                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName.QueueDlq.QueueDlqThato.Cielo }
                        };
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.Cielo, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.Cielo, null, body);
                    _logger.LogInformation($"{nameof(PublisherCielo)} - Fim da Publicação da mensagem na fila: thato-itau-cielo.");
                };
            });
        }

        public async Task PublisherGetNet<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherGetNet)} - Início da Publicação da mensagem na fila: thato-itau-getnet.");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare(_settings.RabbitQueueName.QueueDlq.QueueDlqThato.GetNet, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDlq.QueueDlqThato.GetNet, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName.QueueDlq.QueueDlqThato.GetNet, _settings.RabbitQueueName.QueueDlq.QueueDlqThato.GetNet, "");
                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName.QueueDlq.QueueDlqThato.GetNet }
                        };
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.GetNet, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.GetNet, null, body);
                    _logger.LogInformation($"{nameof(PublisherGetNet)} - Fim da Publicação da mensagem na fila: thato-itau-getnet.");
                };
            });
        }

        public async Task PublisherMauaBank<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherMauaBank)} - Início da Publicação da mensagem na fila: thato-itau-mauabank.");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare(_settings.RabbitQueueName.QueueDlq.QueueDlqThato.MauaBank, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDlq.QueueDlqThato.MauaBank, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName.QueueDlq.QueueDlqThato.MauaBank, _settings.RabbitQueueName.QueueDlq.QueueDlqThato.MauaBank, "");
                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName.QueueDlq.QueueDlqThato.MauaBank }
                        };
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.MauaBank, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.MauaBank, null, body);
                    _logger.LogInformation($"{nameof(PublisherMauaBank)} - Fim da Publicação da mensagem na fila: thato-itau-mauabank.");
                };
            });
        }

    }
}
