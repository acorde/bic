﻿using Bic.Itau.Domain.Model;
using Bic.Itau.Domain.MQ;
using Bic.Itau.Domain.Request;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Bic.Itau.Infrastructure.MQ
{
    public class MQCustomersConsumerItau : IMQCustomersConsumerItau
    {
        private readonly IRequestCustomersDomainItau _requestCustomersDomainItau;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _settings;

        public MQCustomersConsumerItau(IRequestCustomersDomainItau requestCustomersDomainItau, 
                                        SettingsConfiguration settings)
        {
            _requestCustomersDomainItau = requestCustomersDomainItau;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _settings = settings;
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task ConsumerCielo()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.Cielo, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings.RabbitQueueName.QueueDlq.QueueDlqThato.Cielo
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _requestCustomersDomainItau.SendCielo(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.Cielo, false, consumer);

            });
        }

        public async Task ConsumerGetNet()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.GetNet, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings.RabbitQueueName.QueueDlq.QueueDlqThato.GetNet
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _requestCustomersDomainItau.SendGetNet(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.GetNet, false, consumer);

            });
        }

        public async Task ConsumerMauaBank()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.MauaBank, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings.RabbitQueueName.QueueDlq.QueueDlqThato.MauaBank
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _requestCustomersDomainItau.SendMauaBank(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: _settings.RabbitQueueName.QueueDurable.QueueDurableThato.MauaBank, false, consumer);

            });
        }

        public async Task ConsumerMessageSXPAmericanas()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-itau", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-itau-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _requestCustomersDomainItau.SendAmericanas(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-itau", false, consumer);

            });
        }

        public async Task ConsumerMessageSXPAssai()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-itau", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-itau-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _requestCustomersDomainItau.SendAssai(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-itau", false, consumer);

            });
        }

        public async Task ConsumerMessageSXPRenner()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-itau", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-itau-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomainItau.SendRenner(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-itau", false, consumer);

            });
        }
    }
}
