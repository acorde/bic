﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bic.Itau.Domain.Model;

namespace Bic.Itau.Infrastructure.ModelsConfiguration
{
    public class CustomersConfiguration : IEntityTypeConfiguration<Customers>
    {
        public void Configure(EntityTypeBuilder<Customers> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CodeHtml);
            builder.Property(x => x.CnpjParameter);
            //builder.HasIndex(x => new { x.CodeHtml, x.CnpjParameter }).IsUnique(true);
            builder.Property(m => m.CodeInternal);
            builder.Property(m => m.CNPJConsulted);
            builder.Property(m => m.NumberRegistration);
            builder.Property(m => m.NameBusiness);
            builder.Property(m => m.RegistrationStage);

        }
    }
}
