﻿using Bic.Itau.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bic.Itau.Infrastructure.ModelsConfiguration
{
    public class ProposalConfiguration : IEntityTypeConfiguration<Proposal>
    {
        public void Configure(EntityTypeBuilder<Proposal> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(m => m.Valor);
            builder.Property(x => x.CustomersId);
            builder.Property(x => x.UpdatedAt);
            builder.Property(m => m.CreatedAt);
        }
    }
}
