﻿using Bic.Itau.Domain.Model;
using Bic.Itau.Domain.Request;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Text.Json;

namespace Bic.Itau.Infrastructure.Request
{
    public class RequestCustomersItau : IRequestCustomersDomainItau
    {
        private readonly ILogger<RequestCustomersItau> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _configuration;

        public RequestCustomersItau(IHttpClientFactory httpClientFactory, SettingsConfiguration configuration, ILogger<RequestCustomersItau> logger)
        {
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<Customers> SendGetNet(Customers model)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendGetNet)} - Iniciando a requisição Http Post para gerar Cartão Itaú.");

                //var options = new JsonSerializerOptions
                //{
                //    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                //    WriteIndented = true
                //};
                //var person = JsonSerializer.Serialize(model, options);

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressGetNet");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriThatoGetNet;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Itaú não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendGetNet)} - Erro no request Http Post para gerar Cartão Itaú. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendCielo(Customers model)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendCielo)} - Iniciando a requisição Http Post para gerar Cartão Itaú.");

                //var options = new JsonSerializerOptions
                //{
                //    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                //    WriteIndented = true
                //};
                //var person = JsonSerializer.Serialize(model, options);

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressCielo");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriThatoCielo;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Itaú não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendCielo)} - Erro no request Http Post para gerar Cartão Itaú. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendMauaBank(Customers model)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendMauaBank)} - Iniciando a requisição Http Post para gerar Cartão Itaú.");

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressMauaBank");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriThatoMauaBank;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Itaú não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendMauaBank)} - Erro no request Http Post para gerar Cartão Itaú. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendAmericanas(Customers model)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendAmericanas)} - Iniciando a requisição Http Post para gerar Cartão Itaú.");

                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };                
                var person = JsonSerializer.Serialize(model, options);

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressBus");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriCardItauAmericanas;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Itaú não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendAmericanas)} - Erro no request Http Post para gerar Cartão Itaú. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendRenner(Customers model)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendRenner)} - Iniciando a requisição Http Post para gerar Cartão Renner.");

                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };
                var person = JsonSerializer.Serialize(model, options);

                var _httpClient = _httpClientFactory.CreateClient("httpsClientBus");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriCardItauRenner;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Renner não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendRenner)} - Erro no request Http Post para gerar Cartão Renner. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendAssai(Customers model)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendAssai)} - Iniciando a requisição Http Post para gerar Cartão Assai.");

                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };
                var person = JsonSerializer.Serialize(model, options);

                var _httpClient = _httpClientFactory.CreateClient("httpsClientBus");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriCardItauAssai;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Assai não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendAssai)} - Erro no request Http Post para gerar Cartão Assai. {ex.Message}");
                return _customer;
            }
        }
    }
}
