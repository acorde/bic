﻿namespace Bic.Itau.Application.DTO
{
    public class ProposalDto
    {
        public ProposalDto(decimal? valor, int? customersId)
        {
            Valor = valor;
            CustomersId = customersId;
            CreatedAt = DateTimeOffset.Now.DateTime;
        }

        public ProposalDto(int id, decimal? valor, int? customersId, DateTime UpdatedAt)
        {
            Id = id;
            Valor = valor;
            CustomersId = customersId;
            UpdatedAt = DateTimeOffset.Now.DateTime;
        }

        public int? Id { get; set; }
        public decimal? Valor { get; set; }
        public int? CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; } 

    }
}
