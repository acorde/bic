﻿using AutoMapper;
using Bic.Itau.Application.DTO;
using Bic.Itau.Domain.Model;

namespace Bic.Itau.Infrastructure.Mapper
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {
            CreateMap<ProposalDto, Proposal>();
            CreateMap<Proposal, ProposalDto>();
            CreateMap<CustomersDto, Customers>();
            CreateMap<Customers, CustomersDto>();
            CreateMap<AddressDto, Address>();
            CreateMap<Address, AddressDto>();
        }
    }
}
