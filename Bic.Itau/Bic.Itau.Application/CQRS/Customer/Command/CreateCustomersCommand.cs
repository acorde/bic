﻿using Bic.Itau.Application.DTO;
using MediatR;

namespace Bic.Itau.Application.CQRS.Customer.Command
{
    public class CreateCustomersCommand : IRequest<CustomersDto>
    {
        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public AddressDto? Address { get; set; }
        public List<ProposalDto>? Proposals { get; set; }
    }
}
