﻿using Bic.Itau.Domain.MQ;
using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Bic.Itau.Schedule
{
    public class ScheduleItau : IHostedService
    {
        private readonly IMQCustomersConsumerItau _customers;
        private readonly ILogger<ScheduleItau> _logger;

        public ScheduleItau(IMQCustomersConsumerItau customers, ILogger<ScheduleItau> logger)
        {
            _customers = customers;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                RecurringJob.AddOrUpdate("enviar-dados-da-fila-itau-b2b-getnet", () => ConsumerGetNet(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("enviar-dados-da-fila-itau-b2b-cielo", () => ConsumerCielo(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("enviar-dados-da-fila-itau-b2b-mauaBank", () => ConsumerMauaBank(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("enviar-dados-da-fila-itau-b2b-sxp-customers", () => ConsumerMessageSXPAmericanas(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("sxp-customers-renner", () => ConsumerMessageSXPRenner(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("sxp-customers-assai", () => ConsumerMessageSXPAssai(), MinuteInterval(1));
            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerGetNet()
        {
            try
            {
                await _customers.ConsumerGetNet();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire GetNet - Schedule {nameof(ConsumerGetNet)}: {ex.Message}.");
            }
        }

        public async Task ConsumerCielo()
        {
            try
            {
                await _customers.ConsumerCielo();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Cielo - Schedule {nameof(ConsumerCielo)}: {ex.Message}.");
            }
        }

        public async Task ConsumerMauaBank()
        {
            try
            {
                await _customers.ConsumerMauaBank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Maua Bank - Schedule {nameof(ConsumerCielo)}: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageSXPAmericanas()
        {
            try
            {
                await _customers.ConsumerMessageSXPAmericanas();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire BIC Itaú Americanas - Schedule {nameof(ConsumerMessageSXPAmericanas)}: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageSXPRenner()
        {
            try
            {
                await _customers.ConsumerMessageSXPRenner();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire BIC Itaú Renner - Schedule {nameof(ConsumerMessageSXPRenner)}: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageSXPAssai()
        {
            try
            {
                await _customers.ConsumerMessageSXPAssai();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire BIC Itaú Assaí - Schedule {nameof(ConsumerMessageSXPAssai)}: {ex.Message}.");
            }
        }
    }
}
