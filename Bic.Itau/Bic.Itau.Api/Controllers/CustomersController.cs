﻿using AutoMapper;
using Bic.Itau.Application.CQRS.Customer.Command;
using Bic.Itau.CrossCutting.ModelMongo;
using Bic.Itau.CrossCutting.Mongo.Interfaces;
using Bic.Itau.Domain.MQ;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Bic.Itau.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMediator _mediatR;
        private readonly IMapper _mapper;
        private readonly ILogger<CustomersController> _logger;
        private readonly IMQCustomersPublisherItau _publisher;
        private readonly ICustomersMongoServiceItau _customersMongoServiceItau;

        public CustomersController(IMediator mediatR,
                                    ILogger<CustomersController> logger,
                                    IMQCustomersPublisherItau publisher,
                                    ICustomersMongoServiceItau customersMongoServiceItau,
                                    IMapper mapper)
        {
            _mediatR = mediatR;
            _logger = logger;
            _publisher = publisher;
            _customersMongoServiceItau = customersMongoServiceItau;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("itau-cielo")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCielo(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediatR.Send(customersDto);                     
            
            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceItau.CreateAsync<CustomersModel>(customerMap);
                await _publisher.PublisherCielo(customersDto);
                return Ok(_customersDto);
            }
        }

        [HttpPost]
        [Route("itau-getnet")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateGetNet(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediatR.Send(customersDto);

            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceItau.CreateAsync<CustomersModel>(customerMap);
                await _publisher.PublisherGetNet(customersDto);
                return Ok(_customersDto);
            }
        }

        [HttpPost]
        [Route("itau-mauabank")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateMauaBank(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediatR.Send(customersDto);

            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceItau.CreateAsync<CustomersModel>(customerMap);
                await _publisher.PublisherMauaBank(customersDto);
                return Ok(_customersDto);
            }
        }

        private async Task<CustomersModel> CreateCustomersMongo(CreateCustomersCommand customersDto)
        {

            var customersModelDto = new CustomersModelDto(customersDto.CnpjParameter,
                                                            customersDto.CodeHtml,
                                                            customersDto.CodeInternal,
                                                            customersDto.CNPJConsulted,
                                                            customersDto.NumberRegistration,
                                                            customersDto.NameBusiness,
                                                            customersDto.RegistrationStage,
                                                            customersDto.SupplierName,
                                                            customersDto.Address.UfParameter,
                                                                customersDto.Address.Logradouro,
                                                                customersDto.Address.Numero,
                                                                customersDto.Address.Complemento,
                                                                customersDto.Address.CEP,
                                                                 customersDto.Address.Bairro,
                                                                 customersDto.Address.Municipio,
                                                                 customersDto.Address.UF,
                                                                 customersDto.Address.CodeIbge,
                                                                 customersDto.Address.CustomersId
                                                            )
            {
                CnpjParameter = customersDto.CnpjParameter,
                CodeHtml =customersDto.CodeHtml,
                CodeInternal = customersDto.CodeInternal,
                CNPJConsulted =customersDto.CNPJConsulted,
                NumberRegistration = customersDto.NumberRegistration,
                NameBusiness =customersDto.NameBusiness,
                RegistrationStage =customersDto.RegistrationStage,
                SupplierName = customersDto.SupplierName,
                AddressMongoModelDtos = new AddressMongoModelDto(customersDto.Address.UfParameter,
                                                                customersDto.Address.Logradouro,
                                                                customersDto.Address.Numero,
                                                                customersDto.Address.Complemento,
                                                                customersDto.Address.CEP,
                                                                 customersDto.Address.Bairro,
                                                                 customersDto.Address.Municipio,
                                                                 customersDto.Address.UF,
                                                                 customersDto.Address.CodeIbge,
                                                                 customersDto.Address.CustomersId)
                {
                    UfParameter = customersDto.Address.UfParameter,
                    Logradouro = customersDto.Address.Logradouro,
                    Numero = customersDto.Address.Numero,
                    Complemento = customersDto.Address.Complemento,
                    CEP = customersDto.Address.CEP,
                    Bairro = customersDto.Address.Bairro,
                    Municipio = customersDto.Address.Municipio,                    
                    UF = customersDto.Address.UF,
                    CodeIbge = customersDto.Address.CodeIbge,
                    CustomersId = customersDto.Address.CustomersId
                }

            };
            var customerMap = _mapper.Map<CustomersModel>(customersModelDto);
            return customerMap;
        }
    }
}
