using Bic.Itau.CrossCutting.AppSettings;
using Bic.Itau.CrossCutting.Dependency;
using Bic.Itau.CrossCutting.Extension;
using Bic.Itau.CrossCutting.Mapper;
using Bic.Itau.CrossCutting.Mongo.Services;
using Bic.Itau.Infrastructure.ContextMongoDb;
using Bic.Itau.Infrastructure.Mapper;
using Bic.Itau.Schedule;
using Hangfire;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers()
    .AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpClientItau();
builder.Services.ConfigureServices(builder.Configuration);
builder.Services.AddInjection(builder.Configuration);

builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Bic.Itau.Api",
        Version = "v1",
        Contact = new OpenApiContact
        {
            Name = "Ita�",
            Email = "contato@teste.com.br",
            Url = new Uri("https://google.com")
        }
    });

    var xmlFile = "DocumentingSwagger.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    //c.IncludeXmlComments(xmlPath);
});
builder.Services.Configure<RouteOptions>(options =>
{
    options.LowercaseUrls = true;
    options.LowercaseQueryStrings = true;
});
builder.Services.AddHangfireServer();
builder.Services.AddAutoMapper(typeof(ProfileMapper));
builder.Services.AddAutoMapper(typeof(ProfileMapperCrossCutting));
builder.Services.AddHostedService<ScheduleItau>();
builder.Services.AddHangfireServices(builder.Configuration);

builder.Services.Configure<DataBaseSettingsNoSql>(
builder.Configuration.GetSection("DataBaseSettingsNoSql"));
builder.Services.AddSingleton<CustomersMongoServiceItau>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.MapHangfireDashboard("/hangfire-itau");
app.Run();
