﻿namespace Bic.Itau.Domain.MQ
{
    public interface IMQCustomersPublisherItau
    {
        Task PublisherCielo<T>(T message);
        Task PublisherGetNet<T>(T message);
        Task PublisherMauaBank<T>(T message);
    }
}
