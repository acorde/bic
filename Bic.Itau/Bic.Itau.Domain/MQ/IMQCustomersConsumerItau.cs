﻿namespace Bic.Itau.Domain.MQ
{
    public interface IMQCustomersConsumerItau
    {
        Task ConsumerCielo();
        Task ConsumerGetNet();
        Task ConsumerMauaBank();
        Task ConsumerMessageSXPAmericanas();
        Task ConsumerMessageSXPRenner();
        Task ConsumerMessageSXPAssai();
    }
}
