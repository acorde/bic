﻿using Bic.Itau.Domain.Model;

namespace Bic.Itau.Domain.Interfaces
{
    public interface ICustomersRepository
    {
        Task<IEnumerable<Customers>> GetCustomers();
        Task<Customers> GetCustomersById(int id);
        Task<Customers> AddCustomers(Customers customer);
        void UpdateCustomers(Customers customer);
        Task<Customers> DeleteCustomers(int id);
    }
}
