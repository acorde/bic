﻿using System.Text.Json.Serialization;

namespace Bic.Itau.Domain.Model
{
    public sealed class Address : EntityBase
    {
        [JsonPropertyName("ufParameter")]
        public string? UfParameter { get; set; }
        [JsonPropertyName("logradouro")]
        public string? Logradouro { get; set; }
        [JsonPropertyName("numero")]
        public string? Numero { get; set; }
        [JsonPropertyName("complemento")]
        public string? Complemento { get; set; }
        [JsonPropertyName("cep")]
        public string? CEP { get; set; }
        [JsonPropertyName("bairro")]
        public string? Bairro { get; set; }
        [JsonPropertyName("municipio")]
        public string? Municipio { get; set; }
        [JsonPropertyName("uf")]
        public string? UF { get; set; }
        [JsonPropertyName("codeIbge")]
        public string? CodeIbge { get; set; }
        [JsonPropertyName("customersId")]
        public int CustomersId { get; set; }
    }
}
