﻿using Bic.Itau.Domain.Model.Configuration.QueueName.Rabbit;

namespace Bic.Itau.Domain.Model
{
    public sealed class SettingsConfiguration
    {
        public bool? Log { get; set; }
        public string? ConnectionStringId { get; set; }
        public UriSettings? UriSettings { get; set; }
        public RabbitSettings RabbitSettings { get; set; }
        public RabbitQueueName RabbitQueueName { get; set; }
    }
}
