﻿namespace Bic.Itau.Domain.Model
{
    public class RabbitSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
