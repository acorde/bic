﻿namespace Bic.Itau.Domain.Model.Configuration.QueueName.Rabbit.Dlq
{
    public class QueueDlqThato
    {
        public string Cielo { get; set; }
        public string GetNet { get; set; }
        public string MauaBank { get; set; }
    }
}
