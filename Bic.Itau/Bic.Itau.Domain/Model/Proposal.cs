﻿namespace Bic.Itau.Domain.Model
{
    public sealed class Proposal
    {
        
        public Proposal(int id, decimal? valor, int? customersId, DateTime? updatedAt, DateTime createdAt)
        {
            Id = id;
            Valor = valor;
            CustomersId = customersId;
            UpdatedAt = DateTimeOffset.Now.DateTime;
            CreatedAt = createdAt;
        }

        public int? Id { get; set; }
        public decimal? Valor { get; set; }
        public int? CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; } 

    }
}
