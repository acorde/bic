﻿using System.Text.Json.Serialization;

namespace Bic.Itau.Domain.Model
{
    public sealed class Customers : EntityBase
    {
        public Customers()
        {
            Address = new Address();
        }
        [JsonPropertyName("codeHtml")]
        public string? CodeHtml { get; set; }
        [JsonPropertyName("codeInternal")]
        public string? CodeInternal { get; set; }
        [JsonPropertyName("cnpjParameter")]
        public string? CnpjParameter { get; set; }
        [JsonPropertyName("cnpjConsulted")]
        public string? CNPJConsulted { get; set; }
        [JsonPropertyName("numberRegistration")]
        public string? NumberRegistration { get; set; }
        [JsonPropertyName("nameBusiness")]
        public string? NameBusiness { get; set; }
        [JsonPropertyName("registrationStage")]
        public string? RegistrationStage { get; set; }
        [JsonPropertyName("supplierName")]
        public string? SupplierName { get; set; }
        [JsonPropertyName("address")]
        public Address? Address { get; set; }
        public List<Proposal?> Proposals {  get; set; }
        
        public Customers(string? codeHtml, string? codeInternal, string? cnpjParameter, string? cnpjConsulted
            , string? numberRegistration, string? nameBusiness, string? registrationStage, string? supplierName, 
            Address? address, List<Proposal?> proposals)
        {

            CodeHtml = codeHtml;
            CodeInternal = codeInternal;
            CnpjParameter = cnpjParameter;
            CNPJConsulted = cnpjConsulted;
            NumberRegistration = numberRegistration;
            NameBusiness = nameBusiness;
            RegistrationStage = registrationStage;
            SupplierName = supplierName;
            Address = address;
            Proposals = proposals;
        }
    }
}
