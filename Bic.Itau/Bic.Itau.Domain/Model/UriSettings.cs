﻿namespace Bic.Itau.Domain.Model
{
    public sealed class UriSettings
    {
        public bool? IsProduction { get; set; }
        public string UriThatoCielo { get; set; }
        public string UriThatoGetNet { get; set; }
        public string UriThatoMauaBank { get; set; }
        public string UriCardItauAmericanas { get; set; }
        public string UriCardItauAssai { get; set; }
        public string UriCardItauRenner { get; set; }
    }
}
