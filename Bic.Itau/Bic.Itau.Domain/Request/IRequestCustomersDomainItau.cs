﻿using Bic.Itau.Domain.Model;

namespace Bic.Itau.Domain.Request
{
    public interface IRequestCustomersDomainItau
    {
        public Task<Customers> SendGetNet(Customers model);
        public Task<Customers> SendCielo(Customers model);
        public Task<Customers> SendMauaBank(Customers model);
        public Task<Customers> SendAmericanas(Customers model);
        public Task<Customers> SendRenner(Customers model);
        public Task<Customers> SendAssai(Customers model);
    }
}
