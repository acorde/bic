﻿using Bic.Itau.Domain.Model;

namespace Bic.Ita.Domain.ResponseModel
{
    public class ResponseCustomers
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Customers Customers { get; set; }
    }
}
