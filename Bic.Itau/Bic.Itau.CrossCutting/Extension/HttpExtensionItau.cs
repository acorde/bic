﻿using Microsoft.Extensions.DependencyInjection;
using Polly;

namespace Bic.Itau.CrossCutting.Extension
{
    public static class HttpExtensionItau
    {
        public static IServiceCollection AddHttpClientItau(this IServiceCollection services)
        {
            services.AddHttpClient("httpsClientBus", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressBus", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressCielo", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressGetNet", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressMauaBank", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            return services;
        }
    }
}
