﻿using Bic.Itau.CrossCutting.Mongo.Interfaces;
using Bic.Itau.CrossCutting.Mongo.Services;
using Bic.Itau.Domain.Interfaces;
using Bic.Itau.Domain.MQ;
using Bic.Itau.Domain.Request;
using Bic.Itau.Infrastructure.Context;
using Bic.Itau.Infrastructure.MQ;
using Bic.Itau.Infrastructure.Repository;
using Bic.Itau.Infrastructure.Request;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Bic.Itau.CrossCutting.Dependency
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInjection(this IServiceCollection services, IConfiguration configuration)
        {
            var myOracleConnection = configuration.GetConnectionString("ConnectionOracle");
            services.AddDbContext<AppDbContext>(options =>
                                                options.UseOracle(myOracleConnection).LogTo(Console.WriteLine, LogLevel.Information));
            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWorkRepository>();

            services.AddSingleton<IRequestCustomersDomainItau, RequestCustomersItau>();
            services.AddScoped<IMQCustomersPublisherItau, MQCustomersPublisherItau>();
            services.AddSingleton<IMQCustomersConsumerItau, MQCustomersConsumerItau>();

            services.AddScoped<ICustomersMongoServiceItau, CustomersMongoServiceItau>();

            var myhandlers = AppDomain.CurrentDomain.Load("Bic.Itau.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(myhandlers));
            return services;
        }
    }
}
