﻿using AutoMapper;
using Bic.Itau.CrossCutting.ModelMongo;

namespace Bic.Itau.CrossCutting.Mapper
{
    public class ProfileMapperCrossCutting : Profile
    {
        public ProfileMapperCrossCutting()
        {
            CreateMap<CustomersModel, CustomersModelDto>();
            CreateMap<CustomersModelDto, CustomersModel>();
            CreateMap<AddressMongoModelDto, AddressMongoModel>();
            CreateMap<AddressMongoModel, AddressMongoModelDto>();

            //CreateMap<List<CustomersModel>, List<CustomersModelDto>>();
            //CreateMap<List<CustomersModelDto>, List<CustomersModel>>();
            //CreateMap<List<AddressMongoModelDto>, AddressMongoModel>();
            //CreateMap<AddressMongoModel, List<AddressMongoModelDto>>();

            //CreateMap<AddressMongoModelDto, List<AddressMongoModel>>();
            //CreateMap<AddressMongoModel, List<AddressMongoModelDto>>();
        }
    }
}
