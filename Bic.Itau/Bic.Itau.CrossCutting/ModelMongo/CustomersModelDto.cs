﻿using MongoDB.Bson.Serialization.Attributes;
using System.Text.Json.Serialization;

namespace Bic.Itau.CrossCutting.ModelMongo
{
    public class CustomersModelDto : CustomersMongoModelDtoBase
    {
        public CustomersModelDto(string cnpjParameter,
                                 string codeHtml,
                                 string codeInternal,
                                 string cNPJConsulted,
                                 string numberRegistration,
                                 string nameBusiness,
                                 string registrationStage,
                                 string supplierName,
                                 string ufParameter,
                                 string logradouro,
                                 string numero,
                                 string complemento,
                                 string cEP,
                                 string bairro,
                                 string municipio,
                                 string uF,
                                 string codeIbge,
                                 int? customersId)
        {
            CnpjParameter = cnpjParameter;
            CodeHtml = codeHtml;
            CodeInternal = codeInternal;
            CNPJConsulted = cNPJConsulted;
            NumberRegistration = numberRegistration;
            NameBusiness = nameBusiness;
            RegistrationStage = registrationStage;
            SupplierName = supplierName;             
            UfParameter = ufParameter;
            Logradouro = logradouro;
            Numero = numero;
            Complemento = complemento;
            CEP = cEP;
            Bairro = bairro;
            Municipio = municipio;
            UF = uF;
            CodeIbge = codeIbge;
            CustomersId = customersId;
            AddressMongoModelDtos = new AddressMongoModelDto();
        }

        public string CnpjParameter { get; set; }
        public string CodeHtml { get; set; }
        public string CodeInternal { get; set; } 
        public string CNPJConsulted { get; set; } 
        public string NumberRegistration { get; set; }
        public string NameBusiness { get; set; } 
        public string RegistrationStage { get; set; } 
        public string SupplierName { get; set; } 
       
        public AddressMongoModelDto AddressMongoModelDtos { get; set; }
        //[BsonElement("Address")]
        public string UfParameter { get; set; } 
        public string Logradouro { get; set; } 
        public string Numero { get; set; } 
        public string Complemento { get; set; } 
        public string CEP { get; set; } 
        public string Bairro { get; set; } 
        public string Municipio { get; set; } 
        public string UF { get; set; } 
        public string CodeIbge { get; set; } 
        public int? CustomersId { get; set; }

    }

    public class AddressMongoModelDto
    {
        public AddressMongoModelDto()
        {
        }

        public AddressMongoModelDto(string ufParameter, string logradouro, string numero, string complemento, string cEP, string bairro, string municipio, string uF, string codeIbge, int? customersId)
        {
            UfParameter = ufParameter;
            Logradouro = logradouro;
            Numero = numero;
            Complemento = complemento;
            CEP = cEP;
            Bairro = bairro;
            Municipio = municipio;
            UF = uF;
            CodeIbge = codeIbge;
            CustomersId = customersId;
        }
        public string UfParameter { get; set; } = null!;
        public string Logradouro { get; set; } = null!;
        public string Numero { get; set; } = null!;
        public string Complemento { get; set; } = null!;
        public string CEP { get; set; } = null!;
        public string Bairro { get; set; } = null!;
        public string Municipio { get; set; } = null!;
        public string UF { get; set; } = null!;
        public string CodeIbge { get; set; } = null!;
        public int? CustomersId { get; set; }
    }
}
