﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Text.Json.Serialization;

namespace Bic.Itau.CrossCutting.ModelMongo
{
    public abstract class CustomersMongoModelDtoBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }       

    }
}
