﻿using Bic.Itau.Infrastructure.ContextMongoDb;

namespace Bic.Itau.CrossCutting.ModelMongo
{
    public class CustomersModel : CustomersMongoModelBase
    {
        public CustomersModel(string cnpjParameter,
                              string codeHtml,
                              string codeInternal,
                              string cNPJConsulted,
                              string numberRegistration,
                              string nameBusiness,
                              string registrationStage,
                              string supplierName,
                              string ufParameter,
                              string logradouro,
                              string numero,
                              string complemento,
                              string cEP,
                              string bairro,
                              string municipio,
                              string uF,
                              string codeIbge,
                              int? customersId)
        {
            CnpjParameter = cnpjParameter;
            CodeHtml = codeHtml;
            CodeInternal = codeInternal;
            CNPJConsulted = cNPJConsulted;
            NumberRegistration = numberRegistration;
            NameBusiness = nameBusiness;
            RegistrationStage = registrationStage;
            SupplierName = supplierName;
            AddressMongoModels = new AddressMongoModel();
            UfParameter = ufParameter;
            Logradouro = logradouro;
            Numero = numero;
            Complemento = complemento;
            CEP = cEP;
            Bairro = bairro;
            Municipio = municipio;
            UF = uF;
            CodeIbge = codeIbge;
            CustomersId = customersId;
        }

        public string CnpjParameter { get; set; }
        public string CodeHtml { get; set; }
        public string CodeInternal { get; set; }
        public string CNPJConsulted { get; set; }
        public string NumberRegistration { get; set; }
        public string NameBusiness { get; set; }
        public string RegistrationStage { get; set; }
        public string SupplierName { get; set; }
        
        public AddressMongoModel AddressMongoModels { get; set; }
        //[BsonElement("Address")]
        public string UfParameter { get; set; } 
        public string Logradouro { get; set; }
        public string Numero { get; set; } 
        public string Complemento { get; set; } 
        public string CEP { get; set; } 
        public string Bairro { get; set; } 
        public string Municipio { get; set; } 
        public string UF { get; set; } 
        public string CodeIbge { get; set; } 
        public int? CustomersId { get; set; }
    }

    public class AddressMongoModel
    {
        public AddressMongoModel()
        {
        }

        public AddressMongoModel(string ufParameter, string logradouro, string numero, string complemento, string cEP, string bairro, string municipio, string uF, string codeIbge, int? customersId)
        {
            UfParameter = ufParameter;
            Logradouro = logradouro;
            Numero = numero;
            Complemento = complemento;
            CEP = cEP;
            Bairro = bairro;
            Municipio = municipio;
            UF = uF;
            CodeIbge = codeIbge;
            CustomersId = customersId;
        }
        public string UfParameter { get; set; } 
        public string Logradouro { get; set; } 
        public string Numero { get; set; } 
        public string Complemento { get; set; } 
        public string CEP { get; set; } 
        public string Bairro { get; set; } 
        public string Municipio { get; set; } 
        public string UF { get; set; } 
        public string CodeIbge { get; set; } 
        public int? CustomersId { get; set; }
    }
}
