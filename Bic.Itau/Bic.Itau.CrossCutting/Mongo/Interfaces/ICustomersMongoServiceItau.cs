﻿using Bic.Itau.CrossCutting.ModelMongo;

namespace Bic.Itau.CrossCutting.Mongo.Interfaces
{
    public interface ICustomersMongoServiceItau
    {
        Task CreateAsync<T>(CustomersModel newBook);
    }
}
