﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Bic.Itau.CrossCutting.ModelMongo;
using Bic.Itau.CrossCutting.Mongo.Interfaces;
using Bic.Itau.Infrastructure.ContextMongoDb;

namespace Bic.Itau.CrossCutting.Mongo.Services
{
    public class CustomersMongoServiceItau : ICustomersMongoServiceItau
    {
        private readonly IMongoCollection<CustomersModel> _mongoCollection;

        public CustomersMongoServiceItau(IOptions<DataBaseSettingsNoSql> dataBaseSettingsNoSql)
        {
            var mongoClient = new MongoClient(dataBaseSettingsNoSql.Value.HostCorban);
            var mongoDatabase = mongoClient.GetDatabase(dataBaseSettingsNoSql.Value.DatabaseName);
            _mongoCollection = mongoDatabase.GetCollection<CustomersModel>(dataBaseSettingsNoSql.Value.CollectionNameSXPItau);
        }

        public async Task CreateAsync<T>(CustomersModel newBook) =>
            await _mongoCollection.InsertOneAsync(newBook);

    }
}