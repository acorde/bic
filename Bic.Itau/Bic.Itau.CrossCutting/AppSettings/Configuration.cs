﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Bic.Itau.Domain.Model;

namespace Bic.Itau.CrossCutting.AppSettings
{
    public static class Configuration
    {
        public static void AddConfiguration<T>(this IServiceCollection services, IConfiguration configuration, string? configurationTag = null) where T : class
        {
            if (string.IsNullOrEmpty(configurationTag))
            {
                configurationTag = typeof(T).Name;
            }

            var instance = Activator.CreateInstance<T>();
            new ConfigureFromConfigurationOptions<T>(configuration.GetSection(configurationTag)).Configure(instance);
            services.AddSingleton(instance);
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConfiguration<SettingsConfiguration>(configuration, "MySettings");
            return services;
        }
    }
}
