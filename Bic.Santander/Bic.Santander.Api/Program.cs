using Bic.Santander.Application.Mapper;
using Bic.Santander.CrossCutting;
using Bic.Santander.CrossCutting.AppSettings;
using Bic.Santander.CrossCutting.Extension;
using Bic.Santander.CrossCutting.Mapper;
using Bic.Santander.CrossCutting.Mongo.Services;
using Bic.Santander.Infrastructure.ContextMongoDb;
using Bic.Santander.Schedule;
using Hangfire;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers()
    .AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpClientSantander();
builder.Services.ConfigureServices(builder.Configuration);
builder.Services.AddInjection(builder.Configuration);
builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Bic.Santander.Api",
        Version = "v1",
        Contact = new OpenApiContact
        {
            Name = "Santander",
            Email = "contato@teste.com.br",
            Url = new Uri("https://google.com")
        }
    });

    var xmlFile = "DocumentingSwagger.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    //c.IncludeXmlComments(xmlPath);
});
builder.Services.Configure<RouteOptions>(options =>
{
    options.LowercaseUrls = true;
    options.LowercaseQueryStrings = true;
});
builder.Services.AddHangfireServer();
builder.Services.AddAutoMapper(typeof(ProfileMapper));
builder.Services.AddAutoMapper(typeof(ProfileMapperCrossCutting));
builder.Services.AddHostedService<ScheduleSantander>();
builder.Services.AddHangfireServices(builder.Configuration);

builder.Services.Configure<DataBaseSettingsNoSql>(
builder.Configuration.GetSection("DataBaseSettingsNoSql"));
builder.Services.AddSingleton<CustomersMongoServiceSantander>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.MapHangfireDashboard("/hangfire-santander");
app.Run();
