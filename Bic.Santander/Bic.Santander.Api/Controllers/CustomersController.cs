﻿using AutoMapper;
using Bic.Santander.Application.CQRS.Customer.Command;
using Bic.Santander.CrossCutting.ModelMongo;
using Bic.Santander.CrossCutting.Mongo.Interfaces;
using Bic.Santander.Domain.Interfaces.MQ;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Bic.Santander.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IMediator _mediatR;
        private readonly IMapper _mapper;
        private readonly ILogger<CustomersController> _logger;
        private readonly IMQCustomersPublisherSantander _publisher;
        private readonly ICustomersMongoServiceSantander _customersMongoServiceSantander;

        public CustomersController(IMediator mediatR, 
                                ILogger<CustomersController> logger, 
                                IMQCustomersPublisherSantander publisherSantander, 
                                ICustomersMongoServiceSantander customersMongoServiceSantander, 
                                IMapper mapper)
        {
            _mediatR = mediatR;
            _logger = logger;
            _publisher = publisherSantander;
            _customersMongoServiceSantander = customersMongoServiceSantander;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("santander")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> Add(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediatR.Send(customersDto);

            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceSantander.CreateAsync<CustomersModel>(customerMap);
                await _publisher.PublisherCielo(customersDto);
                return Ok(_customersDto);
            }
        }

        [HttpPost]
        [Route("santander-getnet")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateSantanderGetNet(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediatR.Send(customersDto);

            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceSantander.CreateAsync<CustomersModel>(customerMap);
                await _publisher.PublisherGetNet(customersDto);
                return Ok(_customersDto);
            }
        }

        [HttpPost]
        [Route("santander-mauabank")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateSantanderMauaBank(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediatR.Send(customersDto);

            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceSantander.CreateAsync<CustomersModel>(customerMap);
                await _publisher.PublisherMauaBank(customersDto);
                return Ok(_customersDto);
            }
        }

        private async Task<CustomersMongoModel> CreateCustomersMongo(CreateCustomersCommand customersDto)
        {
            CustomersMongoModelDto customersMongoModelDtoMap = new CustomersMongoModelDto
            {
                CnpjParameter = customersDto.CnpjParameter,
                CodeHtml = customersDto.CodeHtml,
                CodeInternal = customersDto.CodeInternal,
                CNPJConsulted = customersDto.CNPJConsulted,
                NumberRegistration = customersDto.NumberRegistration,
                NameBusiness = customersDto.NameBusiness,
                RegistrationStage = customersDto.RegistrationStage,
                SupplierName = customersDto.SupplierName,
                AddressMongoModelDtos =
                    new AddressMongoModelDto()
                    {
                        Bairro = customersDto.Address.Bairro,
                        CEP = customersDto.Address.CEP,
                        CodeIbge = customersDto.Address.CodeIbge,
                        Complemento = customersDto.Address.Complemento,
                        Logradouro = customersDto.Address.Logradouro,
                        Municipio = customersDto.Address.Municipio,
                        Numero = customersDto.Address.Numero,
                        UF = customersDto.Address.UF,
                        UfParameter = customersDto.Address.UfParameter
                    }
            };

            var customerMap = _mapper.Map<CustomersMongoModel>(customersMongoModelDtoMap);
            return customerMap;
        }
    }

    
}
