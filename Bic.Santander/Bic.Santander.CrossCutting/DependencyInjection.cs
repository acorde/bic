﻿using Bic.Santander.CrossCutting.Mongo.Interfaces;
using Bic.Santander.CrossCutting.Mongo.Services;
using Bic.Santander.Domain.Interfaces;
using Bic.Santander.Domain.Interfaces.MQ;
using Bic.Santander.Domain.Request;
using Bic.Santander.Infrastructure.Context;
using Bic.Santander.Infrastructure.MQ;
using Bic.Santander.Infrastructure.Repository;
using Bic.Santander.Infrastructure.Request;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Bic.Santander.CrossCutting
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInjection(this IServiceCollection services, IConfiguration configuration)
        {
            var myOracleConnection = configuration.GetConnectionString("ConnPostgres");
            services.AddDbContext<AppDbContext>(options =>
                                                options.UseNpgsql(myOracleConnection).LogTo(Console.WriteLine, LogLevel.Information));
            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWorkRepository>();

            services.AddSingleton<IRequestCustomersDomainSantander, RequestCustomersSantander>();
            services.AddScoped<IMQCustomersPublisherSantander, MQCustomersPublisherSantander>();
            services.AddSingleton<IMQCustomersConsumerSantander, MQCustomersConsumerSantander>();

            services.AddScoped<ICustomersMongoServiceSantander, CustomersMongoServiceSantander>();

            var myhandlers = AppDomain.CurrentDomain.Load("Bic.Santander.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(myhandlers));
            return services;
        }
    }
}
