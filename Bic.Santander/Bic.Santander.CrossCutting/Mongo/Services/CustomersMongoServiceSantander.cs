﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Bic.Santander.CrossCutting.ModelMongo;
using Bic.Santander.CrossCutting.Mongo.Interfaces;
using Bic.Santander.Infrastructure.ContextMongoDb;

namespace Bic.Santander.CrossCutting.Mongo.Services
{
    public class CustomersMongoServiceSantander : ICustomersMongoServiceSantander
    {
        private readonly IMongoCollection<CustomersMongoModel> _mongoCollection;

        public CustomersMongoServiceSantander(IOptions<DataBaseSettingsNoSql> dataBaseSettingsNoSql)
        {
            var mongoClient = new MongoClient(dataBaseSettingsNoSql.Value.HostCorban);
            var mongoDatabase = mongoClient.GetDatabase(dataBaseSettingsNoSql.Value.DatabaseName);
            _mongoCollection = mongoDatabase.GetCollection<CustomersMongoModel>(dataBaseSettingsNoSql.Value.CollectionNameSXPSantander);
        }

        public async Task CreateAsync<T>(CustomersMongoModel newBook) =>
            await _mongoCollection.InsertOneAsync(newBook);

    }
}