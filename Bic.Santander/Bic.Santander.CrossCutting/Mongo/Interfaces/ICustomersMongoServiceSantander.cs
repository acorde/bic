﻿using Bic.Santander.CrossCutting.ModelMongo;

namespace Bic.Santander.CrossCutting.Mongo.Interfaces
{
    public interface ICustomersMongoServiceSantander
    {
        Task CreateAsync<T>(CustomersMongoModel newBook);
    }
}
