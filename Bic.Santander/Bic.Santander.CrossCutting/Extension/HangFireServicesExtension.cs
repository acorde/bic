﻿using Hangfire;
using Hangfire.Console;
using Hangfire.Redis.StackExchange;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace Bic.Santander.CrossCutting.Extension
{
    public static class HangFireServicesExtension
    {
        public static IServiceCollection AddHangfireServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHangfire(options =>
            {
                var connectionString = configuration.GetValue<string>("ConnRedisSXPSantander");
                var redis = ConnectionMultiplexer.Connect(connectionString);
                options.UseRedisStorage(redis, options: new RedisStorageOptions { Prefix = $"HANG_FIRE" });
                options.UseConsole();
            });
            return services;
        }
    }
}
