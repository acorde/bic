﻿using Microsoft.Extensions.DependencyInjection;

namespace Bic.Santander.CrossCutting.Extension
{
    public static class HttpExtensionSantander
    {
        public static IServiceCollection AddHttpClientSantander(this IServiceCollection services)
        {
            services.AddHttpClient("BaseAddressCielo", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            });

            services.AddHttpClient("BaseAddressGetNet", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            });

            services.AddHttpClient("BaseAddressMauaBank", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7065/");
            });

            return services;
        }
    }
}
