﻿using AutoMapper;
using Bic.Santander.CrossCutting.ModelMongo;

namespace Bic.Santander.CrossCutting.Mapper
{
    public class ProfileMapperCrossCutting : Profile
    {
        public ProfileMapperCrossCutting()
        {
            CreateMap<CustomersMongoModel, CustomersMongoModelDto>();
            CreateMap<CustomersMongoModelDto, CustomersMongoModel>();
            CreateMap<AddressMongoModelDto, AddressMongoModel>();
            CreateMap<AddressMongoModel, AddressMongoModelDto>();
        }
    }
}
