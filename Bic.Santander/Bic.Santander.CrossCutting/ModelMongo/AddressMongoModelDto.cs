﻿namespace Bic.Santander.CrossCutting.ModelMongo
{
    public class AddressMongoModelDto 
    {        
        public string UfParameter { get; set; } = null!;
        public string Logradouro { get; set; } = null!;
        public string Numero { get; set; } = null!;
        public string Complemento { get; set; } = null!;
        public string CEP { get; set; } = null!;
        public string Bairro { get; set; } = null!;
        public string Municipio { get; set; } = null!;
        public string UF { get; set; } = null!;
        public string CodeIbge { get; set; } = null!;
        public int CustomersId { get; set; }

    }
}

