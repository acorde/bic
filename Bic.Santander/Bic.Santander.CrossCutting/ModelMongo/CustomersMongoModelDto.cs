﻿using MongoDB.Bson.Serialization.Attributes;

namespace Bic.Santander.CrossCutting.ModelMongo;

public class CustomersMongoModelDto : CustomersMongoModelDtoBase
{
    public CustomersMongoModelDto()
    {
        AddressMongoModelDtos = new AddressMongoModelDto();
    }
    public string CodeHtml { get; set; } = null!;
    public string CodeInternal { get; set; } = null!;
    public string CNPJConsulted { get; set; } = null!;
    public string NumberRegistration { get; set; } = null!;
    public string NameBusiness { get; set; } = null!;
    public string RegistrationStage { get; set; } = null!;
    public string SupplierName { get; set; } = null!;
    [BsonElement("Address")]
    public AddressMongoModelDto AddressMongoModelDtos { get; set; }
}
