﻿using MongoDB.Bson.Serialization.Attributes;
using Bic.Santander.Infrastructure.ContextMongoDb;

namespace Bic.Santander.CrossCutting.ModelMongo
{
    public class CustomersMongoModel: CustomersMongoModelBase
    {
        public CustomersMongoModel()
        {
            AddressMongoModels = new AddressMongoModel();
        }
        public string CodeHtml { get; set; } = null!;
        public string CodeInternal { get; set; } = null!;
        public string CNPJConsulted { get; set; } = null!;
        public string NumberRegistration { get; set; } = null!;
        public string NameBusiness { get; set; } = null!;
        public string RegistrationStage { get; set; } = null!;
        public string SupplierName { get; set; } = null!;
        [BsonElement("Address")]
        public AddressMongoModel AddressMongoModels { get; set; }
    }
}
