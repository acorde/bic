﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bic.Santander.Domain.Model;

namespace Bic.Santander.Infrastructure.ModelsConfiguration
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.UfParameter);
            builder.Property(x => x.Logradouro);
            builder.Property(x => x.Numero);
            builder.Property(x => x.Complemento);
            builder.Property(x => x.CEP);
            builder.Property(x => x.Bairro);
            builder.Property(x => x.Municipio);
            builder.Property(x => x.UF);
            builder.Property(x => x.CodeIbge);

        }
    }
}
