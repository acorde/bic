﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bic.Santander.Domain.Model;

namespace Bic.Santander.Infrastructure.ModelsConfiguration
{
    public class CustomersConfiguration : IEntityTypeConfiguration<Customers>
    {
        public void Configure(EntityTypeBuilder<Customers> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(m => m.CodeHtml);
            builder.Property(m => m.CodeInternal);
            builder.Property(m => m.CnpjParameter);
            builder.Property(m => m.CNPJConsulted);
            builder.Property(m => m.NumberRegistration);
            builder.Property(m => m.NameBusiness);
            builder.Property(m => m.RegistrationStage);

            //builder.HasData(
            //    new Customers(1, "Maria", "Jopli", "feminino", "janes@email.com", true),
            //    new Customers(2, "Elvis", "Presley", "masculino", "elvis@email.com", true));
        }
    }
}
