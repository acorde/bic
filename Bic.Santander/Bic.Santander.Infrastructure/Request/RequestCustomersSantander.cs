﻿using Bic.Santander.Domain.Model;
using Bic.Santander.Domain.Request;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Text.Json;

namespace Bic.Santander.Infrastructure.Request
{
    public class RequestCustomersSantander : IRequestCustomersDomainSantander
    {
        private readonly ILogger<RequestCustomersSantander> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _configuration;

        public RequestCustomersSantander(IHttpClientFactory httpClientFactory,
                                            SettingsConfiguration configuration, 
                                            ILogger<RequestCustomersSantander> logger)
        {
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<Customers?> SendCielo(Customers model)
        {
            Customers? _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendCielo)} - Iniciando a requisição Http Post para gerar Cartão Americanas.");

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressCielo");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriAtentoCielo;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Americanas não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendCielo)} - Erro no request Http Post para gerar Cartão Americanas. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendGetNet(Customers model)
        {
            Customers? _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendGetNet)} - Iniciando a requisição Http Post para gerar Cartão Americanas.");

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressCielo");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriAtentoGetNet;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Americanas não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendGetNet)} - Erro no request Http Post para gerar Cartão Americanas. {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendMauaBank(Customers model)
        {
            Customers? _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendMauaBank)} - Iniciando a requisição Http Post para gerar Cartão Americanas.");

                var _httpClient = _httpClientFactory.CreateClient("BaseAddressCielo");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriAtentoMauaBank;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Americanas não retornou dados.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendMauaBank)} - Erro no request Http Post para gerar Cartão Americanas. {ex.Message}");
                return _customer;
            }
        }

        //public async Task<Customers> SendMauaBank(Customers model)
        //{
        //    Customers _customer = new Customers();
        //    try
        //    {
        //        _logger.LogInformation($"{nameof(SendMauaBank)} - Iniciando a requisição Http Post para gerar Cartão Assai.");

        //        //var options = new JsonSerializerOptions
        //        //{
        //        //    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        //        //    WriteIndented = true
        //        //};
        //        //var person = JsonSerializer.Serialize(model, options);

        //        var _httpClient = _httpClientFactory.CreateClient("BaseAddressMauaBank");
        //        var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
        //        var _uri = _configuration?.UriSettings?.UriAtentoMauaBank;

        //        using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

        //        if (_httpResponse.IsSuccessStatusCode)
        //        {
        //            var _result = await _httpResponse.Content.ReadAsStringAsync();
        //            _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
        //            return _customer;
        //        }
        //        else
        //        {
        //            _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Assai não retornou dados.");
        //            return _customer;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"{nameof(SendMauaBank)} - Erro no request Http Post para gerar Cartão Assai. {ex.Message}");
        //        return _customer;
        //    }
        //}

        //public async Task<Customers> SendGetNet(Customers model)
        //{
        //    Customers _customer = new Customers();
        //    try
        //    {
        //        _logger.LogInformation($"{nameof(SendGetNet)} - Iniciando a requisição Http Post para gerar Cartão Renner.");

        //        //var options = new JsonSerializerOptions
        //        //{
        //        //    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        //        //    WriteIndented = true
        //        //};
        //        //var person = JsonSerializer.Serialize(model, options);

        //        var _httpClient = _httpClientFactory.CreateClient("BaseAddressGetNet");
        //        var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
        //        var _uri = _configuration?.UriSettings?.UriAtentoGetNet;

        //        using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

        //        if (_httpResponse.IsSuccessStatusCode)
        //        {
        //            var _result = await _httpResponse.Content.ReadAsStringAsync();
        //            _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
        //            return _customer;
        //        }
        //        else
        //        {
        //            _logger.LogWarning($"Cuidado: Requisição Http Post para gerar Cartão Renner não retornou dados.");
        //            return _customer;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"{nameof(SendGetNet)} - Erro no request Http Post para gerar Cartão Renner. {ex.Message}");
        //        return _customer;
        //    }
        //}
    }
}
