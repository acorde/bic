﻿namespace Bic.Santander.Infrastructure.ContextMongoDb
{
    public class DataBaseSettingsNoSql
    {
        public string HostCorban { get; set; } = null!;        
        public string DatabaseName { get; set; } = null!;
        public string CollectionNameSXPSantander { get; set; } = null!;
    }
}
