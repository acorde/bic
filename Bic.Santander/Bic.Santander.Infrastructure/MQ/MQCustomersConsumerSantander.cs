﻿using Bic.Santander.Domain.Interfaces.MQ;
using Bic.Santander.Domain.Model;
using Bic.Santander.Domain.Request;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Bic.Santander.Infrastructure.MQ
{
    public class MQCustomersConsumerSantander : IMQCustomersConsumerSantander
    {
        private readonly ILogger<MQCustomersConsumerSantander> _logger;
        private readonly IRequestCustomersDomainSantander _request;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _settings;

        public MQCustomersConsumerSantander(ILogger<MQCustomersConsumerSantander> logger,
                                            IRequestCustomersDomainSantander request,
                                            SettingsConfiguration settings)
        {
            _logger = logger;
            _request = request;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _settings = settings;
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }


        public async Task ConsumerCielo()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings?.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.Cielo, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.Cielo
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _request.SendCielo(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.Cielo, false, consumer);

            });
        }

        public async Task ConsumerGetNet()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings?.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.GetNet, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings?.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.GetNet
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _request.SendGetNet(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag, false);
                        else
                            channel.BasicNack(ea.DeliveryTag, false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag, false, false);
                    }
                };
                channel.BasicConsume(queue: _settings?.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.GetNet, false, consumer);

            });
        }

        public async Task ConsumerMauaBank()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings?.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.MauaBank, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings?.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.MauaBank
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);

                        var result = await _request.SendMauaBank(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag, false);
                        else
                            channel.BasicNack(ea.DeliveryTag, false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag, false, false);
                    }
                };
                channel.BasicConsume(queue: _settings?.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.MauaBank, false, consumer);

            });
        }
    }
}
