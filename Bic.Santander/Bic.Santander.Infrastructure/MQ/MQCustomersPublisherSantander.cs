﻿using Bic.Santander.Domain.Interfaces.MQ;
using Bic.Santander.Domain.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Bic.Santander.Infrastructure.MQ
{
    public class MQCustomersPublisherSantander : IMQCustomersPublisherSantander
    {
        private readonly ILogger<MQCustomersPublisherSantander> _logger;
        private readonly SettingsConfiguration _settings;

        public MQCustomersPublisherSantander(ILogger<MQCustomersPublisherSantander> logger,
                        SettingsConfiguration settings)
        {
            _logger = logger;
            _settings = settings;
        }

        public static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task PublisherCielo<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings?.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherCielo)} - Início da Publicação da mensagem na fila: thato-itau-cielo.");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.Cielo, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.Cielo, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.Cielo, _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.Cielo, "");
                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.Cielo }
                        };
                    channel.QueueDeclare(queue: _settings?.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.Cielo, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings?.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.Cielo, null, body);
                    _logger.LogInformation($"{nameof(PublisherCielo)} - Fim da Publicação da mensagem na fila: thato-itau-cielo.");
                };
            });
        }

        public async Task PublisherGetNet<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings?.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherGetNet)} - Início da Publicação da mensagem na fila: atento santander GetNet.");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.GetNet, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.GetNet, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.GetNet, _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.GetNet, "");
                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.GetNet }
                        };
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.GetNet, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.GetNet, null, body);
                    _logger.LogInformation($"{nameof(PublisherGetNet)} - Fim da Publicação da mensagem na fila: atento santander GetNet.");
                };
            });
        }

        public async Task PublisherMauaBank<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings?.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherMauaBank)} - Início da Publicação da mensagem na fila: atento santander Maua Bank.");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.MauaBank, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.MauaBank, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.MauaBank, _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.MauaBank, "");
                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.MauaBank }
                        };
                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.MauaBank, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.MauaBank, null, body);
                    _logger.LogInformation($"{nameof(PublisherMauaBank)} - Fim da Publicação da mensagem na fila: atento santander Maua Bank.");
                };
            });
        }
    }
}
