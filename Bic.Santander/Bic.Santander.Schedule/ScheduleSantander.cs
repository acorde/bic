﻿using Bic.Santander.Domain.Interfaces.MQ;
using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Bic.Santander.Schedule
{

    public class ScheduleSantander : IHostedService
    {
        private readonly IMQCustomersConsumerSantander _customers;
        private readonly ILogger<ScheduleSantander> _logger;

        public ScheduleSantander(IMQCustomersConsumerSantander customers, 
                                    ILogger<ScheduleSantander> logger)
        {
            _customers = customers;
            _logger = logger;
        }


        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                RecurringJob.AddOrUpdate("atento-cielo", () => ConsumerCielo(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("atento-getnet", () => ConsumerGetNet(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("atento-mauabank", () => ConsumerMauaBank(), MinuteInterval(1));
            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerCielo()
        {
            try
            {
                await _customers.ConsumerCielo();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Atento Cielo Santander - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerGetNet()
        {
            try
            {
                await _customers.ConsumerGetNet();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Atento GetNet Santander - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerMauaBank()
        {
            try
            {
                await _customers.ConsumerMauaBank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Atento MauaBank Santander - Schedule: {ex.Message}.");
            }
        }
    }

}
