﻿using AutoMapper;
using Bic.Santander.Application.DTO;
using Bic.Santander.Domain.Model;

namespace Bic.Santander.Application.Mapper
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {
            CreateMap<ProposalDto, Proposal>();
            CreateMap<Proposal, ProposalDto>();
            CreateMap<CustomersDto, Customers>();
            CreateMap<Customers, CustomersDto>();
            CreateMap<AddressDto, Address>();
            CreateMap<Address, AddressDto>();
        }
    }
}
