﻿namespace Bic.Santander.Application.DTO
{
    public class ProposalDto
    {
        public int? Id { get; set; }
        public decimal? Valor { get; set; }
        public int? CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; } 
    }
}
