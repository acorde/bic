﻿namespace Santander.Bus.Domain.MQ
{
    public interface IMQCustomersConsumerSantander
    {
        Task ConsumerMessageCardItau();
    }
}
