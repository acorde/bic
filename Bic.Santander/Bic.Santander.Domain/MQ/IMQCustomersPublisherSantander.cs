﻿namespace Santander.Bus.Domain.MQ
{
    public interface IMQCustomersPublisherSantander
    {
        Task PublisherMessageCard<T>(T message);
    }
}
