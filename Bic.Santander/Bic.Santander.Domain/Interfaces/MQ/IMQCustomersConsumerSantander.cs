﻿namespace Bic.Santander.Domain.Interfaces.MQ
{
    public interface IMQCustomersConsumerSantander
    {
        Task ConsumerCielo();
        Task ConsumerGetNet();
        Task ConsumerMauaBank();
    }
}
