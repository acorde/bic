﻿namespace Bic.Santander.Domain.Interfaces.MQ
{
    public interface IMQCustomersPublisherSantander
    {
        Task PublisherCielo<T>(T message);
        Task PublisherGetNet<T>(T message);
        Task PublisherMauaBank<T>(T message);
    }
}
