﻿namespace Bic.Santander.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomersRepository CustomersRepository { get; }
        Task CommitAsync();
    }
}
