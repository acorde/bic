﻿using Bic.Santander.Domain.Model;

namespace Bic.Santander.Domain.Request
{
    public interface IRequestCustomersDomainSantander
    {
        Task<Customers> SendCielo(Customers model);
        Task<Customers> SendGetNet(Customers model);
        Task<Customers> SendMauaBank(Customers model);
    }
}
