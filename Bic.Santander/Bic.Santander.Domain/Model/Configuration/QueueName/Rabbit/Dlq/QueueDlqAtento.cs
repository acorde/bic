﻿namespace Bic.Santander.Domain.Model.Configuration.QueueName.Rabbit.Dlq
{
    public class QueueDlqAtento
    {
        public string? Cielo { get; set; }
        public string? GetNet { get; set; }
        public string? MauaBank { get; set; }
    }
}
