﻿using Bic.Santander.Domain.Model.Configuration.QueueName.Rabbit.Dlq;
using Bic.Santander.Domain.Model.Configuration.QueueName.Rabbit.Durable;

namespace Bic.Santander.Domain.Model.Configuration.QueueName.Rabbit
{
    public class RabbitQueueName
    {
        public QueueDurable? QueueDurable { get; set; }
        public QueueDlq? QueueDlq { get; set; }
    }
}
