﻿namespace Bic.Santander.Domain.Model
{
    public sealed class Customers : EntityBase
    {
        public Customers()
        {
            Address = new Address();
            Proposals = new  List<Proposal>();
        }

        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public Address? Address { get; set; }
        public List<Proposal>? Proposals { get; set; }

        public Customers(string? codeHtml, string? codeInternal, string? cnpjParameter, string? cnpjConsulted
            , string? numberRegistration, string? nameBusiness, string? registrationStage, string? supplierName, 
            Address? address, List<Proposal>? proposals)
        {

            CodeHtml = codeHtml;
            CodeInternal = codeInternal;
            CnpjParameter = cnpjParameter;
            CNPJConsulted = cnpjConsulted;
            NumberRegistration = numberRegistration;
            NameBusiness = nameBusiness;
            RegistrationStage = registrationStage;
            SupplierName = supplierName;
            Address = address;
            Proposals = proposals;
        }
    }
}
