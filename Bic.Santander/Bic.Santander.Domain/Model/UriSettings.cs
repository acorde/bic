﻿namespace Bic.Santander.Domain.Model
{
    public sealed class UriSettings
    {
        public bool? IsProduction { get; set; }
        public string? UriAtentoCielo { get; set; }
        public string? UriAtentoGetNet { get; set; }
        public string? UriAtentoMauaBank { get; set; }
    }
}
