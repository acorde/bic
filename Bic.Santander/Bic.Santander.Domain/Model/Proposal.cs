﻿namespace Bic.Santander.Domain.Model
{
    public class Proposal : EntityBase
    {
        public decimal? Valor { get; set; }
        public int? CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; } 

    }
}
