﻿using Bic.Santander.Domain.Model.Configuration.QueueName.Rabbit;

namespace Bic.Santander.Domain.Model
{
    public class SettingsConfiguration
    {
        public bool? Log { get; set; }
        public string? ConnectionStringId { get; set; }
        public UriSettings? UriSettings { get; set; }
        public RabbitSettings? RabbitSettings { get; set; }
        public RabbitQueueName? RabbitQueueName { get; set; }
    }
}
