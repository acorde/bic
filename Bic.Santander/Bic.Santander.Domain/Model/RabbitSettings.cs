﻿namespace Bic.Santander.Domain.Model
{
    public sealed class RabbitSettings
    {
        public string? Host { get; set; }
        public int Port { get; set; }
    }
}
