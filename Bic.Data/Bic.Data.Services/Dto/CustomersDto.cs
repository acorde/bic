﻿namespace Bic.B2b.Data.Services.Dto
{
    public class CustomersDto
    {
        public CustomersDto()
        {
            Address = new AddressDto();
            Proposals = new List<ProposalDto>();
        }
        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public AddressDto? Address { get; set; }
        public List<ProposalDto>? Proposals { get; set; }

        public CustomersDto(string? codeHtml, string? codeInternal, string? cnpjParameter, string? cnpjConsulted
            , string? numberRegistration, string? nameBusiness, string? registrationStage, string? supplierName, 
            AddressDto? addressDto, List<ProposalDto>? creditProposalDtos)
        {
            CodeHtml = codeHtml;
            CodeInternal = codeInternal;
            CnpjParameter = cnpjParameter;
            CNPJConsulted = cnpjConsulted;
            NumberRegistration = numberRegistration;
            NameBusiness = nameBusiness;
            RegistrationStage = registrationStage;
            SupplierName = supplierName;
            Address = addressDto;
            Proposals = creditProposalDtos;
        }
    }
}
