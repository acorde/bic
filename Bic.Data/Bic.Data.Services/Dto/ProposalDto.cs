﻿using System.Drawing;

namespace Bic.B2b.Data.Services.Dto
{
    public class ProposalDto
    {
        public ProposalDto()
        {
        }

        public ProposalDto(decimal? valor)
        {
            Valor = valor;
            
        }

        public ProposalDto(int id, int customersId, decimal? valor, DateTime? updatedAt, DateTime? createdAt)
        {
            Id = id;
            Valor = valor;
            CustomersId = customersId;
            UpdatedAt = updatedAt;
            CreatedAt = createdAt;
        }

        public int Id { get; set; }
        public decimal? Valor { get; set; }
        public int CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }

    }
}
