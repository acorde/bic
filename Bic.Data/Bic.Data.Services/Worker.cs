using AutoMapper;
using Bic.B2b.Data.Services.Dto;
using Bic.B2b.Data.Services.Model;
using Bic.Data.Services.Model;
using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SXP.Data.Services.Model;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using static Bic.Data.Services.EnumCorban;

namespace Bic.B2b.Data.Services
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly IMapper _mapper;
        private readonly SettingsConfiguration _settings;

        public Worker(ILogger<Worker> logger,
                        IConfiguration configuration,
                        IHttpClientFactory httpClientFactory,
                        IMapper mapper,
                        SettingsConfiguration settings)
        {
            _logger = logger;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _mapper = mapper;
            _settings = settings;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (_logger.IsEnabled(LogLevel.Information))
                {
                    _logger.LogInformation($"Leitura de arquivos em andamento: {DateTimeOffset.Now}");
                    var corbanThato = new Action(ProducerThato);
                    //var corbanThato = new Action(LerArquivoCorbanThato);
                    //var corbanSantander = new Action(LerArquivoCorbanSantander);
                    //var corbanAtento = new Action(LerArquivoCorbanAtentoBrasil);
                    var consumerRabbitMQ = new Action(Consumer);
                    var stopWatch = new Stopwatch();
                    stopWatch.Start();

                    //LerArquivoCorbanItau();
                    Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = 2 }, corbanThato, consumerRabbitMQ);
                    stopWatch.Stop();
                    Console.WriteLine($"O Tempo total de execu��o � {stopWatch.Elapsed.TotalMilliseconds} ms");
                    Thread.Sleep(20000);
                }
                await Task.Delay(60000, stoppingToken);
            }
        }

        private async Task<Root?> GetToken(string email)
        {
            _logger.LogInformation($"{nameof(GetToken)} - In�cio do m�todo {nameof(GetToken)} para obter o token JWT em {DateTimeOffset.Now}.");
            var _url = _configuration["Uri:UriToken"];
            var _httpClient = _httpClientFactory.CreateClient("clientB2bItau");
            _httpClient.DefaultRequestHeaders.Accept.Clear();

            //var _content = new StringContent(JsonSerializer.Serialize(userTokenModel.Email), Encoding.UTF8, "application/json");

            using var _httpResponse = await _httpClient.GetAsync(_url + "?email=" + email);
            var _result = await _httpResponse.Content.ReadAsStringAsync();
            Root? _token = JsonSerializer.Deserialize<Root?>(_result, _options);

            //if (_token.result == null)
            //    _logger.LogInformation($"{nameof(GetToken)} - Fim do m�todo {nameof(GetToken)}. Token JWT n�o gerado em {DateTimeOffset.Now}.");
            //else
            //    _logger.LogInformation($"{nameof(GetToken)} - Fim do m�todo {nameof(GetToken)}. {_token?.result.Sucesso} no retorno do token JWT em {DateTimeOffset.Now}.");

            if (_httpResponse.IsSuccessStatusCode)
            {
                _logger.LogInformation($"{nameof(GetToken)} - Fim do m�todo {nameof(GetToken)}. {_token?.result.Sucesso} no retorno do token JWT em {DateTimeOffset.Now}.");
            }
            else
            {
                _logger.LogInformation($"{nameof(GetToken)} - Fim do m�todo {nameof(GetToken)}. Token JWT n�o gerado em {DateTimeOffset.Now}.");
            }
            return _token;
        }

        private async Task<bool> SendCustomersThato(CustomersDto customerDto)
        {
            _logger.LogInformation($"{nameof(SendCustomersThato)} - In�cio do m�todo {nameof(SendCustomersThato)} para envio de dados em {DateTimeOffset.Now}.");
            var _customersMap = _mapper.Map<Customers>(customerDto);
            var _url = _configuration["Uri:UriItau"];
            var _httpClient = _httpClientFactory.CreateClient("clientB2bItau");

            Root? _token = await GetToken(_configuration["Email:EmailTatho"]);
            if (_token?.result == null)
            {
                _logger.LogError($"{nameof(SendCustomersThato)} - Erro: n�o foi poss�vel gerar o token JWT. Retorno: nulo do token JWT.");
                return false;
            }

            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.result.AccessToken);
            _logger.LogInformation($"{nameof(SendCustomersThato)} - Result: {_token.result.Sucesso} para o Token - {_token.result.AccessToken} gerado em {DateTimeOffset.Now}.");

            var _content = new StringContent(JsonSerializer.Serialize(_customersMap), Encoding.UTF8, "application/json");
            using var _httpResponse = await _httpClient.PostAsync(_url, _content);
            var _result = await _httpResponse.Content.ReadAsStringAsync();
            _logger.LogInformation($"{nameof(SendCustomersThato)} - Result: {_result} em {DateTimeOffset.Now}.");
            _logger.LogWarning($"{nameof(SendCustomersThato)} - Request do m�todo {nameof(SendCustomersThato)}. StatusCode: {_httpResponse.StatusCode} em {DateTimeOffset.Now}.");

            if (_httpResponse.IsSuccessStatusCode)
            {
                var _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                _logger.LogInformation($"{nameof(SendCustomersThato)} - Fim do m�todo {nameof(SendCustomersThato)} para envio de dados. Sucesso em {DateTimeOffset.Now}. Cliente: {_customer?.NameBusiness} - {_customer?.CnpjParameter}.");
                return true;
            }
            else
            {
                _logger.LogError($"{nameof(SendCustomersThato)} - Fim do m�todo {nameof(SendCustomersThato)} para envio de dados. Falha em {DateTimeOffset.Now}.");
                return false;
            }
        }

        private async Task<bool> SendCustomersAtento(CustomersDto customerDto)
        {
            _logger.LogInformation($"{nameof(SendCustomersAtento)} - In�cio do m�todo {nameof(SendCustomersAtento)} para envio de dados em {DateTimeOffset.Now}.");
            var _customersMap = _mapper.Map<Customers>(customerDto);
            var _url = _configuration["Uri:UriSantander"];
            var _httpClient = _httpClientFactory.CreateClient("clientB2bSantander");

            Root? _token = await GetToken(_configuration["Email:EmailAtento"]);
            if (_token?.result == null)
            {
                _logger.LogError($"{nameof(SendCustomersAtento)} - Erro: n�o foi poss�vel gerar o token JWT. Retorno: nulo do token JWT.");
                return false;
            }
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.result.AccessToken);
            var _content = new StringContent(JsonSerializer.Serialize(_customersMap), Encoding.UTF8, "application/json");
            using var _httpResponse = await _httpClient.PostAsync(_url, _content);
            var _result = await _httpResponse.Content.ReadAsStringAsync();
            _logger.LogInformation($"{nameof(SendCustomersAtento)} - Result: {_result} em {DateTimeOffset.Now}.");
            _logger.LogWarning($"{nameof(SendCustomersAtento)} - Request do m�todo {nameof(SendCustomersAtento)}. StatusCode: {_httpResponse.StatusCode} em {DateTimeOffset.Now}.");
            if (_httpResponse.IsSuccessStatusCode)
            {
                var _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                _logger.LogInformation($"{nameof(SendCustomersAtento)} - Fim do m�todo {nameof(SendCustomersAtento)} para envio de dados. Sucesso em {DateTimeOffset.Now}. Cliente: {_customer?.NameBusiness} - {_customer?.CnpjParameter}.");
                return true;
            }
            else
            {
                var _customer = JsonSerializer.Deserialize<ProblemDetails>(_result, _options);
                _logger.LogError($"{nameof(SendCustomersAtento)} - Fim do m�todo {nameof(SendCustomersAtento)} para envio de dados. Falha em {DateTimeOffset.Now}.");
                return false;
            }
        }

        private async Task<bool> SendCustomersCallLink(CustomersDto customerDto)
        {
            var _customersMap = _mapper.Map<Customers>(customerDto);
            var _url = _configuration["Uri:UriBradesco"];
            var _httpClient = _httpClientFactory.CreateClient("clientB2bBradesco");

            Root _token = await GetToken(_configuration["Email:EmailCallLink"].ToString());
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.result.AccessToken);
            var _content = new StringContent(JsonSerializer.Serialize(_customersMap), Encoding.UTF8, "application/json");
            using var _httpResponse = await _httpClient.PostAsync(_url, _content);
            var _result = await _httpResponse.Content.ReadAsStringAsync();

            if (_httpResponse.IsSuccessStatusCode)
            {
                var _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                _logger.LogInformation($"Sucesso - {_customer.NameBusiness} - {_customer.CnpjParameter}");
                return true;
            }
            else
            {
                var _customer = JsonSerializer.Deserialize<ProblemDetails>(_result, _options);
                _logger.LogError($"Erro: {_customer.Status}");
                return false;
            }
        }


        #region Kafka

        private async Task SendKafka(CustomersDto message)
        {
            await Task.Run(() =>
            {
                var messageJson = JsonSerializer.Serialize(message);
                var config = new ProducerConfig { BootstrapServers = _configuration["Kafka:Host"] };
                using (var producer = new ProducerBuilder<string, string>(config).Build())
                {

                    var result = producer.ProduceAsync("sxp-data-itau-customers",
                                                        new Message<string, string>
                                                        {
                                                            Key = Guid.NewGuid().ToString(),
                                                            Value = messageJson
                                                        });
                }
            });
        }

        #endregion

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        async void ProducerThato()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings.Host,
                    Port = _settings.RabbitMQSettings.Port
                };

                using var connection = factory.CreateConnection();
                var QueueNameThato = _settings.RabbitMQSettings.QueueNameThato;
                var queueDlqThato = _settings.RabbitMQSettings.QueueNameThatoDlq;
                var queueNameAtento = _settings.RabbitMQSettings.QueueNameAtento;
                var queueDlqAtento = _settings.RabbitMQSettings.QueueNameAtentoDlq;
                var queueNameCallLink = _settings.RabbitMQSettings.QueueNameCallLink;
                var queueDlqCallLink = _settings.RabbitMQSettings.QueueNameCallLinkDlq;

                var channelAtento = CreateChannel(connection);
                var channelThato = CreateChannel(connection);
                //var channelCanalLink = CreateChannel(connection);

                var pathPlanilha = _configuration["FileThato:PathCorban"];
                var nameFile = _configuration["FileThato:NameFile"];

                var nameSheet = _configuration["FileThato:NameSheet"];
                var nameCorbanThato = _configuration["FileThato:NameCorban"];

                var nameCorbanAtento = _configuration["FileAtento:NameCorban"];
                var nameCorbanCanalLink = _configuration["FileCanalLink:NameCorban"];

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var fileExcel = new ExcelPackage(new FileInfo(pathPlanilha + nameFile));
                ExcelWorksheet sheet = fileExcel.Workbook.Worksheets[nameSheet];
                int rows = sheet.Dimension.Rows;
                int total = 0;
                CustomersDto customerDto;

                for (int i = 2; i <= rows; i++)
                {
                    customerDto = new CustomersDto();
                    customerDto.CodeHtml = sheet.Cells[i, 1].Value == null ? null : sheet.Cells[i, 1].Value.ToString();
                    customerDto.CodeInternal = sheet.Cells[i, 2].Value == null ? null : sheet.Cells[i, 2].Value.ToString();
                    customerDto.Address.UfParameter = sheet.Cells[i, 3].Value == null ? null : sheet.Cells[i, 3].Value.ToString();
                    customerDto.CnpjParameter = sheet.Cells[i, 4].Value == null ? null : sheet.Cells[i, 4].Value.ToString();
                    customerDto.CNPJConsulted = sheet.Cells[i, 5].Value == null ? null : sheet.Cells[i, 5].Value.ToString();
                    customerDto.NumberRegistration = sheet.Cells[i, 6].Value == null ? null : sheet.Cells[i, 6].Value.ToString();
                    customerDto.NameBusiness = sheet.Cells[i, 7].Value == null ? null : sheet.Cells[i, 7].Value.ToString();
                    customerDto.Address.Logradouro = sheet.Cells[i, 8].Value == null ? null : sheet.Cells[i, 8].Value.ToString();
                    customerDto.Address.Numero = sheet.Cells[i, 9].Value == null ? null : sheet.Cells[i, 9].Value.ToString();
                    customerDto.Address.Complemento = sheet.Cells[i, 10].Value == null ? null : sheet.Cells[i, 10].Value.ToString();
                    customerDto.Address.CEP = sheet.Cells[i, 11].Value == null ? null : sheet.Cells[i, 11].Value.ToString();
                    customerDto.Address.Bairro = sheet.Cells[i, 12].Value == null ? null : sheet.Cells[i, 12].Value.ToString();
                    customerDto.Address.Municipio = sheet.Cells[i, 13].Value == null ? null : sheet.Cells[i, 13].Value.ToString();
                    customerDto.Address.UF = sheet.Cells[i, 14].Value == null ? null : sheet.Cells[i, 14].Value.ToString();
                    customerDto.Address.CodeIbge = sheet.Cells[i, 15].Value == null ? null : sheet.Cells[i, 15].Value.ToString();
                    customerDto.RegistrationStage = (sheet.Cells[i, 16].Value == null || (sheet.Cells[i, 16].Value.ToString() == "0")) ? "0" : sheet.Cells[i, 16].Value.ToString();
                    //customerDto.SupplierName = nameof(EnumTypeCorban.Thato);
                    //BuildPublishers(channelThato, QueueNameThato, queueDlqThato, customerDto);

                    customerDto.SupplierName = nameof(EnumTypeCorban.Atento);
                    customerDto?.Proposals?.Add(GerarFaturamentoMensal());

                    BuildPublishers(channelAtento, queueNameAtento, queueDlqAtento, customerDto);

                    total++;
                    Thread.Sleep(2500);
                }
            });

        }

        private static decimal GerarValor()
        {
            Random rnd = new Random();
            return rnd.Next(3000, 100000000);
        }

        private static string GerarPorteEmpresarial(decimal valor)
        {
            Random rnd = new Random();
            
            var pequena = rnd.Next(3000,4999);
            var micro = rnd.Next(5000, 14999);
            var media = rnd.Next(15000, 49999);
            var grande = rnd.Next(50000, 200000);
            if (valor >= pequena && valor <= pequena)
                return "pequena";
            else
                return "grande";
        }

        private static decimal GerarFaturamento(decimal valor)
        {
            Random rnd = new Random();

            var pequena = rnd.Next(3000, 4999);
            var micro = rnd.Next(5000, 14999);
            var media = rnd.Next(15000, 49999);
            var grande = rnd.Next(50000, 200000);

            if (valor > pequena && valor <= pequena)
                return valor;
            else if (valor >= micro && valor <= micro)
                return valor;
            else if (valor >= media && valor <= media)
                return valor;
            else
                return grande;
        }

        private static ProposalDto GerarFaturamentoMensal()
        {
            var valor = GerarValor();
            var valorFaturamento = GerarFaturamento(valor);
            var credit = new ProposalDto(valorFaturamento)
            {
                Valor = valorFaturamento,
            };
            return credit;
        }

        private static void BuildPublishers(IModel channel, string queueName, string queueDlq, CustomersDto message)
        {
            channel.ExchangeDeclare(queueDlq, ExchangeType.Fanout);
            channel.QueueDeclare(queue: queueDlq, true, false, false, null);
            channel.QueueBind(queueDlq, queueDlq, "");
            var args = new Dictionary<string, object>() { { "x-dead-letter-exchange", queueDlq } };

            channel.QueueDeclare(queue: queueName, true, false, false, args);
            var json = JsonSerializer.Serialize(message);
            var body = Encoding.UTF8.GetBytes(json);
            channel.BasicPublish(exchange: string.Empty, routingKey: queueName, null, body);
        }

        async void Consumer()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings.Host,
                    Port = _settings.RabbitMQSettings.Port
                };

                var QueueNameThato = _settings.RabbitMQSettings.QueueNameThato;
                var queueNameBradesco = _settings.RabbitMQSettings.QueueNameCallLink;
                var queueNameAtento = _settings.RabbitMQSettings.QueueNameAtento;
                var queueDlqThato = _settings.RabbitMQSettings.QueueNameThatoDlq;
                var queueDlqBradesco = _settings.RabbitMQSettings.QueueNameCallLinkDlq;
                var queueDlqAtento = _settings.RabbitMQSettings.QueueNameAtentoDlq;

                var connection = factory.CreateConnection();
                //var channel1 = CreateChannel(connection);
                var channel2 = CreateChannel(connection);
                //var channel3 = CreateChannel(connection);

                //BuildAndRunWorker(channel1, QueueNameThato, queueDlqThato);
                BuildAndRunWorker(channel2, queueNameAtento, queueDlqAtento);
                //BuildAndRunWorker(channel3, queueNameBradesco, queueDlqBradesco);
                //Thread.Sleep(1000);
            });
        }

        private void BuildAndRunWorker(IModel channel, string queueName, string queueNameDlq)
        {
            Task.Run(() =>
            {
                channel.QueueDeclare(queue: queueName, true, false, false
                                        , arguments: new Dictionary<string, object>()
                                        {
                                            ["x-dead-letter-exchange"] = queueNameDlq
                                        });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        //var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
                        //var messageJsom = JsonSerializer.Serialize(message, options);


                        var customer = JsonSerializer.Deserialize<CustomersDto>(message, _options);


                        if (customer != null)
                        {
                            if (customer.SupplierName == nameof(EnumTypeCorban.Thato))
                            {
                                var result = await SendCustomersThato(customer);
                                if (result)
                                    channel.BasicAck(ea.DeliveryTag,  false);
                                else
                                    channel.BasicNack(ea.DeliveryTag,  false, false);
                            }
                            else if (customer.SupplierName == nameof(EnumTypeCorban.Atento))
                            {
                                var result = await SendCustomersAtento(customer);
                                if (result)
                                    channel.BasicAck(ea.DeliveryTag,  false);
                                else
                                    channel.BasicNack(ea.DeliveryTag,  false, false);
                            }
                            else
                            {
                                channel.BasicNack(ea.DeliveryTag,  false, false);
                            }
                            //else if (customer.SupplierName == nameof(EnumTypeCorban.Atento))
                            //{
                            //    await SendCustomersAtento(customer);
                            //    channel.BasicAck(ea.DeliveryTag,  false);
                            //}
                            //else
                            //{
                            //    await SendCustomersCallLink(customer);
                            //    channel.BasicAck(ea.DeliveryTag,  false);
                            //}
                        }


                    }
                    catch
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: queueName, false, consumer);
            });
        }
    }
}
