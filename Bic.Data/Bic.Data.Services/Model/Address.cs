﻿namespace Bic.B2b.Data.Services.Model
{
    public class Address //: EntityBase
    {
        public int Id { get; set; }
        public string? UfParameter { get; set; }
        public string? Logradouro { get; set; }
        public string? Numero { get; set; }
        public string? Complemento { get; set; }
        public string? CEP { get; set; }
        public string? Bairro { get; set; }
        public string? Municipio { get; set; }
        public string? UF { get; set; }
        public string? CodeIbge { get; set; }
        public int CustomersId { get; set; }
    }
}

