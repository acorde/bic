﻿using Bic.B2b.Data.Services.Dto;

namespace Bic.B2b.Data.Services.Model
{
    public class Customers// : EntityBase
    {
        public Customers()
        {
            Address = new Address();
            Proposals = new List<ProposalDto>();
        }
        public int Id { get; set; }
        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public Address? Address { get; set; }
        public List<ProposalDto>? Proposals { get; set; }

        public Customers(int id, string? codeHtml, string? codeInternal, string? cnpjParameter, string? cnpjConsulted
            , string? numberRegistration, string? nameBusiness, string? registrationStage, string? supplierName, 
            Address? address, List<ProposalDto>? creditProposalDtos)
        {
            Id = id;
            CodeHtml = codeHtml;
            CodeInternal = codeInternal;
            CnpjParameter = cnpjParameter;
            CNPJConsulted = cnpjConsulted;
            NumberRegistration = numberRegistration;
            NameBusiness = nameBusiness;
            RegistrationStage = registrationStage;
            SupplierName = supplierName;
            Address = address;
            Proposals = creditProposalDtos;
        }
    }
}
