﻿namespace Bic.Data.Services.Model
{
    public class Proposal
    {
        public Proposal(int id, int customersId, decimal? valor, DateTime? updatedAt, DateTime? createdAt)
        {
            Id = id;
            Valor = valor;
            CustomersId = customersId;
            UpdatedAt = updatedAt;
            CreatedAt = createdAt;
        }

        public int Id { get; set; }
        public decimal? Valor { get; set; }
        public int CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }

    }
}
