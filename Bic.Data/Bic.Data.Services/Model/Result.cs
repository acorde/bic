﻿namespace SXP.Data.Services.Model
{
    public class Result
    {
        public string AccessToken { get; set; }
        public bool Sucesso { get; set; }
        public string RefreshToken { get; set; }
    }
}
