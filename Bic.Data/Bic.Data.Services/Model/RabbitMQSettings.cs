﻿namespace Bic.Data.Services.Model
{
    public class RabbitMQSettings
    {
        public bool? IsProduction { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string VirtualHost { get; set; }
        public string QueueNameThato { get; set; }
        public string QueueNameAtento { get; set; }
        public string QueueNameCallLink { get; set; }
        public string QueueNameThatoDlq { get; set; }
        public string QueueNameAtentoDlq { get; set; }
        public string QueueNameCallLinkDlq { get; set; }
    }
}
