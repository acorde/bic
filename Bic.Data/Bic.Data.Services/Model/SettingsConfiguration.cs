﻿namespace Bic.Data.Services.Model
{
    public sealed class SettingsConfiguration
    {
        public bool? Log { get; set; }
        public string? ConnectionStringId { get; set; }
        public RabbitMQSettings RabbitMQSettings { get; set; }
    }
}
