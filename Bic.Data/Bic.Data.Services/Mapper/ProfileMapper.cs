﻿using AutoMapper;
using Bic.B2b.Data.Services.Dto;
using Bic.B2b.Data.Services.Model;
using Bic.Data.Services.Model;

namespace Bic.B2b.Data.Services.Mapper
{
    public class ProfileMapper: Profile
    {
        public ProfileMapper()
        {
            //CreateMap<ProposalDto, Proposal>();
            //CreateMap<Proposal, ProposalDto>();
            CreateMap<CustomersDto, Customers>();
            CreateMap<Customers, CustomersDto>();
            CreateMap<Address, AddressDto>();
            CreateMap<AddressDto, Address>();
        }
    }
}
