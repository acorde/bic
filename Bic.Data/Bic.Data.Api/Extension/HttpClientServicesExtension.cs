﻿using Polly;

namespace Bic.B2b.Api.Extension
{
    public static class HttpClientServicesExtension
    {
        public static IServiceCollection AddConfigurationHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient("clientB2bItau", HttpsClient =>
            {
                HttpsClient.DefaultRequestHeaders.AcceptLanguage.Clear();
                HttpsClient.BaseAddress = new Uri("https://localhost:7119/");
            })
            .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("clientB2bSantander", HttpsClient =>
            {
                HttpsClient.DefaultRequestHeaders.AcceptLanguage.Clear();
                HttpsClient.BaseAddress = new Uri("https://localhost:7119/");
            });

            services.AddHttpClient("clientB2bBradesco", HttpsClient =>
            {
                HttpsClient.DefaultRequestHeaders.AcceptLanguage.Clear();
                HttpsClient.BaseAddress = new Uri("https://localhost:7119/");
            });

            return services;
        }
    }
}
