﻿namespace Bic.B2b.Api.Extension
{
    public static class HealthServicesExtension
    {
        public static IServiceCollection AddServicesConfigurationHealth(this IServiceCollection services)
        {
            services.AddHealthChecks();
            services.AddHealthChecksUI(options =>
            {
                options.SetEvaluationTimeInSeconds(5);
                options.MaximumHistoryEntriesPerEndpoint(5);
                options.AddHealthCheckEndpoint("API Data", "/health");
            }).AddInMemoryStorage();

            return services;
        }
    }
}
