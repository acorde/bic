﻿using Microsoft.OpenApi.Models;

namespace Bic.B2b.Api.Extension
{
    public static class SwaggerServicesExtension
    {
        public static IServiceCollection AddServicesConfigurationSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "SXP.Data.Api",
                    Version = "v1",
                    Contact = new OpenApiContact
                    {
                        Name = "SXP Data",
                        Email = "contato@teste.com.br",
                        Url = new Uri("https://localhost:7193/health")
                    }
                });

                var xmlFile = "DocumentingSwagger.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            });

            return services;
        }
    }
}
