﻿using Bic.Data.Services.Model;
using Microsoft.Extensions.Options;

namespace Bic.Data.Api.Extension
{
    public static class Configuration
    {
        public static void AddConfiguration<T>(this IServiceCollection services, IConfiguration configuration, string? configurationTag = null) where T : class
        {
            if (string.IsNullOrEmpty(configurationTag))
            {
                configurationTag = typeof(T).Name;
            }

            var instance = Activator.CreateInstance<T>();
            new ConfigureFromConfigurationOptions<T>(configuration.GetSection(configurationTag)).Configure(instance);
            services.AddSingleton(instance);
        }


        public static IServiceCollection ConfigureValidateRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConfiguration<SettingsConfiguration>(configuration, "MySettings");
            return services;
        }
    }
}
