using HealthChecks.UI.Client;
using Bic.B2b.Api.Extension;
using Bic.B2b.Data.Services;
using Bic.B2b.Data.Services.Mapper;
using Bic.Data.Api.Extension;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers()
    .AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddServicesConfigurationSwagger();
//builder.Services.AddServicesConfigurationHealth();
builder.Services.AddConfigurationHttpClientServices();
builder.Services.AddHostedService<Worker>();
builder.Services.AddAutoMapper(typeof(ProfileMapper));
builder.Services.ConfigureValidateRabbitMQ(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHealthChecks("/health", new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions
//{
//    Predicate = p => true,
//    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
//});

//app.UseHealthChecksUI(options =>
//{
//    options.UIPath = "/Dashboard";
//});
//app.UseHealthChecks("/health");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
