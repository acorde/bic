﻿using Dapper;
using MongoDB.Driver.Core.Operations;
using Bic.B2b.Domain.Interfaces.Dapper;
using Bic.B2b.Domain.Model;
using System.Data;
using Microsoft.Extensions.Logging;

namespace Bic.B2b.Infrastructure.Repository.Dapper
{
    public class AccountDapperRepository : IAccountDapperRepository
    {
        private readonly IDbConnection _dbConnection;
        private readonly ILogger<AccountDapperRepository> logger;
        public AccountDapperRepository(IDbConnection dbConnection, ILogger<AccountDapperRepository> logger)
        {
            _dbConnection = dbConnection;
            this.logger = logger;
        }

        public async Task<Account?> GetAccountByEmail(string email)
        {
            try
            {
                string query = "Select * from Accounts Where Email = @email";
                return await _dbConnection.QueryFirstOrDefaultAsync<Account>(query, new { Email = email });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Account> GetAccountById(int id)
        {
            string query = "Select * from Accounts Where Id = @id";
            var ret =  await _dbConnection.QueryFirstOrDefaultAsync<Account>(query, new { Id = id });
            return ret;
        }

        public async Task<IEnumerable<Account>> GetAccounts()
        {
            string query = "Select * from Accounts";
            var ret = await _dbConnection.QueryAsync<Account>(query);
            return ret;
        }
    }
}
