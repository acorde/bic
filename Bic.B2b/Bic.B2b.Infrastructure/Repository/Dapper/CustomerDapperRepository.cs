﻿using Bic.B2b.Domain.Interfaces.Dapper;
using Bic.B2b.Domain.Model;
using Bic.B2b.Domain.Model.Response;
using Dapper;
using Microsoft.Extensions.Logging;
using System.Data;

namespace Bic.B2b.Infrastructure.Repository.Dapper
{
    public class CustomerDapperRepository : ICustomerDapperRepository
    {
        private readonly IDbConnection _connection;
        private readonly ILogger<CustomerDapperRepository> _logger;

        public CustomerDapperRepository(IDbConnection connection,
                                    ILogger<CustomerDapperRepository> logger)
        {
            _connection = connection;
            _logger = logger;
            
        }

        public async Task<CustomerResponse?> GetById(int id)
        {
            string? query1 = @"SELECT 
                                c.Id as CustomersId, 
                                c.CodeHtml, 
                                c.CodeInternal, 
                                c.CnpjParameter, 
                                c.CNPJConsulted, 
                                c.NumberRegistration, 
                                c.NameBusiness, 
                                c.RegistrationStage, 
                                c.SupplierName
                                FROM BicB2bDb.Customers as c
                                WHERE c.Id = @id";            
            var customer = await _connection.QueryFirstOrDefaultAsync<CustomerResponse?>(query1, new { Id = id });
            int CustomersId = customer.CustomersId;

            string? query2 = @"SELECT 
                                a.Id as AddressId, 
                                a.UfParameter, 
                                a.Logradouro, 
                                a.Numero, 
                                a.Complemento, 
                                a.CEP, 
                                a.Bairro, 
                                a.Municipio, 
                                a.UF, 
                                a.CodeIbge,
                                a.CustomersId
                                FROM BicB2bDb.Address as a
                                WHERE a.CustomersId = @CustomersId";
            var Address = await _connection.QueryFirstOrDefaultAsync<AddressResponse?>(query2, new { CustomersId = CustomersId });
            

            string? query3 = @"SELECT 
                                p.Id as ProposalId, 
                                p.Valor, 
                                p.CustomersId, 
                                p.UpdatedAt, 
                                p.CreatedAt
                                FROM BicB2bDb.Proposals p  
                                WHERE p.CustomersId = @CustomersId";
            var proposal = await _connection.QueryAsync<ProposalResponse?>(query3, new { CustomersId = CustomersId });

            customer.Address = Address;
            customer.Proposals = proposal.ToList();

            return customer;
        }
    }
}
