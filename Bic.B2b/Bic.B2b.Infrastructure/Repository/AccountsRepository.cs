﻿using Bic.B2b.Domain.Interfaces;
using Bic.B2b.Domain.Model;
using Bic.B2b.Infrastructure.Context;

namespace Bic.B2b.Infrastructure.Repository
{
    public class AccountsRepository : IAccountsRepository
    {
        private readonly  AppDbContext _context;

        public AccountsRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Account> AddAccounts(Account accounts)
        {
            if (accounts is null)
                throw new ArgumentNullException(nameof(accounts));

            await _context.Accounts.AddAsync(accounts);
            //await _context.SaveChangesAsync();
            return accounts;
        }
    }
}
