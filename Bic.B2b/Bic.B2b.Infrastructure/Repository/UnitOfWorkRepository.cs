﻿using Amazon.Runtime.Internal.Util;
using Bic.B2b.Domain.Interfaces;
using Bic.B2b.Infrastructure.Context;
using Microsoft.Extensions.Logging;

namespace Bic.B2b.Infrastructure.Repository
{
    public class UnitOfWorkRepository : IUnitOfWork, IDisposable
    {
        private ICustomersRepository? _customerRepo;
        private IAccountsRepository? _accountsRepository;
        private readonly AppDbContext _context;
        private readonly ILogger<CustomersRepository> _logger;

        public UnitOfWorkRepository(AppDbContext context, 
                                    ILogger<CustomersRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public ICustomersRepository CustomersRepository
        {
            get
            {
                return _customerRepo = _customerRepo ??
                    new CustomersRepository(_context, _logger);
            }
        }

        public IAccountsRepository AccountsRepository
        {
            get
            {
                return _accountsRepository = _accountsRepository ??
                    new AccountsRepository(_context);
            }
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
