﻿using Microsoft.EntityFrameworkCore;
using Bic.B2b.Domain.Interfaces;
using Bic.B2b.Domain.Model;
using Bic.B2b.Infrastructure.Context;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Amazon.Runtime.Internal.Util;
using Microsoft.Extensions.Logging;

namespace Bic.B2b.Infrastructure.Repository
{
    public class CustomersRepository : ICustomersRepository
    {
        protected readonly AppDbContext _context;
        private readonly ILogger<CustomersRepository> _logger;
        public CustomersRepository(AppDbContext context,
                                    ILogger<CustomersRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Customers> GetCustomersById(int id)
        {
            var member = await _context.Customers.FindAsync(id);
            if (member is null) throw new InvalidOperationException("Member not found");

            return member;
        }

        public async Task<Customers> AddCustomers(Customers customer)
        {
            try
            {
                _logger.LogInformation($"{nameof(AddCustomers)} - Início do método {nameof(AddCustomers)} para inserir dados no BD em {DateTimeOffset.Now}.");
                if (customer is null)
                {
                    _logger.LogWarning($"{nameof(AddCustomers)} - Model {customer} nula em {DateTimeOffset.Now}.");
                    throw new ArgumentNullException(nameof(customer));
                }

                await _context.Customers.AddAsync(customer);
                _logger.LogInformation($"{nameof(AddCustomers)} - Fim do método {nameof(AddCustomers)} para inserir dados no BD em {DateTimeOffset.Now}.");
                return customer;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(AddCustomers)} - Erro do método {nameof(AddCustomers)} ao tentar inserir dados no BD em {DateTimeOffset.Now} - Message:{ex.Message} TraceId: {ex.StackTrace}.");
                throw new Exception($"{nameof(AddCustomers)} Erro - Message:{ex.Message} TraceId: {ex.StackTrace}.");
            }
            
        }

        public async Task<IEnumerable<Customers>> GetCustomers()
        {
            var customer = await _context.Customers.ToListAsync();
            return customer ?? Enumerable.Empty<Customers>();
        }

        public void UpdateCustomers(Customers customer)
        {
            if (customer is null) throw new ArgumentNullException(nameof(customer));
            _context.Customers.Update(customer);

        }

        public async Task<Customers> DeleteCustomers(int id)
        {
            var customer = await GetCustomersById(id);
            if (customer is null) throw new InvalidOperationException($"{nameof(customer)} not found");
            _context.Customers.Remove(customer);
            return customer;
        }

    }
}
