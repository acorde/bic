﻿using Bic.B2b.Domain.Model;
using Bic.B2b.Infrastructure.ModelsConfiguration;
using Microsoft.EntityFrameworkCore;

namespace Bic.B2b.Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Proposal> Proposals { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CustomersConfiguration());
            builder.ApplyConfiguration(new AddressConfiguration());
            builder.ApplyConfiguration(new AccountsConfiguration());
            builder.ApplyConfiguration(new ProposalConfiguration());
        }
    }
}
