﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Bic.B2b.Infrastructure.ContextMongoDb
{
    public abstract class CustomersMongoModelBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        public string CnpjParameter { get; set; } = null!;
    }
}
