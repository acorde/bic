﻿namespace Bic.B2b.Infrastructure.ContextMongoDb
{
    public class DataBaseSettingsNoSql
    {
        public string HostCorban { get; set; } = null!;        
        public string DatabaseName { get; set; } = null!;
        public string CollectionNameSXPItau { get; set; } = null!;
        public string CollectionNameSXPSantander { get; set; } = null!;
        public string CollectionNameSXPBradesco { get; set; } = null!;
    }
}
