﻿using Bic.B2b.Domain.Interfaces.Request;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.Request
{
    public class RequestCustomersSantander : IRequestCustomersDomainSantander
    {
        private readonly ILogger<RequestCustomersSantander> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _configuration;

        public RequestCustomersSantander(ILogger<RequestCustomersSantander> logger, 
                                        IHttpClientFactory httpClientFactory, 
                                        SettingsConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _configuration = configuration;
        }

        public async Task<Customers?> SendGetNet(Customers customer)
        {
            Customers? _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendGetNet)} - Iniciando a requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                var _httpClient = _httpClientFactory.CreateClient("clientSantander");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriSantanderGetNet;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    _logger.LogInformation($"{nameof(SendGetNet)} - Fim da requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                    return _customer;
                }
                else
                {
                    _logger.LogError($"{nameof(SendGetNet)} - Fim da requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendGetNet)} - ERRO: {ex.Message}.");
                return _customer;
            }
        }

        public async Task<Customers> SendCielo(Customers customer)
        {
            Customers _customer = new Customers();
            try
            {
                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                    WriteIndented = true
                };
                var model = JsonSerializer.Serialize(customer, options);

                _logger.LogInformation($"{nameof(SendCielo)} - Iniciando a requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                var _httpClient = _httpClientFactory.CreateClient("clientSantander");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriSantander;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);
                
                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    _logger.LogInformation($"{nameof(SendCielo)} - Fim da requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                    return _customer;
                }
                else
                {
                    _logger.LogError($"{nameof(SendCielo)} - Fim da requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendCielo)} - ERRO: {ex.Message}.");
                return _customer;
            }
        }

        public async Task<Customers?> SendMauaBank(Customers customer)
        {
            Customers? _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendMauaBank)} - Iniciando a requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                var _httpClient = _httpClientFactory.CreateClient("clientSantander");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriSantanderMauaBank;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    _logger.LogInformation($"{nameof(SendMauaBank)} - Fim da requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                    return _customer;
                }
                else
                {
                    _logger.LogError($"{nameof(SendMauaBank)} - Fim da requisição Http Post para Api Bic.Santander.Api:: {customer}.");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendMauaBank)} - ERRO: {ex.Message}.");
                return _customer;
            }
        }
    }
}
