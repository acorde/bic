﻿using Microsoft.Extensions.Logging;
using Bic.B2b.Domain.Interfaces.Request;
using Bic.B2b.Domain.Model;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.Request
{
    public class RequestCustomersBradesco : IRequestCustomersDomainBradesco
    {
        private readonly ILogger<RequestCustomersBradesco> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _configuration;

        public RequestCustomersBradesco(ILogger<RequestCustomersBradesco> logger, 
                                        IHttpClientFactory httpClientFactory, 
                                        SettingsConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _configuration = configuration;
        }

        public async Task<Customers> SendDataItau<T>(T customer)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(T)} - Iniciando a requisição Http Post");
                var _httpClient = _httpClientFactory.CreateClient("clientItau");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriItauCielo;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);
                
                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidadp");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(T)} - {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendDataSantander(Customers customer)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendDataSantander)} - Iniciando a requisição Http Post");
                var _httpClient = _httpClientFactory.CreateClient("clientSantander");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriSantander;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendDataSantander)} - {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendDataBradesco(Customers customer)
        {
            Customers _customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(SendDataBradesco)} - Iniciando a requisição Http Post");
                var _httpClient = _httpClientFactory.CreateClient("clientBradesco");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriBradesco;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    _customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return _customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidado");
                    return _customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendDataBradesco)} - {ex.Message}");
                return _customer;
            }
        }

        public async Task<Customers> SendCielo<T>(T model)
        {
            Customers customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(T)} - Iniciando a requisição Http Post");
                var _httpClient = _httpClientFactory.CreateClient("clientItau");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriBradescoCielo;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidadp");
                    return customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(T)} - {ex.Message}");
                return customer;
            }
        }

        public async Task<Customers> SendGetNet<T>(T model)
        {
            Customers customer = new Customers();
            try
            {
                _logger.LogInformation($"{nameof(T)} - Iniciando a requisição Http Post");
                var _httpClient = _httpClientFactory.CreateClient("clientItau");
                var _content = new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json");
                var _uri = _configuration?.UriSettings?.UriBradescoGetNet;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    customer = JsonSerializer.Deserialize<Customers>(_result, _options);
                    return customer;
                }
                else
                {
                    _logger.LogWarning($"Cuidadp");
                    return customer;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(T)} - {ex.Message}");
                return customer;
            }
        }
    }
}
