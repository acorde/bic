﻿using Microsoft.IdentityModel.Tokens;
using Bic.B2b.Domain.ModelToken;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Bic.B2b.Domain.Token;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using MongoDB.Driver.Linq;

namespace Bic.B2b.Infrastructure.Token
{
    public class GeraToken : IGeraToken
    {
        private readonly ILogger<GeraToken> _logger;
        private readonly SettingsConfiguration _configuration;

        public GeraToken(SettingsConfiguration configuration, ILogger<GeraToken> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task<UserToken> GenerateToken(Account userInfo)
        {
            var claims = new[]
            {
                //new Claim(JwtRegisteredClaimNames.Sub, userInfo.Id),
                new Claim(JwtRegisteredClaimNames.Email, userInfo.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                //new Claim(JwtRegisteredClaimNames.Nbf, DateTime.Now.ToString()),
                //new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString())
            };

            var privateKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.JwtOptions.SecurityKey));
            var credentials = new SigningCredentials(privateKey, SecurityAlgorithms.HmacSha256);
            var expiration = DateTime.Now.AddHours(_configuration.JwtOptions.AccessTokenExpiration); //

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration.JwtOptions.Issuer,
                audience: _configuration.JwtOptions.Audience,
                claims: claims,
                //notBefore: DateTime.Now,
                expires: expiration,
                signingCredentials: credentials
                );

            return new UserToken
            (
                sucesso: true,
                accessToken : new JwtSecurityTokenHandler().WriteToken(token),
                refreshToken: ""
            );
        }

        private string GerarToken(IEnumerable<Claim> claims, DateTime dataExpiracao)
        {
            var jwt = new JwtSecurityToken(
                issuer: _configuration.JwtOptions.Issuer,
                audience: _configuration.JwtOptions.Audience,
                claims: claims,
                notBefore: DateTime.Now,
                expires: dataExpiracao);
               // signingCredentials: _configuration.JwtOptions.SigningCredentials);

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}
