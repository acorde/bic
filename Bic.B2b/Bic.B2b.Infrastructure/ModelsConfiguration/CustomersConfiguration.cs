﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bic.B2b.Domain.Model;

namespace Bic.B2b.Infrastructure.ModelsConfiguration
{
    public class CustomersConfiguration : IEntityTypeConfiguration<Customers>
    {
        public void Configure(EntityTypeBuilder<Customers> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(m => m.CodeInternal);
            builder.Property(x => x.CnpjParameter);
            builder.Property(x => x.CodeHtml);
            //builder.HasIndex(x => new { x.CodeHtml, x.CnpjParameter }).IsUnique(true);
            builder.Property(m => m.CNPJConsulted);
            builder.Property(m => m.NumberRegistration);
            builder.Property(m => m.NameBusiness);
            builder.Property(m => m.RegistrationStage);
        }
    }
}
