﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bic.B2b.Domain.Model;

namespace Bic.B2b.Infrastructure.ModelsConfiguration
{
    public class AccountsConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(m => m.Name);
            builder.Property(m => m.Email);
            builder.Property(m => m.Login);
            builder.Property(m => m.Password);

            //builder.HasData(
            //    new Account(1, "Maria", "Jopli", "feminino", "janes@email.com", true);
        }
    }
}
