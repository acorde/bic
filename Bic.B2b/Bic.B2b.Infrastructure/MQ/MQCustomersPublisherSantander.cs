﻿using Bic.B2b.Domain.Interfaces.MQ;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.MQ
{
    public class MQCustomersPublisherSantander : IMQCustomersPublisherSantander
    {
        private readonly ILogger<MQCustomersPublisherSantander> _logger;
        private readonly SettingsConfiguration _settings;

        public MQCustomersPublisherSantander(ILogger<MQCustomersPublisherSantander> logger, 
                                            SettingsConfiguration settingsConfiguration)
        {
            _logger = logger;
            _settings = settingsConfiguration;
        }

        public static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task PublisherMessageSantander<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"Executando a publicação");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare("atento-b2b-dlq", ExchangeType.Fanout);
                    channel.QueueDeclare(queue: "atento-b2b-dlq", true, false, false, null);
                    channel.QueueBind("atento-b2b-dlq", "atento-b2b-dlq", "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", "atento-b2b-dlq" }
                        };

                    channel.QueueDeclare(queue: "atento-b2b", true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "atento-b2b", null, body);
                };
            });
        }
    }
}
