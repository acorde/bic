﻿using Bic.B2b.Domain.Interfaces.MQ;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.MQ
{
    public class MQCustomersPublisherBradesco : IMQCustomersPublisherBradesco
    {
        private readonly ILogger<MQCustomersPublisherBradesco> _logger;
        private readonly SettingsConfiguration _settingsConfiguration;

        public MQCustomersPublisherBradesco(ILogger<MQCustomersPublisherBradesco> logger, 
            SettingsConfiguration settingsConfiguration)
        {
            _logger = logger;
            _settingsConfiguration = settingsConfiguration;
        }

        public static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task PublisherMessageItau<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settingsConfiguration.RabbitSettings.Host,
                    Port = _settingsConfiguration.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"Executando a publicação");
                    var channel = CreateChannel(connection);
                    //BuildPublishers(channel1, "ita-customers", "Produtor A", message);
                    channel.ExchangeDeclare("itau-b2b-customers-dlq", ExchangeType.Fanout);
                    channel.QueueDeclare(queue: "itau-b2b-customers-dlq", true, false, false, null);
                    channel.QueueBind("itau-b2b-customers-dlq", "itau-b2b-customers-dlq", "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", "itau-b2b-customers-dlq" }
                        };

                    channel.QueueDeclare(queue: "itau-b2b-customers", true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "itau-b2b-customers", null, body);
                };
            });
        }

        public Task PublisherMessageNubank<T>(T message)
        {
            throw new NotImplementedException();
        }

        public async Task PublisherMessageBradesco<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settingsConfiguration.RabbitSettings.Host,
                    Port = _settingsConfiguration.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"Executando a publicação");
                    var channel = CreateChannel(connection);
                    channel.ExchangeDeclare("bradesco-b2b-customers-dlq", ExchangeType.Fanout);
                    channel.QueueDeclare(queue: "bradesco-b2b-customers-dlq", true, false, false, null);
                    channel.QueueBind("bradesco-b2b-customers-dlq", "bradesco-b2b-customers-dlq", "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", "bradesco-b2b-customers-dlq" }
                        };

                    channel.QueueDeclare(queue: "bradesco-b2b-customers", true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "bradesco-b2b-customers", null, body);
                };
            });
        }

        public async Task PublisherMessageSantander<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settingsConfiguration.RabbitSettings.Host,
                    Port = _settingsConfiguration.RabbitSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"Executando a publicação");
                    var channel = CreateChannel(connection);
                    channel.ExchangeDeclare("santander-b2b-customers-dlq", ExchangeType.Fanout);
                    channel.QueueDeclare(queue: "santander-b2b-customers-dlq", true, false, false, null);
                    channel.QueueBind("santander-b2b-customers-dlq", "santander-b2b-customers-dlq", "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", "santander-b2b-customers-dlq" }
                        };

                    channel.QueueDeclare(queue: "santander-b2b-customers", true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: "santander-b2b-customers", null, body);
                };
            });
        }
    }
}
