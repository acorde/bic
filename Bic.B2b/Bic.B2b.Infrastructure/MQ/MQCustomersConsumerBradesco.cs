﻿using Bic.B2b.Domain.Interfaces.MQ;
using Bic.B2b.Domain.Interfaces.Request;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.MQ
{
    public class MQCustomersConsumerBradesco : IMQCustomersConsumerBradesco
    {
        private readonly ILogger<MQCustomersConsumerBradesco> _logger;
        private readonly IRequestCustomersDomainBradesco _requestCustomersDomainBradesco;
        private readonly SettingsConfiguration _settingsConfiguration;
        private readonly JsonSerializerOptions _options;

        public MQCustomersConsumerBradesco(ILogger<MQCustomersConsumerBradesco> logger,
                                            IRequestCustomersDomainBradesco requestCustomersDomainBradesco,
                                            SettingsConfiguration settingsConfiguration)
        {
            _logger = logger;
            _requestCustomersDomainBradesco = requestCustomersDomainBradesco;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _settingsConfiguration = settingsConfiguration;
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task ConsumerMessageItau()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settingsConfiguration.RabbitSettings.Host,
                    Port = _settingsConfiguration.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "itau-b2b-customers", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "itau-b2b-customers-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomainBradesco.SendDataItau<Customers>(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "itau-b2b-customers", false, consumer);

            });
        }


        public async Task ConsumerMessageBradesco()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settingsConfiguration.RabbitSettings.Host,
                    Port = _settingsConfiguration.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "bradesco-b2b-customers", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "bradesco-b2b-customers-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomainBradesco.SendDataBradesco(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "bradesco-b2b-customers", false, consumer);

            });
        }

        public async Task ConsumerMessageNubank()
        {
            //await Task.Run(() =>
            //{
            var factory = new ConnectionFactory
            {
                HostName = _settingsConfiguration.RabbitSettings.Host,
                Port = _settingsConfiguration.RabbitSettings.Port
            };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@rabbitmq:5672/") };
            using var connection = factory.CreateConnection();
            using (var channel = connection.CreateModel())
            {
                //channel.ExchangeDeclare("nbk-customers-exh", "fanout");
                //channel.QueueDeclare("nbk-customers-dlq", true, false, false, null);
                //channel.QueueBind("nbk-customers-dlq", "nbk-customers-exh", "");

                //var args = new Dictionary<string, object>()
                //{
                //    { "x-dead-letter-exchange", "nbk-customers-exh" }
                //};

                channel.QueueDeclare(queue: "nbk-customers", true, false, false, null);

                channel.BasicQos(prefetchSize: 0, prefetchCount: 10, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    _logger.LogInformation($"Nubank Item: |{consumer}|.");
                    //try
                    //{
                    //    Random random = new Random();
                    //    var num = random.Next(1, 1000);
                    //    var body = ea.Body.ToArray();
                    //    var message = Encoding.UTF8.GetString(body);

                    //    if (num % 2 == 0)
                    //    {
                    //        _logger.LogWarning($"Número PAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicAck(ea.DeliveryTag, false);
                    //    }
                    //    else
                    //    {
                    //        _logger.LogWarning($"Número ÍMPAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicNack(ea.DeliveryTag, false, false);
                    //    }

                    //}
                    //catch (Exception ex)
                    //{
                    //    _logger.LogError($"--->> ERRO: |{ex.Message}|<<--- .");
                    //    channel.BasicNack(ea.DeliveryTag, false, false);
                    //}
                };
                channel.BasicConsume(queue: "nbk-customers", autoAck: true, consumer: consumer);
            };
            //});

        }

        public async Task ConsumerMessageSantander()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settingsConfiguration.RabbitSettings.Host,
                    Port = _settingsConfiguration.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "santander-b2b-customers", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "santander-b2b-customers-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomainBradesco.SendDataSantander(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "santander-b2b-customers", false, consumer);

            });
        }

    }
}
