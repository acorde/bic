﻿using Bic.B2b.Domain.Interfaces.MQ;
using Bic.B2b.Domain.Interfaces.Request;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.MQ
{
    public class MQCustomersConsumerSantander : IMQCustomersConsumerSantander
    {
        private readonly ILogger<MQCustomersConsumerSantander> _logger;
        private readonly IRequestCustomersDomainSantander _consumer;
        private readonly SettingsConfiguration _settings;
        private readonly JsonSerializerOptions _options;

        public MQCustomersConsumerSantander(ILogger<MQCustomersConsumerSantander> logger,
                                            IRequestCustomersDomainSantander consumer,
                                            SettingsConfiguration settings)
        {
            _logger = logger;
            _consumer = consumer;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _settings = settings;
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task ConsumerCielo()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "atento-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "atento-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _consumer.SendCielo(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "atento-b2b", false, consumer);

            });
        }


        public async Task ConsumerMauaBank()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "atento-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "atento-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _consumer.SendMauaBank(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "atento-b2b", false, consumer);

            });
        }

        public async Task ConsumerGetNet()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "atento-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "atento-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _consumer.SendGetNet(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "atento-b2b", false, consumer);

            });
        }

    }
}
