﻿using Bic.B2b.Domain.Interfaces.MQ;
using Bic.B2b.Domain.Interfaces.Request;
using Bic.B2b.Domain.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Bic.B2b.Infrastructure.MQ
{
    public class MQCustomersConsumerItau : IMQCustomersConsumerItau
    {
        private readonly ILogger<MQCustomersConsumerItau> _logger;
        private readonly IRequestCustomersDomainItau _requestCustomersDomain;
        private readonly SettingsConfiguration _settings;
        private readonly JsonSerializerOptions _options;

        public MQCustomersConsumerItau(ILogger<MQCustomersConsumerItau> logger, 
                                        IRequestCustomersDomainItau requestCustomersDomain, 
                                        SettingsConfiguration settingsConfiguration)
        {
            _logger = logger;
            _requestCustomersDomain = requestCustomersDomain;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _settings = settingsConfiguration;
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task ConsumerCielo()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {                        
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomain.SendDataItauCielo<Customers>(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-b2b", false, consumer);

            });
        }

        public async Task ConsumerGetNet()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomain.SendDataItauGetNet<Customers>(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-b2b", false, consumer);

            });
        }

        public async Task ConsumerMauaBank()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomain.SendDataItauMauaBank<Customers>(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-b2b", false, consumer);

            });
        }

        public async Task ConsumerMessageItau()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "thato-b2b", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "thato-b2b-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        Random random = new Random();
                        int num = random.Next(1, 100);
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomain.SendDataItau<Customers>(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "thato-b2b", false, consumer);

            });
        }

        public async Task ConsumerMessageBradesco()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "bradesco-b2b-customers", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "bradesco-b2b-customers-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        Random random = new Random();
                        int num = random.Next(1, 100);
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomain.SendDataBradesco(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "bradesco-b2b-customers", false, consumer);

            });
        }

        public async Task ConsumerMessageNubank()
        {
            //await Task.Run(() =>
            //{
            var factory = new ConnectionFactory
            {
                HostName = _settings.RabbitSettings.Host,
                Port = _settings.RabbitSettings.Port
            };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@rabbitmq:5672/") };
            using var connection = factory.CreateConnection();
            using (var channel = connection.CreateModel())
            {
                //channel.ExchangeDeclare("nbk-customers-exh", "fanout");
                //channel.QueueDeclare("nbk-customers-dlq", true, false, false, null);
                //channel.QueueBind("nbk-customers-dlq", "nbk-customers-exh", "");

                //var args = new Dictionary<string, object>()
                //{
                //    { "x-dead-letter-exchange", "nbk-customers-exh" }
                //};

                channel.QueueDeclare(queue: "nbk-customers", true, false, false, null);

                channel.BasicQos(prefetchSize: 0, prefetchCount: 10, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    _logger.LogInformation($"Nubank Item: |{consumer}|.");
                    //try
                    //{
                    //    Random random = new Random();
                    //    var num = random.Next(1, 1000);
                    //    var body = ea.Body.ToArray();
                    //    var message = Encoding.UTF8.GetString(body);

                    //    if (num % 2 == 0)
                    //    {
                    //        _logger.LogWarning($"Número PAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicAck(ea.DeliveryTag, false);
                    //    }
                    //    else
                    //    {
                    //        _logger.LogWarning($"Número ÍMPAR: {num} - {DateTimeOffset.Now}");
                    //        channel.BasicNack(ea.DeliveryTag, false, false);
                    //    }

                    //}
                    //catch (Exception ex)
                    //{
                    //    _logger.LogError($"--->> ERRO: |{ex.Message}|<<--- .");
                    //    channel.BasicNack(ea.DeliveryTag, false, false);
                    //}
                };
                channel.BasicConsume(queue: "nbk-customers", autoAck: true, consumer: consumer);
            };
            //});

        }

        public async Task ConsumerMessageSantander()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitSettings.Host,
                    Port = _settings.RabbitSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: "santander-b2b-customers", true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = "santander-b2b-customers-dlq"
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        Random random = new Random();
                        int num = random.Next(1, 100);
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _requestCustomersDomain.SendDataSantander(customer);

                        if (result != null)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: "santander-b2b-customers", false, consumer);

            });
        }

    }
}
