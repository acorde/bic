using Hangfire;
using HealthChecks.UI.Client;
using Bic.B2b.Application.Mapper;
using Bic.B2b.CrossCutting.AppSettings;
using Bic.B2b.CrossCutting.Dependency;
using Bic.B2b.CrossCutting.Extension;
using Bic.B2b.CrossCutting.Mapper;
using Bic.B2b.CrossCutting.Mongo.Services;
using Bic.B2b.Infrastructure.ContextMongoDb;
using Bic.B2b.Schedule;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddServicesConfigurationSwagger();
//builder.Services.AddServicesConfigurationHealth(builder.Configuration);
builder.Services.AddConfigurationHttpClientServices();
builder.Services.AddEndpointsApiExplorer();

builder.Services.ConfigureServices(builder.Configuration);
builder.Services.ConfigureValidateJWT(builder.Configuration);
builder.Services.AddInfrastructureSwagger();
builder.Services.AddInfrastructureJWT(builder.Configuration);

builder.Services.AddInjection(builder.Configuration);


builder.Services.AddAutoMapper(typeof(ProfileMapper));
builder.Services.AddAutoMapper(typeof(ProfileMapperCrossCutting));
builder.Services.AddHangfireServer();
//builder.Services.AddHostedService<SchedullerB2bBradesco>();
//builder.Services.AddHostedService<SchedullerB2bItau>();
builder.Services.AddHostedService<SchedullerB2bSantander>();
builder.Services.AddHangfireServices(builder.Configuration);
//builder.Services.AddHealthChecksUI(options =>
//{
//    options.SetEvaluationTimeInSeconds(5);
//    options.MaximumHistoryEntriesPerEndpoint(5);
//    options.AddHealthCheckEndpoint("API Health Checks", "/health");
//}).AddInMemoryStorage();

builder.Services.Configure<RouteOptions>(options =>
{
    options.LowercaseUrls = true;
    options.LowercaseQueryStrings = true;
});

builder.Services.Configure<DataBaseSettingsNoSql>(
    builder.Configuration.GetSection("DataBaseSettingsNoSql"));

builder.Services.AddSingleton<CustomersMongoServiceItau>();
//builder.Services.AddSingleton<CustomersMongoServiceSantander>();
//builder.Services.AddSingleton<CustomersMongoServiceBradesco>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
//app.UseHealthChecks("/health", new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions
//{
//    Predicate = p => true,
//    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
//});

//app.UseHealthChecksUI(options =>
//{
//    options.UIPath = "/Dashboard";
//});
//app.UseHealthChecks("/health");
app.UseHttpsRedirection();
app.UseStatusCodePages();	//Especificar as mensagens de retorno de status code.
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
//app.MapHangfireDashboard("/hangfire-b2b");
app.Run();
