﻿using Bic.B2b.Application.CQRS.Accounts.Command;
using Bic.B2b.Application.CQRS.Accounts.Query;
using Bic.B2b.Application.DTO.Request;
using Bic.B2b.Application.Interfaces;
using Bic.B2b.Domain.ModelToken;
using Dapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;

namespace Bic.B2b.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IGeraTokenService _geraTokenService;
        private readonly IConfiguration _configuration;
        private readonly ILogger<TokenController> _logger;

        public TokenController(IGeraTokenService geraTokenService,
                                IMediator mediator,
                                IConfiguration configuration,
                                ILogger<TokenController> logger)
        {
            _geraTokenService = geraTokenService;
            _mediator = mediator;
            _configuration = configuration;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet("get-token")]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetToken(string email)
        {
            _logger.LogInformation($"TokenController/get-token - Início do método get-token para obter token JWT em {DateTimeOffset.Now}.");
            var conn = _configuration.GetValue<string>("ConnectionStrings:ConnMySqlBic");
            string query1 = "Select * from Accounts Where Email = @email";

            using MySqlConnection conexao = new(conn);
            var account = await conexao.QueryFirstOrDefaultAsync<AccountDto>(query1, new { Email = email });
            
            if (account == null)
            {
                return BadRequest(new UserToken());
            }

            var token = _geraTokenService.GenerateTokenService(account);
            _logger.LogInformation($"TokenController/get-token - Fim do método get-token para ober token JWT em {DateTimeOffset.Now}. Sucesso => {token.Result.Sucesso}");
            return Ok(token);

            //return Ok(r);

            //var query = new GetAccountByLoginEmailQuery() { Email = email };
            //var account = await _mediator.Send(query);
            //if (account == null)
            //    return NotFound();

            //var token = _geraTokenService.GenerateTokenService(account);
            //return Ok(token);

        }

        [HttpGet]
        [Route("health")]
        public async Task<IActionResult> Health()
        {
            return Ok();
        }

        //[Authorize]
        [HttpPost("create-user")]
        //[ApiExplorerSettings(IgnoreApi = true)]  LoginModel
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> CreateUser(CreateAccountsCommand userInfo)
        {
            var account = await _mediator.Send(userInfo);
            if (account is not null && account.Id > 0)
            {
                //var token = _geraTokenService.GenerateTokenService(account);
                return Ok(account);
                // return Ok($"User {userInfo.Email} was created successfully");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid Login attempt.");
                return BadRequest(ModelState);
            }
        }

        [AllowAnonymous]
        [HttpGet("get-account")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAccount(int id)
        {
            var query = new GetAccountByIdQuery() { Id = id };
            var account = await _mediator.Send(query);
            return Ok(account);
        }

        [AllowAnonymous]
        [HttpGet("get-accounts")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAccounts()
        {
            var query = new GetAccountsQuery();
            var accounts = await _mediator.Send(query);
            return Ok(accounts);
        }

        //[AllowAnonymous]
        //[HttpPost("auth")]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //public async Task<ActionResult<UserToken>> Login([FromBody] LoginModel userInfo)
        //{
        //    var result = await _authenticateService.Authenticate(userInfo.Email, userInfo.Password);

        //    if (result)
        //    {
        //        //var token = _geraTokenService.GenerateTokenService(userInfo);
        //        //return Ok(token);
        //        return Ok($"User {userInfo.Email} login successfully");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Invalid Login attempt.");
        //        return BadRequest(ModelState);
        //    }
        //}

        
    }
}
