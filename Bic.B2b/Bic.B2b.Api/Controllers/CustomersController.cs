﻿using AutoMapper;
using Bic.B2b.Application.CQRS.Accounts.Query;
using Bic.B2b.Application.CQRS.Customer.Command;
using Bic.B2b.Application.CQRS.Customer.Query;
using Bic.B2b.CrossCutting.ModelMongo;
using Bic.B2b.CrossCutting.Mongo.Interfaces;
using Bic.B2b.Domain.Interfaces.Dapper;
using Bic.B2b.Domain.Interfaces.MQ;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bic.B2b.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly IMQCustomersPublisherSantander _publisherSantander;
        private readonly IMQCustomersPublisherBradesco _publisherBradesco;
        private readonly IMQCustomersPublisherItau _publisherItau;
        private readonly ICustomersMongoServiceItau _customersMongoServices;
        private readonly ICustomersMongoServiceSantander _customersMongoServiceSantander;
        private readonly ICustomersMongoServiceBradesco _customersMongoServiceBradesco;
        private readonly ICustomerDapperRepository _customerDapperRepository;

        public CustomersController(ILogger<CustomersController> logger,
                                IMediator mediator,
                                IMapper mapper,
                                IMQCustomersPublisherBradesco publisherBradesco,
                                IMQCustomersPublisherItau publisherItau,
                                IMQCustomersPublisherSantander publisherSantander,
                                ICustomersMongoServiceItau customersMongoServices,
                                ICustomersMongoServiceSantander customersMongoServiceSantander,
                                ICustomersMongoServiceBradesco customersMongoServiceBradesco,
                                ICustomerDapperRepository customerDapperRepository)
        {
            _logger = logger;
            _mediator = mediator;
            _mapper = mapper;
            _publisherBradesco = publisherBradesco;
            _publisherItau = publisherItau;
            _publisherSantander = publisherSantander;
            _customersMongoServices = customersMongoServices;
            _customersMongoServiceSantander = customersMongoServiceSantander;
            _customersMongoServiceBradesco = customersMongoServiceBradesco;
            _customerDapperRepository = customerDapperRepository;
        }

        #region ::ITAÚ::
        [HttpPost]
        [Route("b2b-itau")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCustomersItau(CreateCustomersCommand customersDto)
        {
            try
            {
                var _customersDto = await _mediator.Send(customersDto);
                if (_customersDto is null)
                {
                    return BadRequest(_customersDto);
                }
                else
                {
                    var customerMap = await CreateCustomersMongo(customersDto);
                    if (customerMap is null)
                    {
                        return BadRequest(_customersDto);
                    }

                    await _customersMongoServices.CreateAsync<CustomersMongoModel>(customerMap);
                    await _publisherItau.PublisherMessageItau(customersDto);
                    return Ok(_customersDto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"{nameof(CreateCustomersItau)} - Erro no método {nameof(CreateCustomersItau)}. Message: {ex.Message} Trace: {ex.StackTrace} em {DateTimeOffset.Now}.");
                throw new Exception($"{nameof(CreateCustomersItau)} - Erro no método {nameof(CreateCustomersItau)}. Message: {ex.Message} Trace: {ex.StackTrace} em {DateTimeOffset.Now}.");
            }
        }
        #endregion

        #region ::SANTANDER:: 
        [HttpGet]
        [Route("get-customer/{id}")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            try
            {
                if (id == 0)
                    return NotFound($"{nameof(GetCustomerById)} - Registro Id: {id} não localizado!");

                var query = new GetCustomersQueryById() { Id = id };
                var customer = await _mediator.Send(query);
                //var customer = await _customerDapperRepository.GetById(id);
                return Ok(customer);

            }
            catch (Exception ex)
            {
                _logger.LogInformation($"{nameof(GetCustomerById)} - Erro no método {nameof(GetCustomerById)}. Message: {ex.Message} Trace: {ex.StackTrace} em {DateTimeOffset.Now}.");
                throw new Exception($"{nameof(GetCustomerById)} - Erro no método {nameof(GetCustomerById)}. Message: {ex.Message} em {DateTimeOffset.Now}.");
            }
        }

        [HttpPost]
        [Route("b2b-santander")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCustomersSantander(CreateCustomersCommand customersDto)
        {
            try
            {
                var _customersDto = await _mediator.Send(customersDto);
                if (_customersDto is null)
                {
                    return BadRequest(_customersDto);
                }
                else
                {
                    var customerMap = await CreateCustomersMongo(customersDto);
                    if (customerMap is null)
                    {
                        return BadRequest(_customersDto);
                    }

                    await _customersMongoServices.CreateAsync<CustomersMongoModel>(customerMap);
                    await _publisherSantander.PublisherMessageSantander(customersDto);
                    return CreatedAtAction(nameof(GetCustomerById), new { id = _customersDto.Id }, _customersDto); //Ok(_customersDto);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"{nameof(CreateCustomersSantander)} - Erro no método {nameof(CreateCustomersSantander)}. Message: {ex.Message} Trace: {ex.StackTrace} em {DateTimeOffset.Now}.");
                throw new Exception($"{nameof(CreateCustomersSantander)} - Erro no método {nameof(CreateCustomersSantander)}. Message: {ex.Message} Trace: {ex.StackTrace} em {DateTimeOffset.Now}.");
            }

        }
        #endregion  

        #region ::BRADESCO::
        [HttpPost]
        [Route("b2b-bradesco")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCustomersBradesco(CreateCustomersCommand customersDto)
        {
            var _customersDto = await _mediator.Send(customersDto);
            
            if (_customersDto is null)
            {
                var customerMap = await CreateCustomersMongo(customersDto);
                await _customersMongoServiceBradesco.CreateAsync<CustomersMongoModel>(customerMap);
                await _publisherBradesco.PublisherMessageBradesco(customersDto);
                return BadRequest(_customersDto);
            }
            else
            {
                return Ok(_customersDto);
            }
                
        }
        #endregion

        private async Task<CustomersMongoModel> CreateCustomersMongo(CreateCustomersCommand customersDto)
        {
            CustomersMongoModelDto customersMongoModelDtoMap = new CustomersMongoModelDto
            {
                CnpjParameter = customersDto.CnpjParameter,
                CodeHtml = customersDto.CodeHtml,
                CodeInternal = customersDto.CodeInternal,
                CNPJConsulted = customersDto.CNPJConsulted,
                NumberRegistration = customersDto.NumberRegistration,
                NameBusiness = customersDto.NameBusiness,
                RegistrationStage = customersDto.RegistrationStage,
                SupplierName = customersDto.SupplierName,
                AddressMongoModelDtos =
                    new AddressMongoModelDto()
                    {
                        Bairro = customersDto.Address.Bairro,
                        CEP = customersDto.Address.CEP,
                        CodeIbge = customersDto.Address.CodeIbge,
                        Complemento = customersDto.Address.Complemento,
                        Logradouro = customersDto.Address.Logradouro,
                        Municipio = customersDto.Address.Municipio,
                        Numero = customersDto.Address.Numero,
                        UF = customersDto.Address.UF,
                        UfParameter = customersDto.Address.UfParameter
                    }
            };

            var customerMap = _mapper.Map<CustomersMongoModel>(customersMongoModelDtoMap);
            return customerMap;
        }
    }
}
