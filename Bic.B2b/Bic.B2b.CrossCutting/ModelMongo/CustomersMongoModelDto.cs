﻿using MongoDB.Bson.Serialization.Attributes;

namespace Bic.B2b.CrossCutting.ModelMongo;

public class CustomersMongoModelDto : CustomersMongoModelDtoBase
{
    public CustomersMongoModelDto()
    {
        AddressMongoModelDtos = new AddressMongoModelDto();
    }
    public string CodeHtml { get; set; }
    public string CodeInternal { get; set; }
    public string CNPJConsulted { get; set; }
    public string NumberRegistration { get; set; }
    public string NameBusiness { get; set; }
    public string RegistrationStage { get; set; }
    public string SupplierName { get; set; }
    [BsonElement("Address")]
    public AddressMongoModelDto AddressMongoModelDtos { get; set; }
}
