﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Bic.B2b.CrossCutting.ModelMongo
{
    public abstract class CustomersMongoModelDtoBase
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        public string CnpjParameter { get; set; } = null!;
    }
}
