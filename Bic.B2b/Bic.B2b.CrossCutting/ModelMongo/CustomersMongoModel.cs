﻿using MongoDB.Bson.Serialization.Attributes;
using Bic.B2b.Infrastructure.ContextMongoDb;

namespace Bic.B2b.CrossCutting.ModelMongo
{
    public class CustomersMongoModel: CustomersMongoModelBase
    {
        public CustomersMongoModel()
        {
            AddressMongoModels = new AddressMongoModel();
        }
        public string CodeHtml { get; set; }
        public string CodeInternal { get; set; }
        public string CNPJConsulted { get; set; }
        public string NumberRegistration { get; set; }
        public string NameBusiness { get; set; }
        public string RegistrationStage { get; set; }
        public string SupplierName { get; set; }
        [BsonElement("Address")]
        public AddressMongoModel AddressMongoModels { get; set; }
    }
}
