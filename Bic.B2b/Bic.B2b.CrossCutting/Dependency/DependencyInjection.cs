﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySqlConnector;
using Bic.B2b.Application.Interfaces;
using Bic.B2b.Application.Services;
using Bic.B2b.CrossCutting.Mongo.Interfaces;
using Bic.B2b.CrossCutting.Mongo.Services;
using Bic.B2b.Domain.Interfaces;
using Bic.B2b.Domain.Interfaces.Dapper;
using Bic.B2b.Domain.Interfaces.MQ;
using Bic.B2b.Domain.Interfaces.Request;
using Bic.B2b.Domain.Token;
using Bic.B2b.Infrastructure.Context;
using Bic.B2b.Infrastructure.MQ;
using Bic.B2b.Infrastructure.Repository;
using Bic.B2b.Infrastructure.Repository.Dapper;
using Bic.B2b.Infrastructure.Request;
using Bic.B2b.Infrastructure.Token;
using System.Data;

namespace Bic.B2b.CrossCutting.Dependency
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInjection(this IServiceCollection services, IConfiguration configuration)
        {
            var mySqlConnection = configuration.GetConnectionString("ConnMySqlBic");
            services.AddDbContext<AppDbContext>(options =>
                                                options.UseMySql(mySqlConnection,
                                                                 ServerVersion.AutoDetect(mySqlConnection)));
            //Connection Dapper
            services.AddSingleton<IDbConnection>(provider =>
            {
                var connection = new MySqlConnection(mySqlConnection);
                connection.Open();
                return connection;
            });

            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWorkRepository>();
            services.AddSingleton<IRequestCustomersDomainBradesco, RequestCustomersBradesco>();            
            services.AddScoped<IMQCustomersPublisherBradesco, MQCustomersPublisherBradesco>();
            services.AddSingleton<IMQCustomersConsumerBradesco, MQCustomersConsumerBradesco>();
            //services.AddScoped<IRequestCustomersApplications, RequestCustomersApplicationServices>();           
            services.AddSingleton<IRequestCustomersDomainItau, RequestCustomersItau>();
            services.AddScoped<IMQCustomersPublisherItau, MQCustomersPublisherItau>();
            services.AddSingleton<IMQCustomersConsumerItau, MQCustomersConsumerItau>();
            services.AddSingleton<IRequestCustomersDomainSantander, RequestCustomersSantander>();
            services.AddScoped<IMQCustomersPublisherSantander, MQCustomersPublisherSantander>();
            services.AddSingleton<IMQCustomersConsumerSantander, MQCustomersConsumerSantander>();
            services.AddScoped<ICustomersMongoServiceItau, CustomersMongoServiceItau>();
            services.AddScoped<ICustomersMongoServiceSantander, CustomersMongoServiceSantander>();
            services.AddScoped<ICustomersMongoServiceBradesco, CustomersMongoServiceBradesco>();

            services.AddScoped<IGeraToken, GeraToken>();
            services.AddScoped<IAuthenticateService, AuthenticateService>();
            services.AddScoped<IGeraTokenService, GeraTokenService>();

            services.AddScoped<IAccountsRepository, AccountsRepository>();
            services.AddSingleton<IAccountDapperRepository, AccountDapperRepository>();
            services.AddSingleton<ICustomerDapperRepository, CustomerDapperRepository>();

            var myhandlers = AppDomain.CurrentDomain.Load("Bic.B2b.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(myhandlers));
            return services;
        }
    }
}
