﻿using Microsoft.Extensions.DependencyInjection;
using Polly;

namespace Bic.B2b.CrossCutting.Extension
{
    public static class HttpClientServicesExtension
    {
        public static IServiceCollection AddConfigurationHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient("clientItau", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7188/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("clientSantander", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7007/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("clientBradesco", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("https://localhost:7236/");
            });

            return services;
        }
    }
}
