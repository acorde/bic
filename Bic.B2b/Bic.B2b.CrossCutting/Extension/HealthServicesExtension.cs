﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bic.B2b.CrossCutting.Extension
{
    public static class HealthServicesExtension
    {
        public static IServiceCollection AddServicesConfigurationHealth(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHealthChecks();

            services.AddHealthChecksUI(options =>
            {
                options.SetEvaluationTimeInSeconds(5);
                options.MaximumHistoryEntriesPerEndpoint(5);
                options.AddHealthCheckEndpoint("API Data", "/health");
            }).AddInMemoryStorage();

            return services;
        }
    }
}
