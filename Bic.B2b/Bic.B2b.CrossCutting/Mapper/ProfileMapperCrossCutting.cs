﻿using AutoMapper;
using Bic.B2b.CrossCutting.ModelMongo;

namespace Bic.B2b.CrossCutting.Mapper
{
    public class ProfileMapperCrossCutting : Profile
    {
        public ProfileMapperCrossCutting()
        {
            CreateMap<CustomersMongoModel, CustomersMongoModelDto>();
            CreateMap<CustomersMongoModelDto, CustomersMongoModel>();
            CreateMap<AddressMongoModelDto, AddressMongoModel>();
            CreateMap<AddressMongoModel, AddressMongoModelDto>();
        }
    }
}
