﻿using Bic.B2b.CrossCutting.ModelMongo;

namespace Bic.B2b.CrossCutting.Mongo.Interfaces
{
    public interface ICustomersMongoServiceItau
    {
        Task CreateAsync<T>(CustomersMongoModel newBook);
    }
}
