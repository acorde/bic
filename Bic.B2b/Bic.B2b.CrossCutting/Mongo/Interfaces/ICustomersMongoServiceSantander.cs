﻿using Bic.B2b.CrossCutting.ModelMongo;

namespace Bic.B2b.CrossCutting.Mongo.Interfaces
{
    public interface ICustomersMongoServiceSantander
    {
        Task CreateAsync<T>(CustomersMongoModel newBook);
    }
}
