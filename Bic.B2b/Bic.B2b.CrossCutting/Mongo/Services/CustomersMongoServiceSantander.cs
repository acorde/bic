﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Bic.B2b.CrossCutting.ModelMongo;
using Bic.B2b.CrossCutting.Mongo.Interfaces;
using Bic.B2b.Infrastructure.ContextMongoDb;

namespace Bic.B2b.CrossCutting.Mongo.Services
{
    public class CustomersMongoServiceSantander : ICustomersMongoServiceSantander
    {
        private readonly IMongoCollection<CustomersMongoModel> _mongoCollection;

        public CustomersMongoServiceSantander(IOptions<DataBaseSettingsNoSql> dataBaseSettingsNoSql)
        {
            var mongoClient = new MongoClient(dataBaseSettingsNoSql.Value.HostCorban);
            var mongoDatabase = mongoClient.GetDatabase(dataBaseSettingsNoSql.Value.DatabaseName);
            _mongoCollection = mongoDatabase.GetCollection<CustomersMongoModel>(dataBaseSettingsNoSql.Value.CollectionNameSXPSantander);
        }

        //public async Task<List<CustomersMongoModel>> GetAsync() =>
        //    await _mongoCollection.Find(_ => true).ToListAsync();

        //public async Task<CustomersMongoModel?> GetAsync(string id) =>
        //    await _mongoCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

        //public async Task UpdateAsync(string id, CustomersMongoModel updatedBook) =>
        //    await _mongoCollection.ReplaceOneAsync(x => x.Id == id, updatedBook);

        //public async Task RemoveAsync(string id) =>
        //    await _mongoCollection.DeleteOneAsync(x => x.Id == id);

        public async Task CreateAsync<T>(CustomersMongoModel newBook) =>
            await _mongoCollection.InsertOneAsync(newBook);

    }
}