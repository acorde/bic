﻿using Bic.B2b.Domain.Model;
using Bic.B2b.Domain.ModelToken;

namespace Bic.B2b.Domain.Token
{
    public interface IGeraToken
    {
        Task<UserToken> GenerateToken(Account userInfo);
    }
}
