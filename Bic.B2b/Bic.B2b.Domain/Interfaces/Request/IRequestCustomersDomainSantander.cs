﻿using Bic.B2b.Domain.Model;

namespace Bic.B2b.Domain.Interfaces.Request
{
    public interface IRequestCustomersDomainSantander
    {
        public Task<Customers> SendGetNet(Customers model);
        public Task<Customers> SendCielo(Customers model);
        public Task<Customers> SendMauaBank(Customers model);
    }
}
