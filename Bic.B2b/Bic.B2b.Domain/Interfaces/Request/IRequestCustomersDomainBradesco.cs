﻿using Bic.B2b.Domain.Model;

namespace Bic.B2b.Domain.Interfaces.Request
{
    public interface IRequestCustomersDomainBradesco
    {
        public Task<Customers> SendCielo<T>(T model);
        public Task<Customers> SendGetNet<T>(T model);
        public Task<Customers> SendDataItau<T>(T model);
        public Task<Customers> SendDataSantander(Customers model);
        public Task<Customers> SendDataBradesco(Customers model);
    }
}
