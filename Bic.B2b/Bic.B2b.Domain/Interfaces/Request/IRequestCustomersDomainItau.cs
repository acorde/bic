﻿using Bic.B2b.Domain.Model;

namespace Bic.B2b.Domain.Interfaces.Request
{
    public interface IRequestCustomersDomainItau
    {
        Task<Customers> SendDataItauGetNet<T>(T model);
        Task<Customers> SendDataItauCielo<T>(T model);
        Task<Customers> SendDataItauMauaBank<T>(T model);
        Task<Customers> SendDataItau<T>(T model);
        Task<Customers> SendDataSantander(Customers model);
        Task<Customers> SendDataBradesco(Customers model);
    }
}
