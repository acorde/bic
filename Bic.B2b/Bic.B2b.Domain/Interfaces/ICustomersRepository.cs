﻿using Bic.B2b.Domain.Model;

namespace Bic.B2b.Domain.Interfaces
{
    public interface ICustomersRepository
    {
        Task<IEnumerable<Customers>> GetCustomers();
        Task<Customers> GetCustomersById(int id);
        Task<Customers> AddCustomers(Customers customer);
        void UpdateCustomers(Customers customer);
        Task<Customers> DeleteCustomers(int id);
    }
}
