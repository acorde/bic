﻿namespace Bic.B2b.Domain.Interfaces.MQ
{
    public interface IMQCustomersPublisherBradesco
    {
        Task PublisherMessageNubank<T>(T message);
        Task PublisherMessageItau<T>(T message);
        Task PublisherMessageBradesco<T>(T message);
        Task PublisherMessageSantander<T>(T message);
    }
}
