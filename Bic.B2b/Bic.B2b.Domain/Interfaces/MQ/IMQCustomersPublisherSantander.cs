﻿namespace Bic.B2b.Domain.Interfaces.MQ
{
    public interface IMQCustomersPublisherSantander
    {
        Task PublisherMessageSantander<T>(T message);
    }
}
