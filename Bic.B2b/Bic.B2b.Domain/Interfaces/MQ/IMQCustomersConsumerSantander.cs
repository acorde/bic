﻿namespace Bic.B2b.Domain.Interfaces.MQ
{
    public interface IMQCustomersConsumerSantander
    {
        Task ConsumerCielo();
        Task ConsumerMauaBank();
        Task ConsumerGetNet();
    }
}
