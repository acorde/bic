﻿namespace Bic.B2b.Domain.Interfaces.MQ
{
    public interface IMQCustomersConsumerBradesco
    {
        Task ConsumerMessageNubank();
        Task ConsumerMessageItau();
        Task ConsumerMessageBradesco();
        Task ConsumerMessageSantander();
    }
}
