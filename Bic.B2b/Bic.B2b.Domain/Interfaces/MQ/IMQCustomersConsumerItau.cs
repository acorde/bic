﻿namespace Bic.B2b.Domain.Interfaces.MQ
{
    public interface IMQCustomersConsumerItau
    {
        Task ConsumerCielo();
        Task ConsumerGetNet();
        Task ConsumerMauaBank();
        Task ConsumerMessageNubank();
        Task ConsumerMessageItau();
        Task ConsumerMessageBradesco();
        Task ConsumerMessageSantander();
    }
}
