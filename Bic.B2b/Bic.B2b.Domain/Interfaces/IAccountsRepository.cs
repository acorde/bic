﻿using Bic.B2b.Domain.Model;

namespace Bic.B2b.Domain.Interfaces
{
    public interface IAccountsRepository
    {
        Task<Account> AddAccounts(Account accounts);
    }
}
