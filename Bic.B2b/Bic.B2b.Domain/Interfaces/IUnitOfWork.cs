﻿namespace Bic.B2b.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomersRepository CustomersRepository { get; }
        IAccountsRepository AccountsRepository { get; }
        Task CommitAsync();
    }
}
