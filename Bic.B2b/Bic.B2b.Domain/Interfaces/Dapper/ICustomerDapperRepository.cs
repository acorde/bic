﻿using Bic.B2b.Domain.Model.Response;

namespace Bic.B2b.Domain.Interfaces.Dapper
{
    public interface ICustomerDapperRepository
    {
        Task<CustomerResponse?> GetById(int id);
    }
}
