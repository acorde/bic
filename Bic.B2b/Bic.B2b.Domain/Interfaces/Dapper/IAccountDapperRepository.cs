﻿using Bic.B2b.Domain.Model;

namespace Bic.B2b.Domain.Interfaces.Dapper
{
    public interface IAccountDapperRepository
    {
        Task<IEnumerable<Account>> GetAccounts();
        Task<Account> GetAccountById(int id);
        Task<Account?> GetAccountByEmail(string email);
    }
}
