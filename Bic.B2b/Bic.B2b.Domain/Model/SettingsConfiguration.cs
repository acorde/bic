﻿using Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit;

namespace Bic.B2b.Domain.Model
{
    public sealed class SettingsConfiguration
    {
        public bool? Log { get; set; }
        public string? ConnectionStringId { get; set; }
        public UriSettings? UriSettings { get; set; }
        public JwtOptions? JwtOptions { get; set; }
        public RabbitSettings RabbitSettings { get; set; }
        public RabbitQueueName RabbitQueueName { get; set; }
    }
}
