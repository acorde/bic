﻿namespace Bic.B2b.Domain.Model
{
    public class RabbitSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
