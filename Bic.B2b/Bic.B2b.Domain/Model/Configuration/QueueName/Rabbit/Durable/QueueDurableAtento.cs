﻿namespace Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit.Durable
{
    public class QueueDurableAtento
    {
        public string Cielo { get; set; }
        public string GetNet { get; set; }
        public string MauaBank { get; set; }
    }
}
