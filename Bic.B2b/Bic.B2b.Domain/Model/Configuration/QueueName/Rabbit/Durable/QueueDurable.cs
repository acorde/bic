﻿namespace Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit.Durable
{
    public class QueueDurable
    {
        public QueueDurableThato QueueDurableThato { get; set; }
        public QueueDurableAtento QueueDurableAtento { get; set; }
        public QueueDurableCallLink QueueDurableCallLink { get; set; }
    }
}
