﻿namespace Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit.Dlq
{
    public class QueueDlqCallLink
    {
        public string Cielo { get; set; }
        public string GetNet { get; set; }
        public string MauaBank { get; set; }
    }
}
