﻿namespace Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit.Dlq
{
    public class QueueDlq
    {
        public QueueDlqThato QueueDlqThato { get; set; }
        public QueueDlqAtento QueueDlqAtento { get; set; }
        public QueueDlqCallLink QueueDlqCallLink { get; set; }
    }
}
