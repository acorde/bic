﻿using Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit.Dlq;
using Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit.Durable;

namespace Bic.B2b.Domain.Model.Configuration.QueueName.Rabbit
{
    public class RabbitQueueName
    {
        public QueueDurable QueueDurable { get; set; }
        public QueueDlq QueueDlq { get; set; }
    }
}
