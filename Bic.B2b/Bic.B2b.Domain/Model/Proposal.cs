﻿namespace Bic.B2b.Domain.Model
{
    public class Proposal
    {
        public int Id { get; set; }
        public decimal? Valor { get; set; }
        public int? CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; } 

    }
}
