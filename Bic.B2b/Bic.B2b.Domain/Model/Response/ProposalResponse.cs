﻿namespace Bic.B2b.Domain.Model.Response
{
    public class ProposalResponse
    {
        public int ProposalId { get; set; }
        public decimal? Valor { get; set; }
        public int? CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; } 
    }
}
