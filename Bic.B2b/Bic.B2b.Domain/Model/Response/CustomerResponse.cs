﻿namespace Bic.B2b.Domain.Model.Response
{
    public class CustomerResponse
    {
        public CustomerResponse()
        {
            Address = new AddressResponse();
            Proposals = new List<ProposalResponse>();            
        }
        public int CustomersId { get; set; }
        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public AddressResponse? Address { get; set; }
        public List<ProposalResponse>? Proposals { get; set; }

    }
}
