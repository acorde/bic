﻿namespace Bic.B2b.Domain.Model
{
    public sealed class UriSettings
    {
        public bool? IsProduction { get; set; }
        public string UriItau { get; set; }
        public string UriSantander { get; set; }
        public string UriSantanderGetNet { get; set; }
        public string UriSantanderMauaBank { get; set; }
        public string UriBradesco { get; set; }

        public string UriItauGetNet { get; set; }
        public string UriItauCielo { get; set; }
        public string UriItauMauaBank { get; set; }

        public string UriBradescoGetNet { get; set; }
        public string UriBradescoCielo { get; set; }

    }
}
