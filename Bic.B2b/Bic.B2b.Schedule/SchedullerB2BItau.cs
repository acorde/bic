﻿using Bic.B2b.Domain.Interfaces.MQ;
using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Bic.B2b.Schedule
{
    public class SchedullerB2bItau : IHostedService
    {
        private readonly IMQCustomersConsumerItau _customersConsumerItau;
        private readonly ILogger<SchedullerB2bItau> _logger;

        public SchedullerB2bItau(IMQCustomersConsumerItau customersConsumerItau, ILogger<SchedullerB2bItau> logger)
        {
            _customersConsumerItau = customersConsumerItau;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                RecurringJob.AddOrUpdate("enviar-dados-para-fila-thato-b2b-getnet", () => ConsumerGetNet(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("enviar-dados-para-fila-thato-b2b-cielo", () => ConsumerCielo(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("enviar-dados-para-fila-thato-b2b-mauabank", () => ConsumerMauaBank(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("b2b-customers-santander", () => ConsumerMessageSantander(), MinuteInterval(2));
                //RecurringJob.AddOrUpdate("Enviar-dados-para-fila-thato-b2b", () => ConsumerMessageItau(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("nbk-customers", () => ConsumerMessageNubank(), MinuteInterval(1));

            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerGetNet()
        {
            try
            {
                await _customersConsumerItau.ConsumerGetNet();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire GetNet - Schedule: {ex.Message}.");
            }

        }

        public async Task ConsumerCielo()
        {
            try
            {
                await _customersConsumerItau.ConsumerCielo();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Cielo - Schedule: {ex.Message}.");
            }

        }

        public async Task ConsumerMauaBank()
        {
            try
            {
                await _customersConsumerItau.ConsumerMauaBank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Maua Bank - Schedule: {ex.Message}.");
            }

        }

        public async Task ConsumerMessageSantander()
        {
            try
            {
                await _customersConsumerItau.ConsumerMessageSantander();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Santander - Schedule: {ex.Message}.");
            }

        }
        public async Task ConsumerMessageItau()
        {
            try
            {
                await _customersConsumerItau.ConsumerMessageItau();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Thato - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageNubank()
        {
            try
            {
                await _customersConsumerItau.ConsumerMessageNubank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}.");
            }
        }

    }
}
