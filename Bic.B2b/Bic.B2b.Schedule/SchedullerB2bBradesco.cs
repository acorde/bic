﻿using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Bic.B2b.Domain.Interfaces.MQ;

namespace Bic.B2b.Schedule
{
    public class SchedullerB2bBradesco : IHostedService
    {
        private readonly IMQCustomersConsumerBradesco _customersConsumerBradesco;
        private readonly ILogger<SchedullerB2bBradesco> _logger;

        public SchedullerB2bBradesco(IMQCustomersConsumerBradesco customersConsumerBradesco, ILogger<SchedullerB2bBradesco> logger)
        {
            _customersConsumerBradesco = customersConsumerBradesco;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                //RecurringJob.AddOrUpdate("b2b-customers-santander", () => ConsumerMessageSantander(), MinuteInterval(2));
                //RecurringJob.AddOrUpdate("b2b-customers-itau", () => ConsumerMessageItau(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("nbk-customers", () => ConsumerMessageNubank(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("b2b-customers-bradesco", () => ConsumerMessageBradesco(), MinuteInterval(1));
            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerMessageSantander()
        {
            try
            {
                await _customersConsumerBradesco.ConsumerMessageSantander();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Santander - Schedule: {ex.Message}.");
            }

        }
        public async Task ConsumerMessageItau()
        {
            try
            {
                await _customersConsumerBradesco.ConsumerMessageItau();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Itaú - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageNubank()
        {
            try
            {
                await _customersConsumerBradesco.ConsumerMessageNubank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}.");
            }
        }

        public async Task ConsumerMessageBradesco()
        {
            try
            {
                await _customersConsumerBradesco.ConsumerMessageBradesco();
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERRO: {ex.Message}.");
            }
        }
    }
}
