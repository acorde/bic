﻿using Bic.B2b.Domain.Interfaces.MQ;
using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Bic.B2b.Schedule
{
    public class SchedullerB2bSantander : IHostedService
    {
        private readonly IMQCustomersConsumerSantander _customers;
        private readonly ILogger<SchedullerB2bSantander> _logger;

        public SchedullerB2bSantander(IMQCustomersConsumerSantander customers, 
                                    ILogger<SchedullerB2bSantander> logger)
        {
            _customers = customers;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                RecurringJob.AddOrUpdate("atento-b2b-getnet", () => ConsumerGetNet(), MinuteInterval(2));
                RecurringJob.AddOrUpdate("atento-b2b-cielo", () => ConsumerCielo(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("atento-b2b-mauabank", () => ConsumerMauaBank(), MinuteInterval(1));
            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerGetNet()
        {
            try
            {
                await _customers.ConsumerGetNet();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Santander GetNet- Schedule: {ex.Message}.");
            }

        }
        public async Task ConsumerCielo()
        {
            try
            {
                await _customers.ConsumerCielo();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Santander CIelo - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerMauaBank()
        {
            try
            {
                await _customers.ConsumerMauaBank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire Santander Maua Bank - Schedule: {ex.Message}.");
            }
        }
    }
}
