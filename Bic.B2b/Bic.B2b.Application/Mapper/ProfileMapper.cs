﻿using AutoMapper;
using Bic.B2b.Application.DTO.Request;
using Bic.B2b.Application.DTO.Response;
using Bic.B2b.Domain.Model;
using Bic.B2b.Domain.Model.Response;

namespace Bic.B2b.Application.Mapper
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {

            CreateMap<CustomerResponse, CustomerResponseDto>().ReverseMap();
            CreateMap<AddressResponse, AddressResponseDto>().ReverseMap();
            CreateMap<ProposalResponse, ProposalResponseDto>().ReverseMap();

            CreateMap<ProposalResponseDto, Proposal>().ReverseMap();
            CreateMap<AddressResponseDto, Address>().ReverseMap();
            CreateMap<CustomerResponseDto, Customers>().ReverseMap();
            CreateMap<ProposalDto, Proposal>().ReverseMap();
            CreateMap<AccountDto, Account>().ReverseMap();
            CreateMap<CustomerDto, Customers>().ReverseMap();
            CreateMap<AddressDto, Address>().ReverseMap();
        }
    }
}
