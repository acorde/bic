﻿using Bic.B2b.Application.DTO.Request;
using Bic.B2b.Domain.ModelToken;

namespace Bic.B2b.Application.Interfaces
{
    public interface IGeraTokenService
    {
        Task<UserToken> GenerateTokenService(AccountDto userInfo);
    }
}
