﻿using AutoMapper;
using Bic.B2b.Application.DTO.Request;
using Bic.B2b.Application.Interfaces;
using Bic.B2b.Domain.Model;
using Bic.B2b.Domain.ModelToken;
using Bic.B2b.Domain.Token;

namespace Bic.B2b.Application.Services
{
    public class GeraTokenService : IGeraTokenService
    {
        public readonly IGeraToken _geraToken;
        private readonly IMapper _mapper;

        public GeraTokenService(IGeraToken geraToken, IMapper mapper)
        {
            _geraToken = geraToken;
            _mapper = mapper;
        }

        public async Task<UserToken> GenerateTokenService(AccountDto userInfo)
        {
            var accountMap = _mapper.Map<Account>(userInfo);
            var token = await _geraToken.GenerateToken(accountMap);
            return token;
        }
    }
}
