﻿namespace Bic.B2b.Application.DTO.Request
{
    public class AccountDto
    {
        public AccountDto(string email)
        {
            Email = email;
        }

        public AccountDto(int id, string name, string email, string login, string password)
        {
            Id = id;
            Name = name;
            Email = email;
            Login = login;
            Password = password;
        }

        public int? Id { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Login { get; set; }
        public string? Password { get; set; }
    }
}
