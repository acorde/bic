﻿namespace Bic.B2b.Application.DTO.Request
{
    public class ProposalDto
    {
        public ProposalDto()
        {

        }
        public ProposalDto(int id, int customersId, decimal? valor, DateTime? updatedAt, DateTime? createdAt)
        {
            Id = id;
            Valor = valor;
            CustomersId = customersId;
            UpdatedAt = updatedAt;
            CreatedAt = createdAt;
        }

        public int Id { get; set; }
        public decimal? Valor { get; set; }
        public int CustomersId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }

    }
}
