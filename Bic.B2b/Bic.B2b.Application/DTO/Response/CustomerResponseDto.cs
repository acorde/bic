﻿namespace Bic.B2b.Application.DTO.Response
{
    public class CustomerResponseDto
    {
        public CustomerResponseDto()
        {
            Address = new AddressResponseDto();
            Proposals = new List<ProposalResponseDto>();
        }
        public int CustomersId { get; set; }
        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public AddressResponseDto? Address { get; set; }
        public List<ProposalResponseDto>? Proposals { get; set; }
    }
}
