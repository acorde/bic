﻿using AutoMapper;
using MediatR;
using Bic.B2b.Domain.Interfaces.Dapper;
using Bic.B2b.Application.DTO.Request;

namespace Bic.B2b.Application.CQRS.Accounts.Query
{
    public class GetAccountsQueryHandler : IRequestHandler<GetAccountsQuery, IEnumerable<AccountDto>>
    {
        private readonly IAccountDapperRepository _memberRepository;
        private readonly IMapper _mapper;
        public GetAccountsQueryHandler(IAccountDapperRepository memberRepository, IMapper mapper)
        {
            _memberRepository = memberRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<AccountDto>> Handle(GetAccountsQuery request, CancellationToken cancellationToken)
        {
            var accounts = await _memberRepository.GetAccounts();
            var accountsMap = _mapper.Map<IEnumerable<AccountDto>>(accounts);
            return accountsMap;
        }
    }
}
