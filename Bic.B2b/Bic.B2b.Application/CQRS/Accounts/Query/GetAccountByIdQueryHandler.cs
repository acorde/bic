﻿using AutoMapper;
using MediatR;
using Bic.B2b.Domain.Interfaces.Dapper;
using Bic.B2b.Application.DTO.Request;

namespace Bic.B2b.Application.CQRS.Accounts.Query
{
    public class GetAccountByIdQueryHandler : IRequestHandler<GetAccountByIdQuery, AccountDto>
    {
        private readonly IAccountDapperRepository _memberRepository;
        private readonly IMapper _mapper;
        public GetAccountByIdQueryHandler(IAccountDapperRepository memberRepository, IMapper mapper)
        {
            _memberRepository = memberRepository;
            _mapper = mapper;
        }

        public async Task<AccountDto> Handle(GetAccountByIdQuery request, CancellationToken cancellationToken)
        {
            var account = await _memberRepository.GetAccountById(request.Id);
            var accountMap = _mapper.Map<AccountDto>(account);
            return accountMap;
        }
    }
}
