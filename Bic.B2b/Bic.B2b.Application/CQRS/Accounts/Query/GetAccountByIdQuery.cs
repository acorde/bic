﻿using MediatR;
using Bic.B2b.Application.DTO.Request;

namespace Bic.B2b.Application.CQRS.Accounts.Query
{
    public class GetAccountByIdQuery : IRequest<AccountDto>
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Login { get; set; }
        public string? Password { get; set; }

    }
}
