﻿using Bic.B2b.Application.DTO.Request;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Bic.B2b.Application.CQRS.Accounts.Query
{
    public class GetAccountByLoginEmailQuery : IRequest<AccountDto>
    {
        //[Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public string Email { get; set; }
    }
}
