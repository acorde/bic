﻿using AutoMapper;
using MediatR;
using Bic.B2b.Domain.Interfaces.Dapper;
using System.Security.Principal;
using Bic.B2b.Application.DTO.Request;

namespace Bic.B2b.Application.CQRS.Accounts.Query
{
    public class GetAccountByLoginEmailQueryHandler : IRequestHandler<GetAccountByLoginEmailQuery, AccountDto>
    {
        private readonly IAccountDapperRepository _memberRepository;
        private readonly IMapper _mapper;
        public GetAccountByLoginEmailQueryHandler(IAccountDapperRepository memberRepository, IMapper mapper)
        {
            _memberRepository = memberRepository;
            _mapper = mapper;
        }

        public async Task<AccountDto> Handle(GetAccountByLoginEmailQuery request, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(request.Email))
            {
                var retAccount = await _memberRepository.GetAccountByEmail(request.Email);
                var retAccountDtoMap = _mapper.Map<AccountDto>(retAccount);
                return retAccountDtoMap;
            }
            else
            {
                var retAccountDtoMap = _mapper.Map<AccountDto>(request);
                return retAccountDtoMap;
            }

        }
    }
}
