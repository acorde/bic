﻿using AutoMapper;
using MediatR;
using Bic.B2b.Domain.Interfaces;
using Bic.B2b.Domain.Model;
using Bic.B2b.Application.DTO.Request;

namespace Bic.B2b.Application.CQRS.Accounts.Command
{
    public class CreateAccountsCommandHandler : IRequestHandler<CreateAccountsCommand, AccountDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateAccountsCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<AccountDto> Handle(CreateAccountsCommand request, CancellationToken cancellationToken)
        {
            var accountDto = new AccountDto(request.Id, request.Name, request.Email, request.Login, request.Password);
            var accountMap = _mapper.Map<Account>(accountDto);
            await _unitOfWork.AccountsRepository.AddAccounts(accountMap);
            await _unitOfWork.CommitAsync();
            var accountDtoMap = _mapper.Map<AccountDto>(accountMap);
            return accountDtoMap;
        }
    }
}
