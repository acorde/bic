﻿using AutoMapper;
using Bic.B2b.Application.DTO.Response;
using Bic.B2b.Domain.Interfaces.Dapper;
using MediatR;

namespace Bic.B2b.Application.CQRS.Customer.Query
{
    public class GetCustomersQueryByIdHandler : IRequestHandler<GetCustomersQueryById, CustomerResponseDto>
    {
        private readonly ICustomerDapperRepository _repository;
        private readonly IMapper _mapper;

        public GetCustomersQueryByIdHandler(ICustomerDapperRepository repository,   
                                        IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CustomerResponseDto> Handle(GetCustomersQueryById req, CancellationToken cancellationToken)
        {
            var retCustomer = await _repository.GetById(req.Id);
            var customerDto = _mapper.Map<CustomerResponseDto>(retCustomer);
            return customerDto;
        }
    }
}
