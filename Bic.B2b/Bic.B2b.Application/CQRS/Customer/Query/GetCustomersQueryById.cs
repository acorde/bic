﻿using Bic.B2b.Application.DTO.Response;
using MediatR;

namespace Bic.B2b.Application.CQRS.Customer.Query
{
    public class GetCustomersQueryById : IRequest<CustomerResponseDto>
    {
        public int Id { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public AddressResponseDto? Address { get; set; }
        public List<CustomerResponseDto>? Proposals { get; set; }
    }
}
