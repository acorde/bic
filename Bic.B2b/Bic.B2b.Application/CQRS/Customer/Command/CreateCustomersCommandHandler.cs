﻿using AutoMapper;
using Bic.B2b.Application.DTO.Request;
using Bic.B2b.Domain.Interfaces;
using Bic.B2b.Domain.Model;
using MediatR;

namespace Bic.B2b.Application.CQRS.Customer.Command
{
    public class CreateCustomersCommandHandler : IRequestHandler<CreateCustomersCommand, CustomerDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateCustomersCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CustomerDto> Handle(CreateCustomersCommand request, CancellationToken cancellationToken)
        {
            var customerDto = new CustomerDto(request.CodeHtml, request.CodeInternal, request.CnpjParameter, request.CNPJConsulted
                                                , request.NumberRegistration, request.NameBusiness, 
                                                  request.RegistrationStage, request.SupplierName
                                                , request.Address, request.Proposals);

            var customerMap = _mapper.Map<Customers>(customerDto);
            await _unitOfWork.CustomersRepository.AddCustomers(customerMap);
            await _unitOfWork.CommitAsync();
            var ret = _mapper.Map<CustomerDto>(customerMap);
            return ret;
        }
    }
}
