﻿using Bic.B2b.Application.DTO.Request;
using MediatR;

namespace Bic.B2b.Application.CQRS.Customer.Command
{
    public class CreateCustomersCommand : IRequest<CustomerDto>
    {
        public string? CodeHtml { get; set; }
        public string? CodeInternal { get; set; }
        public string? CnpjParameter { get; set; }
        public string? CNPJConsulted { get; set; }
        public string? NumberRegistration { get; set; }
        public string? NameBusiness { get; set; }
        public string? RegistrationStage { get; set; }
        public string? SupplierName { get; set; }
        public AddressDto? Address { get; set; }
        public List<ProposalDto>? Proposals { get; set; }
    }
}
