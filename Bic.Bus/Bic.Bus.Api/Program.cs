using Bic.Bus.Application.Mapper;
using Bic.Bus.CrossCutting.AppSettings;
using Bic.Bus.CrossCutting.Dependency;
using Bic.Bus.CrossCutting.Extension;
using Bic.Bus.Schedule;
using Hangfire;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpClientBus();
builder.Services.ConfigureServices(builder.Configuration);
builder.Services.AddInjection(builder.Configuration);

builder.Services.AddSwaggerGen(c => {
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Bic.Bus.Api",
        Version = "v1",
        Contact = new OpenApiContact
        {
            Name = "Bus",
            Email = "contato@teste.com.br",
            Url = new Uri("https://google.com")
        }
    });

    var xmlFile = "DocumentingSwagger.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    //c.IncludeXmlComments(xmlPath);
});
builder.Services.Configure<RouteOptions>(options =>
{
    options.LowercaseUrls = true;
    options.LowercaseQueryStrings = true;
});

builder.Services.AddHangfireServer();
builder.Services.AddHangfireServerBic(builder.Configuration);
builder.Services.AddAutoMapper(typeof(ProfileMapper));
builder.Services.AddHostedService<Schedule>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.MapHangfireDashboard("/hangfire-bus");
app.Run();
