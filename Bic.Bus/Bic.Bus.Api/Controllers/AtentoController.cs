﻿using Bic.Bus.Application.CQRS.Customer.Command;
using Bic.Bus.Domain.Interfaces.MQ;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Bic.Bus.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AtentoController : ControllerBase
    {
        private readonly IMediator _mediatR;
        private readonly ILogger<AtentoController> _logger;
        private readonly IMQCustomersPublisher _publisher;

        public AtentoController(IMediator mediatR, 
                                ILogger<AtentoController> logger, 
                                IMQCustomersPublisher publisher)
        {
            _mediatR = mediatR;
            _logger = logger;
            _publisher = publisher;
        }

        [HttpPost]
        [Route("card-cielo")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCardCielo(CreateCustomersCommand customerDto)
        {
            var _customersDto = await _mediatR.Send(customerDto);
            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                await _publisher.PublisherCielo(customerDto);
                return Ok(customerDto);
            }
        }

        [HttpPost]
        [Route("card-getnet")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCardGetNet(CreateCustomersCommand customerDto)
        {
            var _customersDto = await _mediatR.Send(customerDto);
            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                await _publisher.PublisherGetNet(customerDto);
                return Ok(customerDto);
            }
        }

        [HttpPost]
        [Route("card-mauabank")]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(CreateCustomersCommand), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCardMauaBank(CreateCustomersCommand customerDto)
        {
            var _customersDto = await _mediatR.Send(customerDto);
            if (_customersDto is null)
            {
                return BadRequest(_customersDto);
            }
            else
            {
                await _publisher.PublisherMauaBank(customerDto);
                return Ok(customerDto);
            }
        }
    }
}
