﻿using AutoMapper;
using Bic.Bus.Application.DTO;
using Bic.Bus.Domain.Model;

namespace Bic.Bus.Application.Mapper
{
    public class ProfileMapper : Profile
    {
        public ProfileMapper()
        {
            CreateMap<CustomersDto, Customers>();
            CreateMap<Customers, CustomersDto>();
            CreateMap<AddressDto, Address>();
            CreateMap<Address, AddressDto>();
        }
    }
}
