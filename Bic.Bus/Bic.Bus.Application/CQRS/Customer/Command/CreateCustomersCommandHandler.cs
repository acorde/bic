﻿using AutoMapper;
using MediatR;
using Bic.Bus.Application.DTO;
using Bic.Bus.Domain.Model;
using Bic.Bus.Domain.Interfaces.Repository;

namespace Bic.Bus.Application.CQRS.Customer.Command
{
    public class CreateCustomersCommandHandler : IRequestHandler<CreateCustomersCommand, CustomersDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateCustomersCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CustomersDto> Handle(CreateCustomersCommand request, CancellationToken cancellationToken)
        {
            var customerDto = new CustomersDto(request.CodeHtml, request.CodeInternal, request.CnpjParameter, request.CNPJConsulted
                                                , request.NumberRegistration, request.NameBusiness, request.RegistrationStage, request.SupplierName
                                                , request.Address);

            var customers = _mapper.Map<Customers>(customerDto);
            await _unitOfWork.CustomersRepository.AddCustomers(customers);
            await _unitOfWork.CommitAsync();
            var resultCustomer = _mapper.Map<CustomersDto>(customers);

            return resultCustomer;
        }
    }
}
