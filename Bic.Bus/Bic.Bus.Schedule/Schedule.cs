﻿using Bic.Bus.Domain.Interfaces.Kafka;
using Bic.Bus.Domain.Interfaces.MQ;
using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Bic.Bus.Schedule
{
    public class Schedule : IHostedService
    {
        private readonly IMQCustomersConsumer _customersConsumer;
        private readonly ILogger<Schedule> _logger;
        private readonly IMQKafkaConsumer _mqKafkaConsumer;

        public Schedule(IMQCustomersConsumer customersConsumer, 
                        ILogger<Schedule> logger, 
                        IMQKafkaConsumer mqKafkaConsumer)
        {
            _customersConsumer = customersConsumer;
            _logger = logger;
            _mqKafkaConsumer = mqKafkaConsumer;
        }


        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await AgendarFilas();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private async Task AgendarFilas()
        {
            await Task.Run(() =>
            {
                RecurringJob.AddOrUpdate("atento-getnet", () => ConsumerGetNet(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("atento-cielo", () => ConsumerCielo(), MinuteInterval(1));
                RecurringJob.AddOrUpdate("atento-mauaBank", () => ConsumerMauaBank(), MinuteInterval(1));
                //RecurringJob.AddOrUpdate("thato-kafka-cielo", () => ConsumerKafka(), MinuteInterval(1));
            });
        }

        public static string MinuteInterval(int interval)
        {
            return $"*/{interval} * * * *";
        }

        public async Task ConsumerKafka()
        {
            try
            {
                await _mqKafkaConsumer.ConsumerKafka();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire: Kafka - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerGetNet()
        {
            try
            {
                await _customersConsumer.ConsumerGetNet();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire: Thato bandeira GetNet - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerCielo()
        {
            try
            {
                await _customersConsumer.ConsumerCielo();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire: Atento bandeira Cielo - Schedule: {ex.Message}.");
            }
        }

        public async Task ConsumerMauaBank()
        {
            try
            {
                await _customersConsumer.ConsumerMauaBank();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Hangfire: Thato bandeira Maua Bank - Schedule: {ex.Message}.");
            }
        }
    }   
}
