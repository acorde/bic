﻿namespace Bic.Bus.Domain.Model
{

    public class Card
    {
        public int Id { get; set; }
        public string NumberCard { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string NameCard { get; set; }
        public string Band { get; set; }
        public string SecurityCode { get; set; }
        public int CustomersId { get; set; }
    }
   
}
