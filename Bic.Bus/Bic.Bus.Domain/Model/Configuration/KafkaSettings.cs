﻿namespace Bic.Bus.Domain.Model.Configuration
{
    public class KafkaSettings
    {
        public string Host { get; set; }
        public string Topic { get; set; }
    }
}
