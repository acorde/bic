﻿using Bic.Bus.Domain.Model.Configuration.QueueName.Kafka.Dlq;
using Bic.Bus.Domain.Model.Configuration.QueueName.Kafka.Durable;

namespace Bic.Bus.Domain.Model.Configuration.QueueName.Kafka
{
    public class KafkaQueueName
    {
        public QueueDurable QueueDurable { get; set; }
        public QueueDlq QueueDlq { get; set; }
    }
}
