﻿namespace Bic.Bus.Domain.Model.Configuration.QueueName.Kafka.Dlq
{
    public class QueueDlqAtento
    {
        public string Cielo { get; set; }
        public string GetNet { get; set; }
        public string MauaBank { get; set; }
    }
}
