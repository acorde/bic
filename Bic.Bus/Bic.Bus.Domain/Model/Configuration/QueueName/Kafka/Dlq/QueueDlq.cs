﻿namespace Bic.Bus.Domain.Model.Configuration.QueueName.Kafka.Dlq
{
    public class QueueDlq
    {
        public QueueDlqThato QueueDlqThato { get; set; }
        public QueueDlqAtento QueueDlqAtento { get; set; }
        public QueueDlqCallLink QueueDlqCallLink { get; set; }
    }
}
