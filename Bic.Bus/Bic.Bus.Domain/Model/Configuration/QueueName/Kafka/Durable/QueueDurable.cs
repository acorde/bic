﻿namespace Bic.Bus.Domain.Model.Configuration.QueueName.Kafka.Durable
{
    public class QueueDurable
    {
        public QueueDurableThato QueueDurableThato { get; set; }
        public QueueDurableAtento QueueDurableAtento { get; set; }
        public QueueDurableCallLink QueueDurableCallLink { get; set; }
    }
}
