﻿namespace Bic.Bus.Domain.Model.Configuration.QueueName.Kafka.Durable
{
    public class QueueDurableAtento
    {
        public string Cielo { get; set; }
        public string GetNet { get; set; }
        public string MauaBank { get; set; }
    }
}
