﻿using Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit.Dlq;
using Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit.Durable;

namespace Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit
{
    public class RabbitQueueName
    {
        public QueueDurable? QueueDurable { get; set; }
        public QueueDlq? QueueDlq { get; set; }
    }
}
