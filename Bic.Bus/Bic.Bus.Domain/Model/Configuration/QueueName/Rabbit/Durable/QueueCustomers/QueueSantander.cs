﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit.Durable.QueueCustomers
{
    public class QueueSantander
    {
        public string? Cielo { get; set; }
        public string? GetNet { get; set; }
        public string? MauaBank { get; set; }
    }
}
