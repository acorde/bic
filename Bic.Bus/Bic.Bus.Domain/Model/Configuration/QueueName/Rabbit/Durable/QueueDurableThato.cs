﻿using Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit.Dlq.QueueCustomers;

namespace Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit.Durable
{
    public class QueueDurableThato
    {
        public QueueBradesco? QueueBradesco { get; set; }
        public QueueItau? QueueItau { get; set; }
        public QueueSantander? QueueSantander { get; set; }
    }
}
