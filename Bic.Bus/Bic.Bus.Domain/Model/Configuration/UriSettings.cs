﻿namespace Bic.Bus.Domain.Model.Configuration
{
    public sealed class UriSettings
    {
        public bool? IsProduction { get; set; }
        public string UriCielo { get; set; }
        public string UriGetNet { get; set; }
        public string UriMauaBank { get; set; }
        public string UriCardAmericanas { get; set; }
        public string UriCardAssai { get; set; }
        public string UriCardRenner { get; set; }
    }
}
