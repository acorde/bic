﻿using Bic.Bus.Domain.Model.Configuration.QueueName.Kafka;
using Bic.Bus.Domain.Model.Configuration.QueueName.Rabbit;

namespace Bic.Bus.Domain.Model.Configuration
{
    public sealed class SettingsConfiguration
    {
        public bool? Log { get; set; }
        public string? ConnectionStringId { get; set; }
        public UriSettings? UriSettings { get; set; }
        public RabbitSettings? RabbitMQSettings { get; set; }
        public RabbitQueueName? RabbitQueueName { get; set; }
        public KafkaSettings? KafkaSettings { get; set; }
        public KafkaQueueName? KafkaQueueName { get; set; }
        
    }
}
