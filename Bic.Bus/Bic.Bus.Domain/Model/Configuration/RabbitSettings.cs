﻿namespace Bic.Bus.Domain.Model.Configuration
{
    public class RabbitSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
