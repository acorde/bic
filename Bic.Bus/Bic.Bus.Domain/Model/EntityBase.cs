﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bic.Bus.Domain.Model
{
    public abstract class EntityBase
    {
        public int Id { get; set; }
    }
}
