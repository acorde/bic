﻿namespace Bic.Bus.Domain.Interfaces.MQ
{
    public interface IMQCustomersConsumer
    {
        Task ConsumerCielo();
        Task ConsumerGetNet();
        Task ConsumerMauaBank();
    }
}
