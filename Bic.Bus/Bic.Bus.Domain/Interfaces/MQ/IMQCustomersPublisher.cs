﻿namespace Bic.Bus.Domain.Interfaces.MQ
{
    public interface IMQCustomersPublisher
    {
        Task PublisherCielo<T>(T message);
        Task PublisherGetNet<T>(T message);
        Task PublisherMauaBank<T>(T message);
    }
}
