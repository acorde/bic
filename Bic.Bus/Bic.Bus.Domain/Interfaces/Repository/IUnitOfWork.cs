﻿using Bic.Bus.Domain.Model;

namespace Bic.Bus.Domain.Interfaces.Repository
{
    public interface IUnitOfWork
    {
        ICustomersRepository CustomersRepository { get; }
        Task CommitAsync();
    }
}
