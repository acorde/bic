﻿namespace Bic.Bus.Domain.Interfaces.Request
{
    public interface IRequestCustomersDomain
    {
        public Task<bool> SendGetNet<T>(T model);
        public Task<bool> SendCielo<T>(T model);
        public Task<bool> SendMauaBank<T>(T model);
    }
}
