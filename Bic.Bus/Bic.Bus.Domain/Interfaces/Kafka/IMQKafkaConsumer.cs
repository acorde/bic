﻿using Bic.Bus.Domain.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bic.Bus.Domain.Interfaces.Kafka
{
    public interface IMQKafkaConsumer
    {
        Task ConsumerKafka();
    }
}
