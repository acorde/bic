﻿using Bic.Bus.Domain.ResponseModel;

namespace Bic.Bus.Domain.Interfaces.Kafka
{
    public interface IMQKafkaPublisher
    {
        Task SendKafka(ResponseCustomers message, string nameQueue);
    }
}
