﻿using Bic.Bus.Domain.Model;

namespace Bic.Bus.Domain.ResponseModel
{
    public class ResponseCustomers
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Customers Customers { get; set; }
    }
}
