//using Newtonsoft.Json;
//using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Text.Json.Serialization;
//using System.Text.Json.Serialization;

namespace Bic.Bus.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            string _customers = @"{
                                ""Success"": true,
                                ""Customers"":{
                                    ""CodeHtml"":""1"",
                                    ""CodeInternal"":""100GCA158"",
                                    ""CnpjParameter"":""03094515000196"",
                                    ""CNPJConsulted"":""03094515000196"",
                                    ""NumberRegistration"":""03.094.515/0001-96"",
                                    ""NameBusiness"":""NOVO DE NOVO COMERCIO E REFORMAS DE MOVEIS LTDA - ME"",
                                    ""RegistrationStage"":""675.079.240.117"",
                                    ""SupplierName"":""Thato"",
                                        ""Address"":{
                                            ""UfParameter"":""SP"",
                                            ""Logradouro"":""PC ARGENTINA"",
                                            ""Numero"":""289"",
                                            ""Complemento"":""."",
                                            ""CEP"":""06.756-330"",
                                            ""Bairro"":""JARDIM MARIA ROSA"",
                                            ""Municipio"":""TABOAO DA SERRA"",
                                            ""UF"":""SP"",
                                            ""CodeIbge"":""3552809"",
                                            ""CustomersId"":0,
                                            ""Id"":0
                                        },
                                    ""Id"":0
                                }
                        }";

            //string _customers = @"{
            //                    'Id':0,
            //                    'CodeHtml':'1',
            //                    'CodeInternal':'100GCA158',
            //                    'CnpjParameter':'03094515000196',
            //                    'CNPJConsulted':'03094515000196',
            //                    'NumberRegistration':'03.094.515/0001-96',
            //                    'NameBusiness':'NOVO DE NOVO COMERCIO E REFORMAS DE MOVEIS LTDA - ME',
            //                    'RegistrationStage':'675.079.240.117',
            //                    'SupplierName':'Thato'
            //                    }";

            var _customer = JsonSerializer.Deserialize<ResponseCustomers>(_customers);
            //var _customer = JsonConvert.DeserializeObject<Customers>(_customers);
            //Console.WriteLine(account.Email);
            Assert.True(true);
        }

        public class Customers
        {
            public Customers()
            {
                Address = new Address();
            }
            //[JsonIgnore]
            public int Id { get; set; }
            public string CodeHtml { get; set; }
            public string CodeInternal { get; set; }
            public string CnpjParameter { get; set; }
            public string CNPJConsulted { get; set; }
            public string NumberRegistration { get; set; }
            public string NameBusiness { get; set; }
            public string RegistrationStage { get; set; }
            public string SupplierName { get; set; }
            public Address Address { get; set; }
            //public Card Cards { get; set; }

            public Customers(string codeHtml, string codeInternal, string cnpjParameter, string cnpjConsulted
                , string numberRegistration, string nameBusiness, string registrationStage, string supplierName
                ,Address address)
            {

                CodeHtml = codeHtml;
                CodeInternal = codeInternal;
                CnpjParameter = cnpjParameter;
                CNPJConsulted = cnpjConsulted;
                NumberRegistration = numberRegistration;
                NameBusiness = nameBusiness;
                RegistrationStage = registrationStage;
                SupplierName = supplierName;
                Address = address;
            }
        }

        public class Address
        {
            public string? UfParameter { get; set; }
            public string? Logradouro { get; set; }
            public string? Numero { get; set; }
            public string? Complemento { get; set; }
            public string? CEP { get; set; }
            public string? Bairro { get; set; }
            public string? Municipio { get; set; }
            public string? UF { get; set; }
            public string? CodeIbge { get; set; }
            public int CustomersId { get; set; }
        }

        public class ResponseCustomers
        {
            public bool Success { get; set; }
            public Customers Customers { get; set; }            
        }
    }
}