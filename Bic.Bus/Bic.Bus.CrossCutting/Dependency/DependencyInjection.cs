﻿using Bic.Bus.Domain.Interfaces.Kafka;
using Bic.Bus.Domain.Interfaces.MQ;
using Bic.Bus.Domain.Interfaces.Repository;
using Bic.Bus.Domain.Interfaces.Request;
using Bic.Bus.Infrastructure.Context;
using Bic.Bus.Infrastructure.Kafka;
using Bic.Bus.Infrastructure.Rabbit.Consumer;
using Bic.Bus.Infrastructure.Rabbit.Publisher;
using Bic.Bus.Infrastructure.Repository;
using Bic.Bus.Infrastructure.Request;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bic.Bus.CrossCutting.Dependency
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInjection(this IServiceCollection services, IConfiguration configuration)
        {
            var mySqlConnection = configuration.GetConnectionString("ConnectionMySql");
            services.AddDbContext<AppDbContext>(options =>
                                                options.UseMySql(mySqlConnection,
                                                                 ServerVersion.AutoDetect(mySqlConnection)));
            services.AddScoped<ICustomersRepository, CustomersRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWorkRepository>();
            services.AddSingleton<IMQKafkaPublisher, MQKafkaPublisher>();
            services.AddSingleton<IRequestCustomersDomain, RequestCustomers>();
            services.AddSingleton<IMQCustomersConsumer, MQCustomersConsumer>();
            services.AddScoped<IMQCustomersPublisher, MQCustomersPublisher>();
            services.AddSingleton<IMQKafkaConsumer, MQKafkaConsumer>();

            var myhandlers = AppDomain.CurrentDomain.Load("Bic.Bus.Application");
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(myhandlers));
            return services;
        }
    }
}
