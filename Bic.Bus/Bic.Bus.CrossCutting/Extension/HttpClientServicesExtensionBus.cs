﻿using Microsoft.Extensions.DependencyInjection;
using Polly;

namespace Bic.Bus.CrossCutting.Extension
{
    public static class HttpClientServicesExtensionBus
    {
        public static IServiceCollection AddHttpClientBus(this IServiceCollection services)
        {
            services.AddHttpClient("clientNodeBus", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("http://localhost:8081/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressCielo", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("http://localhost:8082/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressGetNet", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("http://localhost:8083/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            services.AddHttpClient("BaseAddressMauaBank", HttpsClient =>
            {
                HttpsClient.BaseAddress = new Uri("http://localhost:8084/");
            }).AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
            {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(5),
                TimeSpan.FromSeconds(10)
            }))
            .AddTransientHttpErrorPolicy(builder => builder.CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 3,
                durationOfBreak: TimeSpan.FromSeconds(30)
            ));

            return services;
        }
    }
}
