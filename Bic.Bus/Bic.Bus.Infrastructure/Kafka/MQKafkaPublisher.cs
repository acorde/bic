﻿using Bic.Bus.Domain.Interfaces.Kafka;
using Bic.Bus.Domain.Model.Configuration;
using Bic.Bus.Domain.ResponseModel;
using Confluent.Kafka;
using System.Text.Json;

namespace Bic.Bus.Infrastructure.Kafka
{
    public class MQKafkaPublisher : IMQKafkaPublisher
    {
        private readonly SettingsConfiguration _settingsConfiguration;
        public MQKafkaPublisher(SettingsConfiguration settingsConfiguration)
        {
            _settingsConfiguration = settingsConfiguration;
        }

        public async Task SendKafka(ResponseCustomers message, string nameQueue)
        {
            await Task.Run(() =>
            {
                var messageJson = JsonSerializer.Serialize(message);
                var config = new ProducerConfig { BootstrapServers = _settingsConfiguration.KafkaSettings.Host };
                using (var producer = new ProducerBuilder<string, string>(config).Build())
                {
                    var result = producer.ProduceAsync(nameQueue,
                                                        new Message<string, string>
                                                        {
                                                            Key = Guid.NewGuid().ToString(),
                                                            Value = messageJson
                                                        });
                }
            });
        }
    }
}
