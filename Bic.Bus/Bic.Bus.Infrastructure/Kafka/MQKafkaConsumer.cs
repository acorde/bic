﻿using Bic.Bus.Domain.Interfaces.Kafka;
using Bic.Bus.Domain.ResponseModel;
using Confluent.Kafka;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace Bic.Bus.Infrastructure.Kafka
{
    public class MQKafkaConsumer : IMQKafkaConsumer
    {
        private readonly ILogger<MQKafkaConsumer> _logger;
        private readonly IConfiguration _config;
        private readonly JsonSerializerOptions _options;

        public MQKafkaConsumer(ILogger<MQKafkaConsumer> logger,
                                IConfiguration config)
        {
            _logger = logger;
            _config = config;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
        }

        public async Task ConsumerKafka()
        {
            await Task.Run(() =>
            {
                string? _host = "localhost:9092";// _config.GetSection("KafkaSettings:Host").Value;
                string? _topic = "subscribe"; // _config.GetSection("KafkaSettings:Topic").Value;

                var config = new ConsumerConfig
                {
                    BootstrapServers = "localhost:9092",
                    GroupId = "subscribe-group-0",
                    AutoOffsetReset = AutoOffsetReset.Earliest,
                    //EnableAutoOffsetStore = false,
                    //EnableAutoCommit = true, //default
                    //AutoCommitIntervalMs = 0
                };

                using (var consumer = new ConsumerBuilder<string, string>(config).Build())
                {
                    consumer.Subscribe("subscribe");
                    try
                    {
                        var cr = consumer.Consume();
                        var result = JsonSerializer.Deserialize<ResponseCustomers>(cr.Value, _options);
                        _logger.LogInformation($"Key: {cr.Message.Key} | Value: {cr.Message.Value}");

                        consumer.Close();
                    }
                    catch (OperationCanceledException)
                    {
                        throw;
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            });
        }
    }
}
