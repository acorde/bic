﻿using Microsoft.EntityFrameworkCore;
using Bic.Bus.Domain.Model;
using Bic.Bus.Infrastructure.ModelsConfiguration;

namespace Bic.Bus.Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        public DbSet<Customers> Customers { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Card> Card { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CustomersConfiguration());
            builder.ApplyConfiguration(new AddressConfiguration());
            builder.ApplyConfiguration(new CardConfiguration());
        }
    }
}
