﻿using Bic.Bus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bic.Bus.Infrastructure.ModelsConfiguration
{
    public class CardConfiguration : IEntityTypeConfiguration<Card> 
    {
        public void Configure(EntityTypeBuilder<Card> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.NumberCard);
            builder.Property(x => x.ExpirationDate);
            builder.Property(x => x.NameCard);
            builder.Property(x => x.Band);
            builder.Property(x => x.SecurityCode);
            builder.Property(x => x.CustomersId);
        }
    }
}
