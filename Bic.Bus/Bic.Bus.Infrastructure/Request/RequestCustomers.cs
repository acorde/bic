﻿using Bic.Bus.Domain.Interfaces.Kafka;
using Bic.Bus.Domain.Interfaces.Request;
using Bic.Bus.Domain.Model.Configuration;
using Bic.Bus.Domain.ResponseModel;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Text.Json;

namespace Bic.Bus.Infrastructure.Request
{
    public class RequestCustomers : IRequestCustomersDomain
    {
        private readonly ILogger<RequestCustomers> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _settings;
        private readonly IMQKafkaPublisher _mqKafkaPublisher;

        public RequestCustomers(IHttpClientFactory httpClientFactory,
                                    ILogger<RequestCustomers> logger,
                                    SettingsConfiguration settings,
                                    IMQKafkaPublisher mqKafkaPublisher)
        {
            _httpClientFactory = httpClientFactory;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _logger = logger;
            _settings = settings;
            _mqKafkaPublisher = mqKafkaPublisher;
        }

        public async Task<bool> SendGetNet<T>(T model)
        {
            try
            {
                _logger.LogInformation($"{nameof(SendGetNet)} - Iniciando a requisição Http Post para geraração de cartão na Cielo.");
                var _httpClient = _httpClientFactory.CreateClient("BaseAddressGetNet");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");
                
                var _uri = _settings?.UriSettings?.UriGetNet;
                
                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    var _responseCustomer = JsonSerializer.Deserialize<ResponseCustomers>(_result, _options);
                    
                    //await _mqKafkaPublisher.SendKafka(_responseCustomer, _settings.KafkaQueueName.QueueDurable.QueueDurableThato.ToString());

                    if (!_responseCustomer.Success)
                    {
                        _logger.LogError($"{nameof(SendGetNet)} - Erro no request Http Post para geraração de cartão na Cielo. {_responseCustomer.Message}");
                        return false;
                    }
                    _logger.LogInformation($"{nameof(SendGetNet)} - Fim da requisição Http Post para geraração de cartão na Cielo.");
                    return true;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: requisição Http Post para geraração de cartão na Cielo não retornou dados.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendGetNet)} - Erro: requisição Http Post para geraração de cartão na Cielo. {ex.Message}");
                return false;
            }
        }

        public async Task<bool> SendCielo<T>(T model)
        {
            try
            {
                _logger.LogInformation($"{nameof(SendCielo)} - Iniciando a requisição Http Post para geraração de cartão na Cielo.");
                var _httpClient = _httpClientFactory.CreateClient("BaseAddressCielo");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");

                var _uri = _settings?.UriSettings?.UriCielo;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    var _responseCustomer = JsonSerializer.Deserialize<ResponseCustomers>(_result, _options);

                    //await _mqKafkaPublisher.SendKafka(_responseCustomer, "subscribe");

                    if (!_responseCustomer.Success)
                    {
                        _logger.LogError($"{nameof(SendCielo)} - Erro no request Http Post para geraração de cartão na Cielo. {_responseCustomer.Message}");
                        return false;
                    }
                    _logger.LogInformation($"{nameof(SendCielo)} - Fim da requisição Http Post para geraração de cartão na Cielo.");
                    return true;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: requisição Http Post para geraração de cartão na Cielo não retornou dados.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendCielo)} - Erro: requisição Http Post para geraração de cartão na Cielo. {ex.Message}");
                return false;
            }
        }

        public async Task<bool> SendMauaBank<T>(T model)
        {
            try
            {
                _logger.LogInformation($"{nameof(SendMauaBank)} - Iniciando a requisição Http Post para geraração de cartão na Maua Bank.");
                var _httpClient = _httpClientFactory.CreateClient("BaseAddressMauaBank");
                var _content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json");

                var _uri = _settings?.UriSettings?.UriMauaBank;

                using var _httpResponse = await _httpClient.PostAsync(_uri, _content);

                if (_httpResponse.IsSuccessStatusCode)
                {
                    var _result = await _httpResponse.Content.ReadAsStringAsync();
                    var _responseCustomer = JsonSerializer.Deserialize<ResponseCustomers>(_result, _options);

                    //await _mqKafkaPublisher.SendKafka(_responseCustomer, _settings.KafkaQueueName.QueueDurable.QueueDurableThato.ToString());

                    if (!_responseCustomer.Success)
                    {
                        _logger.LogError($"{nameof(SendMauaBank)} - Erro no request Http Post para geraração de cartão na Maua Bank. {_responseCustomer.Message}");
                        return false;
                    }
                    _logger.LogInformation($"{nameof(SendMauaBank)} - Fim da requisição Http Post para geraração de cartão na Maua Bank.");
                    return true;
                }
                else
                {
                    _logger.LogWarning($"Cuidado: requisição Http Post para geraração de cartão na Cielo não retornou dados.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(SendMauaBank)} - Erro: requisição Http Post para geraração de cartão na Maua Bank. {ex.Message}");
                return false;
            }
        }
    }
}
