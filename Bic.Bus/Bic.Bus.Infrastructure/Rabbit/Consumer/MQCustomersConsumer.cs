﻿using Bic.Bus.Domain.Interfaces.MQ;
using Bic.Bus.Domain.Interfaces.Request;
using Bic.Bus.Domain.Model;
using Bic.Bus.Domain.Model.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace Bic.Bus.Infrastructure.Rabbit.Consumer
{
    public class MQCustomersConsumer : IMQCustomersConsumer
    {
        private readonly IRequestCustomersDomain _request;
        private readonly JsonSerializerOptions _options;
        private readonly SettingsConfiguration _settings;

        public MQCustomersConsumer(IRequestCustomersDomain request,
                                        SettingsConfiguration settings)
        {
            _request = request;
            _options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            _settings = settings;
        }

        private static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task ConsumerCielo()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings?.Host,
                    Port = _settings.RabbitMQSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.QueueSantander?.Cielo, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings?.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.Cielo
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _request.SendCielo<Customers>(customer);

                        if (result)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.Cielo, false, consumer);

            });
        }

        public async Task ConsumerMauaBank()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings?.Host,
                    Port = _settings.RabbitMQSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.QueueSantander?.MauaBank, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings?.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.MauaBank
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _request.SendMauaBank<Customers>(customer);

                        if (result)
                            channel.BasicAck(ea.DeliveryTag, false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(_settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.MauaBank, false, consumer);

            });
        }

        public async Task ConsumerGetNet()
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings?.Host,
                    Port = _settings.RabbitMQSettings.Port
                };
                var connection = factory.CreateConnection();
                var channel = CreateChannel(connection);
                channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.QueueSantander?.GetNet, true, false, false
                                    , arguments: new Dictionary<string, object>()
                                    {
                                        ["x-dead-letter-exchange"] = _settings?.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.GetNet
                                    });

                channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var customer = JsonSerializer.Deserialize<Customers>(message, _options);
                        var result = await _request.SendGetNet<Customers>(customer);

                        if (result)
                            channel.BasicAck(ea.DeliveryTag,  false);
                        else
                            channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                    catch (Exception)
                    {
                        channel.BasicNack(ea.DeliveryTag,  false, false);
                    }
                };
                channel.BasicConsume(queue: _settings.RabbitQueueName?.QueueDurable?.QueueDurableAtento?.QueueSantander?.GetNet, false, consumer);

            });
        }

    }
}
