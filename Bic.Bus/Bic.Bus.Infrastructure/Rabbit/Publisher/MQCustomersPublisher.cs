﻿using Bic.Bus.Domain.Interfaces.MQ;
using Bic.Bus.Domain.Model.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Bic.Bus.Infrastructure.Rabbit.Publisher
{
    public class MQCustomersPublisher : IMQCustomersPublisher
    {
        private readonly ILogger<MQCustomersPublisher> _logger;
        private readonly SettingsConfiguration _settings;

        public MQCustomersPublisher(ILogger<MQCustomersPublisher> logger, 
                                    SettingsConfiguration settings)
        {
            _logger = logger;
            _settings = settings;
        }

        public static IModel CreateChannel(IConnection connection)
        {
            var channel = connection.CreateModel();
            return channel;
        }

        public async Task PublisherCielo<Customers>(Customers message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings?.Host,
                    Port = _settings.RabbitMQSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherCielo)} - Inicio a publicação da mensagem na fila atento-bus para o cliente Cielo.");
                    var channel = CreateChannel(connection);
                    channel.ExchangeDeclare(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.Cielo, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.Cielo, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.Cielo, _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.Cielo, "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.Cielo }
                        };

                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.Cielo, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.Cielo, null, body);
                    _logger.LogInformation($"{nameof(PublisherCielo)} - Fim a publicação da mensagem na fila atento-bus para o cliente Cielo.");
                };
            });
        }

        public async Task PublisherGetNet<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings?.Host,
                    Port = _settings.RabbitMQSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherGetNet)} - Inicio a publicação da mensagem na fila atento-bus para o cliente GetNet.");
                    var channel = CreateChannel(connection);
                    channel.ExchangeDeclare(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.GetNet, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.GetNet, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.GetNet, _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.GetNet, "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.GetNet }
                        };

                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.GetNet, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.GetNet, null, body);
                    _logger.LogInformation($"{nameof(PublisherGetNet)} - Fim a publicação da mensagem na fila atento-bus para o cliente GetNet.");
                };
            });
        }

        public async Task PublisherMauaBank<T>(T message)
        {
            await Task.Run(() =>
            {
                var factory = new ConnectionFactory
                {
                    HostName = _settings.RabbitMQSettings?.Host,
                    Port = _settings.RabbitMQSettings.Port
                };
                using (var connection = factory.CreateConnection())
                {
                    _logger.LogInformation($"{nameof(PublisherMauaBank)} - Inicio a publicação da mensagem na fila atento-bus para o cliente Maua Bank.");
                    var channel = CreateChannel(connection);
                    channel.ExchangeDeclare(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.MauaBank, ExchangeType.Fanout);
                    channel.QueueDeclare(queue: _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.MauaBank, true, false, false, null);
                    channel.QueueBind(_settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.MauaBank, _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.MauaBank, "");

                    var args = new Dictionary<string, object>()
                        {
                            { "x-dead-letter-exchange", _settings.RabbitQueueName?.QueueDlq?.QueueDlqAtento?.QueueSantander?.MauaBank }
                        };

                    channel.QueueDeclare(queue: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.MauaBank, true, false, false, args);
                    var json = JsonSerializer.Serialize(message);
                    var body = Encoding.UTF8.GetBytes(json);
                    channel.BasicPublish(exchange: string.Empty, routingKey: _settings.RabbitQueueName.QueueDurable?.QueueDurableAtento?.QueueSantander?.MauaBank, null, body);
                    _logger.LogInformation($"{nameof(PublisherMauaBank)} - Fim a publicação da mensagem na fila atento-bus para o cliente Maua Bank.");
                };
            });
        }
    }
}
